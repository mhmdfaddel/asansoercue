class SubStage {
  String name;
  DateTime date;
  bool done;
  String description;

  SubStage({this.name, this.date, this.done, this.description});
}
