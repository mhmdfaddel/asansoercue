import 'dart:convert';

import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/accessories/_SystemPadding.dart';
import 'package:asansorci/inProgress_short/InProgressLift.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Stage.dart';

class InProgressLiftsView extends StatefulWidget {
  @override
  _InProgressLiftsViewState createState() => _InProgressLiftsViewState();
}

class _InProgressLiftsViewState extends State<InProgressLiftsView> {
  final String appTitle = 'ASANSÖRCÜ';
  Constants consts = new Constants();
  int _currentStep = 1;
  List<Stage> stages = [
   Stage(name: "Rail installation and external door trim installation"),
  Stage(name: "Assembling and installing the motor on a table"),
  Stage(name: "Assembling the moving stabilizer and the cab (Suspension) and connecting them by means of traction or lifting ropes"),
   Stage(name: "Installing the power supply 380 and installing a control panel"),
  Stage(name: "Installation of motor cables, lighting, speed control, safety keys and everything related to the motor room to operate the elevator down and up"),
  Stage(name: "Installation and supply of control panel cables and lighting and installation of light bulbs in the well"),
  Stage(name: "Installation of shock absorbers, stabilizer protection plates, ladder, speed control, and installation of a well control panel"),
  Stage(name: "Installation of external doors"),
  Stage(name: "Installing the Cabin over the Suspension"),
  Stage(name: "Installing a control panel above the cabin, equipping and Installing magnetic sensors and special safety devices above the cabin, and connecting them to a control panel"),
  Stage(name: "Installation of the interior door of the cabin with Fotosel motion sensor"),
  Stage(name: "Tests of mechanical and electrical parts in the entire elevator"),
  ];

  List<InProgressLift> myLifts;
  Color color = Colors.white;

  var _dropLiftType = "";
  TextEditingController buildingno = new TextEditingController();
  TextEditingController buildingname = new TextEditingController();
  TextEditingController committedname = new TextEditingController();
  TextEditingController committednumber = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController buildinginfo = new TextEditingController();
  TextEditingController search = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getLifts();

    for(InProgressLift a in myLifts)
      {
        a.stages.addAll(stages);
        a.color = Colors.white;
      }
  }

  Future<void> _launched;


  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        myLifts.clear();
        getLifts();
      });
    _refreshController.loadComplete();
  }

  List<InProgressLift> temps = [];

  List<InProgressLift> existLift(String temp, String cirtic) {
    List<InProgressLift> testo = new List<InProgressLift>();


    if(cirtic == 'Fake ID'.tr().toString())
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].fakekimlik.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    else if(cirtic == 'BINA NUMARASI'.tr().toString()){
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].buildingNumber.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    }
    else if(cirtic == 'BINA'.tr().toString()){
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].buildingName.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    }
    return testo;
  }


  String _dropDownValue;

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height,
          width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "InProgresslifts".tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid), // set border width
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: TextField(
                controller: search,
                onChanged: (value) =>{
                  setState(() {
                    if(this.search.value.text == "")
                    {
                      myLifts = temps;
                    }
                    else if(_dropDownValue == 'Fake ID'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'Fake ID'.tr().toString());
                    }
                    else if(_dropDownValue == 'BINA NUMARASI'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'BINA NUMARASI'.tr().toString());
                    }
                    else if(_dropDownValue == 'BINA'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'BINA'.tr().toString());
                    }
                  })
                },
                decoration: InputDecoration(
                  hintText: "search_elevator".tr().toString(),
                  hintStyle: TextStyle(
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.02,
                  MediaQuery.of(context).size.width * 0.02,
                  0,
                  MediaQuery.of(context).size.width * 0.02),
              child: Row(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.width * 0.37,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text("search_by".tr().toString())
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: Colors.blue),
                              ),
                        isExpanded: true,
                        iconSize: 15.0,
                        style: TextStyle(color: Colors.blue),
                        items: [
                          "Fake ID".tr().toString(),
                          "building_number".tr().toString(),
                          "building_name".tr().toString()
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val;
                            },
                          );
                        },
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.32, 0, 0, 0),
                    child: Text(
                      'total'.tr().toString() +
                          ' : ' +
                          this.myLifts.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                height:
                    (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                        ? MediaQuery.of(context).size.height * 0.5887
                        : MediaQuery.of(context).size.height * 0.602,
                width: MediaQuery.of(context).size.width * 1,
                child: Row(
                  children: [
                    Container(
                      child: Expanded(
                        child: ListView.builder(
                          itemCount: this.myLifts.length,
                          itemBuilder: (context, index) =>
                              this._buildRow(this.myLifts[index]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            copyRights(),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(InProgressLift temp) {
    return GestureDetector(
      onHorizontalDragStart: (DragStartDetails details){
        setState(() {
          temp.color = Colors.redAccent;
        });
      },
      onHorizontalDragEnd: (DragEndDetails details){
        if (details.primaryVelocity > 0) {

          setState(() {
            _showDialog(temp);
          });
        }
        else
          {
            setState(() {
              temp.color = Colors.white;
            });
          }
      },
      child: Card(
        shadowColor: Colors.grey,
        margin: const EdgeInsets.all(10),
        elevation: 30.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        color: temp.color,
        child: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RawMaterialButton(
                  onPressed: () {
                    _awaitReturnValueFromSecondScreen(context, temp);
                  },
                  elevation: 2.0,
                  fillColor:
                      (temp.currentStage >= 0) ? Colors.green : Colors.blue,
                  child: Text((temp.currentStage+1 < 12)?(temp.currentStage+1).toString():(temp.currentStage).toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          )),
                  padding: EdgeInsets.all(15.0),
                  shape: CircleBorder(),
                ),
                SizedBox(height: 2,),
                RawMaterialButton(onPressed: (){

                },
                  elevation: 2.0,
                  fillColor: Colors.blueGrey,
                  shape: CircleBorder(),
                  child: IconButton(
                    padding: EdgeInsets.all(10.0),
                    icon: Icon(
                      Icons.location_on,
                      color: Colors.green,
                      size: 20.0,
                    ),
                    onPressed: () {
                        MapUtils.openMap(temp.location.latitude, temp.location.longitude);
                    },
                  ),
                ),

                SizedBox(height: 2,),

                RawMaterialButton(onPressed: (){
                },
                  elevation: 2.0,
                  fillColor: Colors.blueGrey,
                  shape: CircleBorder(),
                  child: IconButton(
                    padding: EdgeInsets.all(10.0),
                    icon: Icon(
                      Icons.info,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    onPressed: () {
                      setState(() {
                        showDialog(context: context, builder: (BuildContext){
                          return SingleChildScrollView(
                            child: Dialog(
                              insetPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*.3,horizontal: MediaQuery.of(context).size.width*.1),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.8,
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: SizedBox.expand(
                                          child: Padding(
                                            padding: const EdgeInsets.all(1.0),
                                            child: Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.23,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        //controller : buildingno,
                                                        decoration: InputDecoration(
                                                          hintText:
                                                          temp.fakekimlik,
                                                          hintStyle: TextStyle(
                                                            color: Colors.grey,
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons.door_front),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller : buildingno,
                                                        decoration: InputDecoration(
                                                          hintText:
                                                          '#        '+temp.buildingNumber,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller: buildingname,
                                                        decoration: InputDecoration(
                                                          hintText: temp.buildingName,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon:
                                                          Icon(Icons.house_siding_sharp),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.4,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        controller: committedname,
                                                        decoration: InputDecoration(
                                                          hintText: temp
                                                              .comittedname,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons
                                                              .person),
                                                        ),
                                                      ),
                                                      GestureDetector(
                                                        onDoubleTap: (){
                                                          _launched = _makePhoneCall("tel:" + temp.comittedphone);
                                                        },
                                                        child: TextField(
                                                          enabled: true,
                                                          controller: committednumber,
                                                          decoration: InputDecoration(
                                                            hintText: temp
                                                                .comittedphone,
                                                            hintStyle: TextStyle(
                                                            ),
                                                            border: InputBorder.none,
                                                            icon: Icon(Icons
                                                                .phone),

                                                          ),
                                                        ),
                                                      ),
                                                      TextField(
                                                        enabled: true,
                                                        controller: buildinginfo,
                                                        decoration: InputDecoration(
                                                          hintText: temp
                                                              .buildinginfo,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons
                                                              .apartment),
                                                        ),
                                                      ),
                                                      TextField(
                                                        enabled: false,
                                                        decoration: InputDecoration(
                                                          hintText: temp
                                                              .lifttype,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons
                                                              .settings),
                                                        ),
                                                      ),
                                                      Container(

                                                        height: MediaQuery.of(context).size.height * 0.06,
                                                        margin: EdgeInsets.all(10),
                                                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                                                        child: DropdownButton(
                                                          hint:  Text( (_dropLiftType == "")?
                                                          temp.lifttype.tr().toString()
                                                            :_dropLiftType.tr().toString(),
                                                            // style: TextStyle(color: Colors.blue),
                                                          ),
                                                          isExpanded: true,
                                                          iconSize: 15.0,
                                                          style: TextStyle(color: Colors.blue),
                                                          items: [
                                                            'Gear Full Engine'.tr().toString(),
                                                            'Gear Less Engine'.tr().toString(),
                                                          ].map(
                                                                (val) {
                                                              return DropdownMenuItem<String>(
                                                                value: val,
                                                                child: Text(val),
                                                              );
                                                            },
                                                          ).toList(),
                                                          onChanged: (val) {
                                                            setState(() {
                                                              _dropLiftType = val;
                                                              temp.lifttype = val;
                                                            },
                                                            );
                                                          },
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                ),


                                                Row(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.symmetric(vertical: 0 , horizontal: 30),
                                                      child: RaisedButton(
                                                        color: Colors.white12,
                                                        child: Text(
                                                          'Cancel'.tr().toString(),
                                                          style: TextStyle(
                                                              color: Colors.white),
                                                        ),
                                                        onPressed: (){
                                                          setState(() {
                                                            Navigator.pop(context);
                                                          });
                                                        },
                                                      ),
                                                    ),

                                                    RaisedButton(
                                                      color: Colors.blueAccent,
                                                      child: Text(
                                                        'Okay'.tr().toString(),
                                                        style: TextStyle(
                                                            color: Colors.white),
                                                      ),
                                                      onPressed: () async
                                                      {
                                                        if(!buildingname.text.isEmpty)
                                                          temp.buildingName = buildingname.text;
                                                        if(!buildingno.text.isEmpty)
                                                          temp.buildingNumber = buildingno.text;
                                                        if(!committedname.text.isEmpty)
                                                          temp.comittedname = committedname.text;
                                                        if(!committednumber.text.isEmpty)
                                                          temp.comittedphone = committednumber.text;
                                                        if(_dropLiftType == "")
                                                          temp.lifttype = temp.lifttype;
                                                        else temp.lifttype = _dropLiftType;
                                                        if(!buildinginfo.text.isEmpty)
                                                          temp.buildinginfo = buildinginfo.text;

                                                        putRequest(temp);
                                                        await Navigator.of(context).pop();
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        });
                      });
                    },
                  ),
                ),

              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'building_number'.tr().toString() + " : ",
                          children: [
                            TextSpan(
                              text: temp.buildingNumber.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'building_name'.tr().toString() + " : ",
                          children: [
                            TextSpan(
                              text: temp.buildingName.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'Comitted Name'.tr().toString() + ' : ',
                          children: [
                            TextSpan(
                              text: temp.comittedname.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'Comitted Phone'.tr().toString() + ' : ',
                          children: [
                            TextSpan(
                              text: temp.comittedphone.toString(),
                            )
                          ],
                        ),
                      )),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context, InProgressLift temp) async {
    print(temp.stages[0].desc);
    await Navigator.pushNamed(context, "/inProgressLiftView",
        arguments: {"InProgressLifte": temp});
  }

  _showDialog(InProgressLift temp) async {
    await showDialog<String>(
      context: context,
      builder: (context) =>  new AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                autofocus: true,
                controller: passwordController,
                decoration: new InputDecoration(

                    labelText: 'Password'.tr().toString()),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child:  Text('Cancel'.tr().toString()),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child:  Text('Ok'),
              onPressed: () {
                if(passwordController.text == (new Constants()).password)
                  {
                    myLifts.remove(temp);
                    deleteRequest(temp);
                    Navigator.pop(context);
                  }
              })
        ],
      ),
    );
  }

  Future<void> getLifts() async {
    myLifts = new List<InProgressLift>();
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_inProgress);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);

      myLifts = data.map((InProgres) => new InProgressLift().fromJson(InProgres)).toList();

      temps.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }
  }

  Future<Response> deleteRequest (InProgressLift temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_inProgress+temp.id.toString());

    await delete(url
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => super.widget));
      }
    });
  }

  Future<Response> putRequest (InProgressLift temp) async {
    print(temp.toJson());
    Uri url = Uri.https(
        consts.domain, consts.domain_inProgress+temp.id.toString());


    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}


