import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/Models/Test_Book.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:asansorci/inProgress_short/InProgressLift.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';

import 'Stage.dart';

class InProgressLiftView extends StatefulWidget {
  @override
  _InProgressLiftViewState createState() => _InProgressLiftViewState();
}

class _InProgressLiftViewState extends State<InProgressLiftView> {
  String appTitle = 'ASANSÖRCÜ';
  Constants consts = new Constants();
  int _currentStep = 0;

  TextEditingController installerNumberController = new TextEditingController();
  TextEditingController installerNameController = new TextEditingController();
  TextEditingController kimlikController = new TextEditingController();
  TextEditingController telephoneController = new TextEditingController();
  TextEditingController _controller = new TextEditingController();

  DateTime realdate;
  TimeOfDay time;
  LocaleType localetype;

  String _dropstatus = "";

  List<Stage> stages = [
    Stage(name: "Rail installation and external door trim installation"),
    Stage(name: "Assembling and installing the motor on a table"),
    Stage(
        name: "Assembling the moving stabilizer and the cab (Suspension) and connecting them by means of traction or lifting ropes"),
    Stage(
        name: "Installing the power supply 380 and installing a control panel"),
    Stage(
        name: "Installation of motor cables, lighting, speed control, safety keys and everything related to the motor room to operate the elevator down and up"),
    Stage(
        name: "Installation and supply of control panel cables and lighting and installation of light bulbs in the well"),
    Stage(
        name: "Installation of shock absorbers, stabilizer protection plates, ladder, speed control, and installation of a well control panel"),
    Stage(name: "Installation of external doors"),
    Stage(name: "Installing the Cabin over the Suspension"),
    Stage(
        name: "Installing a control panel above the cabin, equipping and Installing magnetic sensors and special safety devices above the cabin, and connecting them to a control panel"),
    Stage(
        name: "Installation of the interior door of the cabin with Fotosel motion sensor"),
    Stage(
        name: "Tests of mechanical and electrical parts in the entire elevator"),
  ];
  InProgressLift temp;

  @override
  void initState() {
    super.initState();
    temp = new InProgressLift();
    temp.stages.addAll(stages);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }


  int tempo;
  bool firsttime = true;


  @override
  Widget build(BuildContext context) {
    if (tempo == null) {
      if (firsttime) {
        try {
          Map<String, Object> rcvdData =
              ModalRoute
                  .of(context)
                  .settings
                  .arguments;
          if (rcvdData['InProgressLifte'] != null) {
            temp = rcvdData['InProgressLifte'];
            tempo = temp.currentStage;

            for(int i = 0; i < temp.stages.length; i++)
            {
              if(temp.stages[i].desc.isNotEmpty)
                stages[i].controller.text = temp.stages[i].desc;
              else
              {

              }
            }
          }
        } catch (Exception) {}
        if(tempo != 12)
          _currentStep = tempo;
        else
          _currentStep = temp.currentStage-1;
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        isDetails: false,
        isHome: false,
        title: appTitle,
        isLang: false,
      ),
      body: Column(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.7304,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 1,
                  child: Stepper(
                    type: StepperType.vertical,
                    physics: ClampingScrollPhysics(),
                    onStepTapped: (step) {
                      setState(() {
                        _currentStep = step;
                      });
                    },
                    onStepContinue: () {
                      setState(() {
                        print(_currentStep);
                        _currentStep++;
                      });
                    },
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) {
                      return Row(
                        children: <Widget>[
                          RaisedButton(
                            color: (_currentStep == 11)
                                ? Colors.green
                                : Colors.blueAccent, // background
                            onPressed: () {
                              onStepContinue();
                            },
                            child: (_currentStep == 11)
                                ? Text('complete'.tr().toString())
                                : Text('continue'.tr().toString()),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          RaisedButton(
                            color: Colors.grey, // background
                            textColor: Colors.black45, // foreground
                            onPressed: () {
                              onStepCancel();
                            },
                            child: Text(
                              'back'.tr().toString(),
                            ),
                          )
                        ],
                      );
                    },
                    currentStep: _currentStep,
                    steps: List.generate(stages.length, (index) =>
                    new Step(
                      title: new Text(stages[index].name.tr().toString()),
                      content: Column(
                        children: <Widget>[
                          Container(
                            height: MediaQuery
                                .of(context)
                                .size
                                .height * 0.3,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle
                                      .solid), // set border width
                              // set rounded corner radius
                            ),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical, //.horizontal
                              child: TextField(
                                controller: stages[index].controller,
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                  hintText: (temp.stages[index].desc.isEmpty)
                                      ? "description".tr().toString()
                                      : temp.stages[index].desc,
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  border: InputBorder.none,
                                  icon: Icon(Icons.description),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      isActive: true,
                      // state: _currentStep >= 0
                      //     ? StepState.complete
                      //     : StepState.disabled,
                    ),
                    ),
                  ),
                ),
                Divider(
                  indent: 2,
                  endIndent: 10,
                ),

              ],
            ),
          ),
          Row(
            children: [
              Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.5,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.lightBlue,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.save),
                      Text(
                        'save'.tr().toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {
                    temp.currentStage = _currentStep;
                    for (int i = 0; i < stages.length; i++) {
                      temp.stages[i].desc = stages[i].controller.text;
                    }
                    putRequest(temp);
                    if (temp.currentStage == 11) {
                      setState(() {
                        showDialog(context: context, builder: (BuildContext) {
                          return SingleChildScrollView(
                            child: Dialog(
                              insetPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery
                                      .of(context)
                                      .size
                                      .height * .3, horizontal: MediaQuery
                                  .of(context)
                                  .size
                                  .width * .1),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                height: MediaQuery
                                    .of(context)
                                    .size
                                    .height * 0.5,
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width * 0.9,
                                color: Colors.black45,
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: SizedBox.expand(
                                          child: Padding(
                                            padding: const EdgeInsets.all(1.0),
                                            child: Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .height *
                                                      0.13,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        controller: kimlikController,
                                                        decoration: InputDecoration(
                                                          hintText: 'Kimlik'
                                                              .tr()
                                                              .toString(),
                                                          hintStyle: TextStyle(
                                                            color: Colors.grey,
                                                          ),
                                                          border: InputBorder
                                                              .none,
                                                          icon: Icon(Icons
                                                              .person),
                                                        ),
                                                      ),
                                                      Row(

                                                        children: <Widget>[
                                                          IconButton(
                                                            icon: Icon(Icons
                                                                .date_range),
                                                            color: Colors.grey,
                                                            iconSize: 20,
                                                            onPressed: () {
                                                              setState(() {
                                                                DatePicker
                                                                    .showDatePicker(
                                                                    context,
                                                                    showTitleActions: true,
                                                                    minTime: DateTime(
                                                                        2000, 1,
                                                                        1),
                                                                    maxTime: DateTime(
                                                                        2050, 1,
                                                                        1),
                                                                    onChanged: (
                                                                        date) {
                                                                      setState(() {
                                                                        _controller =
                                                                        new TextEditingController(
                                                                            text: date
                                                                                .year
                                                                                .toString() +
                                                                                "-" +
                                                                                date
                                                                                    .month
                                                                                    .toString() +
                                                                                "-" +
                                                                                date
                                                                                    .day
                                                                                    .toString());
                                                                        realdate =
                                                                            date;
                                                                      });
                                                                      print(
                                                                          'change $date');
                                                                    },
                                                                    onConfirm: (
                                                                        date) {
                                                                      setState(() {
                                                                        _controller =
                                                                        new TextEditingController(
                                                                            text: date
                                                                                .year
                                                                                .toString() +
                                                                                "-" +
                                                                                date
                                                                                    .month
                                                                                    .toString() +
                                                                                "-" +
                                                                                date
                                                                                    .day
                                                                                    .toString());
                                                                        realdate =
                                                                            date;
                                                                      });
                                                                      print(
                                                                          'confirm $date');
                                                                    },
                                                                    onCancel: () {},
                                                                    currentTime: DateTime
                                                                        .now(),
                                                                    locale: _changeLocal(
                                                                        context));
                                                              });
                                                            },
                                                          ),
                                                          new Expanded(
                                                            child: TextField(
                                                              controller: _controller,
                                                              enabled: false,
                                                              keyboardType: TextInputType
                                                                  .datetime,
                                                              decoration: InputDecoration(

                                                                hintText: "Pick Date"
                                                                    .tr()
                                                                    .toString(),
                                                                hintStyle: TextStyle(
                                                                    color: Colors
                                                                        .grey),
                                                                contentPadding:
                                                                EdgeInsets
                                                                    .symmetric(
                                                                    vertical: 8,
                                                                    horizontal: 8),
                                                                isDense: true,
                                                              ),
                                                              onTap: () {
                                                                DatePicker
                                                                    .showDatePicker(
                                                                    context,
                                                                    showTitleActions: true,
                                                                    minTime: DateTime(
                                                                        2000, 1,
                                                                        1),
                                                                    maxTime: DateTime(
                                                                        2050, 1,
                                                                        1),
                                                                    onChanged: (
                                                                        date) {
                                                                      print(
                                                                          'change $date');
                                                                    },
                                                                    onConfirm: (
                                                                        date) {
                                                                      print(
                                                                          'confirm $date');
                                                                    },
                                                                    currentTime: DateTime
                                                                        .now(),
                                                                    locale: _changeLocal(
                                                                        context));
                                                              },
                                                              style: TextStyle(
                                                                fontSize: 14.0,
                                                                color: Colors
                                                                    .black,
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .height *
                                                      0.13,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        controller: installerNameController,
                                                        decoration: InputDecoration(
                                                          hintText: 'installer name'
                                                              .tr()
                                                              .toString(),
                                                          hintStyle: TextStyle(
                                                            color: Colors.grey,
                                                          ),
                                                          border: InputBorder
                                                              .none,
                                                          icon: Icon(Icons
                                                              .person),
                                                        ),
                                                      ),
                                                      TextField(
                                                        enabled: true,
                                                        controller: installerNumberController,
                                                        decoration: InputDecoration(
                                                          hintText: 'installer number',
                                                          hintStyle: TextStyle(
                                                            color: Colors.grey,
                                                          ),
                                                          border: InputBorder
                                                              .none,
                                                          icon: Icon(Icons
                                                              .phone),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),

                                                Container(

                                                  height: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .height * 0.06,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  child: DropdownButton(
                                                    hint: Text(
                                                      (_dropstatus == "") ?
                                                      'status'.tr().toString()
                                                          : _dropstatus,
                                                      style: TextStyle(
                                                          color: Colors.blue),
                                                    ),
                                                    isExpanded: true,
                                                    iconSize: 15.0,
                                                    style: TextStyle(
                                                        color: Colors.blue),
                                                    items: [
                                                      'working'.tr().toString(),
                                                      'not_working'
                                                          .tr()
                                                          .toString()
                                                    ].map(
                                                          (val) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: val,
                                                          child: Text(val),
                                                        );
                                                      },
                                                    ).toList(),
                                                    onChanged: (val) {
                                                      setState(() {
                                                        _dropstatus = val;
                                                      },
                                                      );
                                                    },
                                                  ),
                                                ),

                                                RaisedButton(
                                                  child: Text(
                                                    'save'.tr().toString(),
                                                  ),
                                                  onPressed: () async
                                                  {
                                                    Asansor newAsansor = new Asansor(
                                                        buildingNo: temp
                                                            .buildingNumber,
                                                        location: temp.location,
                                                        phoneNo: telephoneController
                                                            .text,
                                                        buildingname: temp
                                                            .buildingName);

                                                    if (!installerNumberController.text.isEmpty)
                                                      newAsansor
                                                          .installerPhone =
                                                          installerNumberController
                                                              .text;
                                                    if (!installerNameController.text.isEmpty)
                                                      newAsansor.installerName =
                                                          installerNameController
                                                              .text;
                                                    if (!kimlikController.text.isEmpty)
                                                      newAsansor.kimlikNo =
                                                          kimlikController.text;
                                                    if (!_dropstatus.isEmpty)
                                                      newAsansor.status =
                                                          _dropstatus;
                                                    _awaitReturnValueFromSecondScreen(context, newAsansor);
                                                    await Navigator.of(context)
                                                        .pop();
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        });
                      });
                    }
                  },
                ),
              ),
            ],
          ),
          copyRights(),
        ],
      ),
    );
  }

  LocaleType _changeLocal(BuildContext context) {
    if (EasyLocalization
        .of(context)
        .locale == Locale('ar', 'SY')) {
      localetype = LocaleType.ar;
    }
    else if (EasyLocalization
        .of(context)
        .locale == Locale('en', 'US')) {
      localetype = LocaleType.en;
    }
    else if (EasyLocalization
        .of(context)
        .locale == Locale('tr', 'TR')) {
      localetype = LocaleType.tr;
    }
    else {
      localetype = LocaleType.de;
    }
    return localetype;
  }

  Future<Response> putRequest(InProgressLift temp) async {

    for(Stage s in temp.stages)
      {
        s.desc.replaceAll('\n', '\\n');
      }
    Uri url = Uri.https(
        consts.domain,
        consts.domain_inProgress + temp.id.toString());


    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        Fluttertoast.showToast(
            msg: "Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

      }
      else
      {
        Fluttertoast.showToast(
            msg: "Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      Navigator.pop(context);
    });
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context, Asansor temp) async {
    postAsansorRequest(temp);
      Maintainance m1 = new Maintainance();
      m1.liftNumber = temp.kimlikNo;
      m1.description = 'Engine';
      m1.dateTime = manipulateDate(realdate, 0);
      m1.done = '0';
      m1.employeeName = temp.installerName;
      m1.location = temp.location;
      post3Maintenance(m1);
      Maintainance m2 = new Maintainance();
      m2.liftNumber = temp.kimlikNo;
      m2.description = 'Cabine';
      m2.dateTime = manipulateDate(realdate, 1);
      m2.done = '0';
      m2.employeeName = temp.installerName;
      m2.location = temp.location;
      post3Maintenance(m2);
      Maintainance m3 = new Maintainance();
      m3.liftNumber = temp.kimlikNo;
      m3.description = 'ًWell';
      m3.dateTime = manipulateDate(realdate, 2);
      m3.done = '0';
      m3.employeeName = temp.installerName;
      m3.location = temp.location;
      post3Maintenance(m3);
    Navigator.popAndPushNamed(context, "/");
  }


  String manipulateDate(DateTime date, int month) {
    return date.year.toString() +
        "-" +
        (date.month + month).toString() +
        "-" +
        date.day.toString();
  }

  Future<void> post3Maintenance(Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances);
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await post(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        Fluttertoast.showToast(
            msg: "Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

      }
      else
      {
        Fluttertoast.showToast(
            msg: "Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      Navigator.pop(context);
    });
  }

  Future<Response> postAsansorRequest (Asansor temp) async {
      Uri url = Uri.https(
          consts.domain ,consts.domain_lifts);

      var body = temp.toJson();
      var encodedbody = json.encode(body);
      Map<String, String> headers = {'Content-Type': 'application/json'};

      await post(url,
          headers: headers,
          body: encodedbody
      ).then((Response response) {
        print("Response status: ${response.statusCode}");
        print("Response body: ${response.contentLength}");
        print(response.headers);
        print(response.request);
        if(response.statusCode == 200)
        {
          Fluttertoast.showToast(
              msg: "Success".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );

        }
        else
        {
          Fluttertoast.showToast(
              msg: "Failed".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
        Navigator.pop(context);
      });
  }

}
