import 'package:asansorci/employees/Employee.dart';
import 'package:asansorci/inProgress_short/SubStage.dart';
import 'package:flutter/cupertino.dart';

class Stage {
  List<SubStage> substages;
  String name;
  List<Employee> employees;
  Employee supervisor;
  String desc="";
  TextEditingController controller;

  Stage({this.substages, this.name, this.desc}) {
    employees = new List<Employee>();
    substages = new List<SubStage>();
    controller = new TextEditingController();
  }

}
