import 'dart:convert';

import 'package:asansorci/inProgress_short/Stage.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';

class InProgressLift {
  int id;
  List<Stage> stages = [];
  String buildingNumber;
  String fakekimlik;
  int currentStage;
  Position location;
  String buildinginfo;
  String lifttype;
  String buildingName;
  String comittedname;
  String comittedphone;
  Color color;

  InProgressLift(
      {
      this.currentStage,
      this.buildingNumber,
      this.buildingName,
      this.comittedname,
      this.comittedphone,
      this.id,
      this.buildinginfo,
      this.lifttype,
      this.location,
      this.fakekimlik}){
    for(int  i =0 ; i < 12; i++)
      {
        this.stages.add(new Stage());
      }
  }

  toJson()=>{
    "id":this.id,
    "buildingname":this.buildingName,
    "buildingNo":this.buildingNumber,
    "currentStage":this.currentStage,
    "latitude":this.location.latitude,
    "longitude":this.location.longitude,
    "buildingapt":this.buildinginfo,
    "comittedName":this.comittedname,
    "comittedNumber":this.comittedphone,
    "lifttype":this.lifttype,
    for(int i =0; i < stages.length;i++)
      "des"+(i+1).toString():this.stages[i].desc,
    "fakekimlik":this.fakekimlik
  };

  InProgressLift fromJson(Map<String, dynamic> parsedJson)
  {
    InProgressLift temp = new InProgressLift();
    var codeUnits = parsedJson["buildingname"].toString().codeUnits;
    temp.buildingName = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["buildingNo"].toString().codeUnits;
    temp.buildingNumber = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["comittedName"].toString().codeUnits;
    temp.comittedname = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["comittedNumber"].toString().codeUnits;
    temp.comittedphone = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["fakekimlik"].toString().codeUnits;
    temp.fakekimlik = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["buildingapt"].toString().codeUnits;
    temp.buildinginfo = Utf8Decoder().convert(codeUnits);
    temp.location = new Position(latitude: parsedJson["latitude"] , longitude: parsedJson["longitude"]);
    codeUnits = parsedJson["lifttype"].toString().codeUnits;
    temp.lifttype = Utf8Decoder().convert(codeUnits);
    temp.currentStage = parsedJson["currentStage"];
    temp.id = parsedJson["id"];

    for(int j =0;j< temp.stages.length;j++) {
      int counter = j+1;
      String des = "des"; des += counter.toString();
      if (parsedJson[des] != null) {
        codeUnits = parsedJson[des].toString().codeUnits;
        temp.stages[j].desc =  Utf8Decoder().convert(codeUnits);
      }
      else
        temp.stages[j].desc = "";
    }

    return temp;
  }

  @override
  String toString() {
    return 'InProgressLift{id: $id, stages: $stages, buildingNumber: $buildingNumber, fakekimlik: $fakekimlik, currentStage: $currentStage, location: $location, buildinginfo: $buildinginfo, lifttype: $lifttype, buildingName: $buildingName, comittedname: $comittedname, comittedphone: $comittedphone, color: $color}';
  }
}
