import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:asansorci/inProgress_short/Stage.dart';
import 'package:asansorci/inProgress_short/SubStage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'InProgressLift.dart';

class StageView extends StatefulWidget {
  @override
  _StageViewState createState() => _StageViewState();
}

class _StageViewState extends State<StageView> {
  List<Stage> stages = [];

  Employee super1 =
      Employee(name: "HaciAli Atalar", id: "1", speciality: "Engineer");
  Employee super2 = Employee(name: "Ali Kara", id: "2", speciality: "Engineer");
  Employee super3 =
      Employee(name: "Mahmoud Fadel", id: "3", speciality: "Engineer");

  List<Employee> employees1 = [
    Employee(name: "Mohammad Fadel", id: "123456", speciality: "Doors"),
    Employee(name: "Berkay Cengiz", id: "123465", speciality: "Indexer"),
    Employee(name: "Mahir Odeh", id: "123457", speciality: "Rail"),
  ];

  List<Employee> employees2 = [
    Employee(
        name: "Turgay Jabbar", id: "223456", speciality: "Engine Installer"),
    Employee(name: "Ahmet Buyuk", id: "323465", speciality: "Cabine Installer"),
  ];

  List<Employee> employees3 = [
    Employee(
        name: "Ahmad Aziza", id: "623456", speciality: "Electric Specialist"),
    Employee(
        name: "Jawed Khan", id: "723465", speciality: "Control Specialist"),
  ];

  List<SubStage> substages1 = [
    SubStage(
        name: "Doors",
        description: "Installing The Doors",
        date: DateTime.now(),
        done: true),
    SubStage(
        name: "Indexes",
        description: "Installing The Indexes",
        date: DateTime.now(),
        done: true),
    SubStage(
        name: "Rails",
        description: "Installing The Rails",
        date: DateTime.now(),
        done: false)
  ];

  List<SubStage> substages2 = [
    SubStage(
        name: "Engine",
        description: "Installing The Engine",
        date: DateTime.now(),
        done: true),
    SubStage(
        name: "Cabine",
        description: "Installing The Cabine",
        date: DateTime.now(),
        done: true),
  ];

  List<SubStage> substages3 = [
    SubStage(
        name: "Electricity",
        description: "Installing The Electricity Equs",
        date: DateTime.now(),
        done: true),
    SubStage(
        name: "Controls",
        description: "Installing The Controls",
        date: DateTime.now(),
        done: true),
  ];

  Stage Stage1 = Stage(name: "First Stage");
  Stage Stage2 = Stage(name: "Second Stage");
  Stage Stage3 = Stage(name: "Third Stage");

  _StageViewState() {
    Stage1.employees.addAll(employees1);
    Stage1.substages.addAll(substages1);
    Stage1.supervisor = super1;

    Stage2.employees.addAll(employees2);
    Stage2.substages.addAll(substages2);
    Stage2.supervisor = super2;

    Stage3.employees.addAll(employees3);
    Stage3.substages.addAll(substages3);
    Stage3.supervisor = super3;
  }

  int _currentStep = 0;
  int tempo;

  InProgressLift temp;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (tempo == null) {
      try {
        Map<String, Object> rcvdData =
            ModalRoute.of(context).settings.arguments;
        if (rcvdData['InProgressLifte'] != null) {
          temp = rcvdData['InProgressLifte'];
          tempo = temp.currentStage;
          print(_currentStep);
        }
      } catch (Exception) {}
    }
    _currentStep = tempo;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        isDetails: true,
        isHome: false,
        isLang: false,
      ),
      body: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.0,
                    MediaQuery.of(context).size.height * 0.02,
                    MediaQuery.of(context).size.width * 0.8,
                    0),
                child: Text(
                  'Fake ID'.tr().toString(),
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.06,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.grey, // set border color
                      width: 0.5,
                      style: BorderStyle.solid), // set border width
                  // set rounded corner radius
                ),
                child: TextField(
                  enabled: false,
                  decoration: InputDecoration(
                    hintText: '',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    border: InputBorder.none,
                    icon: Icon(Icons.insert_drive_file),
                  ),
                ),
              ),
            ],
          ),
          Divider(
            indent: 5,
            endIndent: 10,
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.665,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Stepper(
                    type: StepperType.vertical,
                    physics: ClampingScrollPhysics(),
                    onStepTapped: (step) {
                      setState(() {
                        _currentStep = step;
                        tempo = step;
                      });
                    },
                    onStepCancel: () {
                      if (_currentStep >= 0)
                        setState(() {
                          _currentStep -= 1;
                          tempo -= 1;
                        });
                    },
                    onStepContinue: () {
                      if (_currentStep < 2)
                        setState(() {
                          _currentStep += 1;
                          tempo += 1;
                        });
                    },
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) {
                      return Row(
                        children: <Widget>[
                          RaisedButton(
                            color: (_currentStep == 2)
                                ? Colors.green
                                : Colors.blueAccent, // background
                            textColor: Colors.white, // foreground
                            onPressed: () {
                              onStepContinue();
                            },
                            child: (_currentStep == 2)
                                ? Text('complete'.tr().toString())
                                : Text('continue'.tr().toString()),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          RaisedButton(
                            color: Colors.grey, // background
                            textColor: Colors.black45, // foreground
                            onPressed: () {
                              onStepCancel();
                            },
                            child: Text(
                              'back'.tr().toString(),
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        ],
                      );
                    },
                    currentStep: _currentStep,
                    steps: <Step>[
                      new Step(
                        title: new Text(Stage1.name.tr().toString()),
                        content: Column(
                          children: <Widget>[
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: this.Stage1.substages.length,
                              itemBuilder: (context, index) {
                                return _buildRow(Stage1, index);
                              },
                            ),
                          ],
                        ),
                        isActive: _currentStep >= 0,
                        state: _currentStep >= 0
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      new Step(
                        title: new Text(Stage2.name.tr().toString()),
                        content: Column(
                          children: <Widget>[
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: this.Stage2.substages.length,
                              itemBuilder: (context, index) {
                                return _buildRow(Stage2, index);
                              },
                            ),
                          ],
                        ),
                        isActive: _currentStep >= 0,
                        state: _currentStep >= 1
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      new Step(
                        title: new Text(Stage3.name.tr().toString()),
                        content: Column(
                          children: <Widget>[
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: this.Stage3.substages.length,
                              itemBuilder: (context, index) {
                                return _buildRow(Stage3, index);
                              },
                            ),
                          ],
                        ),
                        isActive: true,
                        state: _currentStep >= 2
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                    ],
                  ),
                  Divider(
                    indent: 5,
                    endIndent: 10,
                  ),
                  Column(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.65,
                                0),
                            child: Text(
                              'building_number'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              enabled: false,
                              decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.house_outlined),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.8,
                                0),
                            child: Text(
                              'building_name'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              enabled: false,
                              decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.house),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.65,
                                0),
                            child: Text(
                              'Comitted Phone'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              enabled: false,
                              decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.phone),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.65,
                                0),
                            child: Text(
                              'Comitted Name'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey, // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              enabled: false,
                              decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.person),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ),
    );
  }

  Widget _buildRow(Stage temp, index) {
    return CheckboxListTile(
      title: Row(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.0,
                0.0,
                MediaQuery.of(context).size.width * 0.04,
                0.0),
            margin: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.0, 0.0, 0.0, 0.0),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
            ),
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return SingleChildScrollView(
                        child: Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width * 0.9,
                            color: Colors.black45,
                            child: Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    color: Colors.white24,
                                    child: SizedBox.expand(
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.06,
                                              margin: EdgeInsets.all(10),
                                              padding: EdgeInsets.fromLTRB(
                                                  10.0, 0, 0, 0),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    // set border color
                                                    width: 0.5,
                                                    style: BorderStyle
                                                        .solid), // set border width
                                                // set rounded corner radius
                                              ),
                                              child: TextField(
                                                enabled: false,
                                                decoration: InputDecoration(
                                                  hintText:
                                                      temp.supervisor.name,
                                                  hintStyle: TextStyle(
                                                    color: Colors.grey,
                                                  ),
                                                  border: InputBorder.none,
                                                  icon:
                                                      Icon(Icons.emoji_people),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.06,
                                              margin: EdgeInsets.all(10),
                                              padding: EdgeInsets.fromLTRB(
                                                  10.0, 0, 0, 0),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    // set border color
                                                    width: 0.5,
                                                    style: BorderStyle
                                                        .solid), // set border width
                                                // set rounded corner radius
                                              ),
                                              child: TextField(
                                                enabled: false,
                                                decoration: InputDecoration(
                                                  hintText: temp
                                                      .substages[index].date
                                                      .toString(),
                                                  hintStyle: TextStyle(
                                                    color: Colors.grey,
                                                  ),
                                                  border: InputBorder.none,
                                                  icon: Icon(Icons
                                                      .date_range_outlined),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.2,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.8,
                                              margin: EdgeInsets.all(10),
                                              alignment: Alignment.center,
                                              // padding: EdgeInsets.fromLTRB(
                                              //     10.0, 0, 0, 0),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    // set border color
                                                    width: 0.5,
                                                    style: BorderStyle
                                                        .solid), // set border width
                                                // set rounded corner radius
                                              ),
                                              child: Wrap(
                                                spacing: 8.0,
                                                // gap between adjacent chips
                                                runSpacing: 4.0,
                                                // gap between lines
                                                direction: Axis.horizontal,
                                                // main axis (rows or columns)
                                                children: <Widget>[
                                                  for (var emp
                                                      in temp.employees)
                                                    InputChip(
                                                      padding:
                                                          EdgeInsets.all(2.0),
                                                      avatar: CircleAvatar(
                                                        backgroundColor: Colors
                                                            .blue.shade600,
                                                        child: Text(emp.name[0]
                                                            .toUpperCase()),
                                                      ),
                                                      label: Text(emp.name),
                                                      //selected: _isSelected,
                                                      selectedColor:
                                                          Colors.green,
                                                      onSelected:
                                                          (bool selected) {
                                                        setState(() {
                                                          //_isSelected = selected;
                                                        });
                                                      },
                                                      // onPressed: () {
                                                      //   //
                                                      // },
                                                      onDeleted: () {
                                                        //
                                                      },
                                                    ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.3,
                                              margin: EdgeInsets.all(10),
                                              padding: EdgeInsets.fromLTRB(
                                                  10.0, 0, 0, 0),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    // set border color
                                                    width: 0.5,
                                                    style: BorderStyle
                                                        .solid), // set border width
                                                // set rounded corner radius
                                              ),
                                              child: TextField(
                                                maxLines: null,
                                                enabled: true,
                                                decoration: InputDecoration(
                                                  hintText: temp
                                                      .substages[index]
                                                      .description,
                                                  hintStyle: TextStyle(
                                                    color: Colors.grey,
                                                  ),
                                                  border: InputBorder.none,
                                                  icon: Icon(Icons.info),
                                                ),
                                              ),
                                            ),
                                            RaisedButton(
                                              color: Colors.white12,
                                              child: Text(
                                                'Okay'.tr().toString(),
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              onPressed: () =>
                                                  {Navigator.of(context).pop()},
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              },
              child: Icon(
                Icons.info_outline,
                size: 30.0,
              ),
            ),
          ),
          Text(temp.substages[index].name.tr().toString()),
        ],
      ),
      value: temp.substages[index].done,
      onChanged: (val) {
        setState(
          () {
            temp.substages[index].done = !temp.substages[index].done;
          },
        );
      },
    );
  }
}
