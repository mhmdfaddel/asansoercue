
import 'dart:convert';

import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class CreateEmployee extends StatefulWidget {
  @override
  _CreateEmployeeState createState() => _CreateEmployeeState();
}

class _CreateEmployeeState extends State<CreateEmployee> {
  String appTitle = 'ASANSÖRCÜ';

  Employee temp;
  Constants consts = new Constants();
  TextEditingController empNameController = TextEditingController();
  TextEditingController empPhoneController = TextEditingController();
  TextEditingController empSpeciController = TextEditingController();
  TextEditingController empUsernameController = TextEditingController();
  TextEditingController empPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: myAppBar(
        isHome: false,
        isDetails: false,
        title: appTitle,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8155,
            width: MediaQuery.of(context).size.width * 1,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.height * 0.02,
                      MediaQuery.of(context).size.width * 0.0,
                      0),
                  child: Text(
                    "Create New Employee".tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02,
                              MediaQuery.of(context).size.width * 0.75,
                              0),
                          child: Text(
                            "Employee Name".tr().toString(),
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey,
                                // set border color
                                width: 0.5,
                                style: BorderStyle.solid), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            controller: empNameController,
                            decoration: InputDecoration(
                              hintText: "Employee Name".tr().toString(),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.emoji_people),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02,
                              MediaQuery.of(context).size.width * 0.6,
                              0),
                          child: Text(
                            "Employee Username".tr().toString(),
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey,
                                // set border color
                                width: 0.5,
                                style: BorderStyle.solid), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            controller: empUsernameController,
                            decoration: InputDecoration(
                              hintText: "Employee Username".tr().toString(),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.phone_android_outlined),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02,
                              MediaQuery.of(context).size.width * 0.6,
                              0),
                          child: Text(
                            "Employee Password".tr().toString(),
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey,
                                // set border color
                                width: 0.5,
                                style: BorderStyle.solid), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            controller: empPasswordController,
                            decoration: InputDecoration(
                              hintText: "Employee Password".tr().toString(),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.phone_android_outlined),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02,
                              MediaQuery.of(context).size.width * 0.63,
                              0),
                          child: Text(
                            "Employee Phone Number".tr().toString(),
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey,
                                // set border color
                                width: 0.5,
                                style: BorderStyle.solid), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            controller: empPhoneController,
                            decoration: InputDecoration(
                              hintText:
                                  "Employee Phone Number".tr().toString(),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.phone_android_outlined),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02,
                              MediaQuery.of(context).size.width * 0.6,
                              0),
                          child: Text(
                            "Employee Speciality".tr().toString(),
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey,
                                // set border color
                                width: 0.5,
                                style: BorderStyle.solid), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            controller: empSpeciController,
                            decoration: InputDecoration(
                              hintText: "Employee Speciality".tr().toString(),
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.build),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

              ],
            ),
          ),
          copyRights(),
        ],
      ),
      floatingActionButton: FloatingActionButton(

        backgroundColor: Colors.green,
        child: Icon(Icons.add),
        onPressed: (){
          int i = 1;
          temp = new Employee(name: empNameController.value.text,
              speciality: empSpeciController.value.text,
              active: i,username: empUsernameController.value.text,
              password: empPasswordController.value.text, phone: empPhoneController.value.text );

          postRequest(temp);
        },
      ),
    );
  }

  Future<http.Response> postRequest (Employee temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_employee);

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.post(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        Fluttertoast.showToast(
            msg: "Done".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.popAndPushNamed(context, "/");
      }
      else
        {
          Fluttertoast.showToast(
              msg: "Failed".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
    });
  }

  double _getHeight(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*.8155;
    }
    return MediaQuery.of(context).size.height*.81;
  }
}
