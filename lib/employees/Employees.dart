import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class Employees extends StatefulWidget {
  @override
  _EmployeesState createState() => _EmployeesState();
}

class _EmployeesState extends State<Employees> {
  final String appTitle = 'ASANSÖRCÜ';
  String _dropDownValue;

  Future<void> _launched;

  List<Employee> employees1 = [];
  List<Employee> temps = [];
  Constants consts = new Constants();
  TextEditingController search = new TextEditingController();

  void getEmployees() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_employee);
      http.Response response = await client.get(url);
      List data = jsonDecode(response.body);
      employees1 = data.map((e) => new Employee().fromJson(e)).toList();
      temps.addAll(employees1);
      setState(() {});
    } finally {
      client.close();
    }
  }


  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        employees1.clear();
        getEmployees();
      });
    print('refreshed');
    _refreshController.loadComplete();
  }

  List<Employee> existEmployee(String temp, String cirtic) {
    List<Employee> testo = new List<Employee>();

    if(cirtic == 'Employee Name'.tr().toString())
      for (int i = 0; i < employees1.length; i++) {
        if (employees1[i].name.contains(temp)) {
          testo.add(employees1[i]);
        }
      }
    else if(cirtic == 'Employee ID'.tr().toString()){
      for (int i = 0; i < employees1.length; i++) {
        if (employees1[i].id.contains(temp)) {
          testo.add(employees1[i]);
        }
      }
    }
    else if(cirtic == 'Employee Phone Number'.tr().toString()){
      for (int i = 0; i < employees1.length; i++) {
        if (employees1[i].phone.contains(temp)) {
          testo.add(employees1[i]);
        }
      }
    }
    return testo;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmployees();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Employees".tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid),
                borderRadius:
                    BorderRadius.all(Radius.circular(50)), // set border width
                // set rounded corner radius
              ),
              child: TextField(
                controller: search,
                  onChanged: (value) =>{
                    setState(() {
                      if(this.search.value.text == "")
                      {
                        employees1 = temps;
                      }
                      else if(_dropDownValue == 'Employee Name'.tr().toString())
                      {
                        employees1 = existEmployee(search.value.text, 'Employee Name'.tr().toString());
                      }
                      else if(_dropDownValue == 'Employee Phone Number'.tr().toString())
                      {
                        employees1 = existEmployee(search.value.text, 'Employee Phone Number'.tr().toString());
                      }
                      else if(_dropDownValue == 'Employee ID'.tr().toString())
                      {
                        employees1 = existEmployee(search.value.text, 'Employee ID'.tr().toString());
                      }
                    })
                  },
                decoration: InputDecoration(
                  hintText: "Search Employee".tr().toString(),
                  hintStyle: TextStyle(
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.02,
                  MediaQuery.of(context).size.width * 0.02,
                  0,
                  MediaQuery.of(context).size.width * 0.02),
              child: Row(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.width * 0.47,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text("search_by".tr().toString())
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: Colors.blue),
                              ),
                        isExpanded: true,
                        iconSize: 15.0,
                        style: TextStyle(color: Colors.blue),
                        items: [
                          "Employee ID".tr().toString(),
                          "Employee Name".tr().toString(),
                          "Employee Phone Number".tr().toString()
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val;
                            },
                          );
                        },
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.2, 0, 0, 0),
                    child: Text(
                      "total".tr().toString() +
                          ' : ' +
                          this.employees1.length.toString(),
                      //+this.myLifts.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                  ? MediaQuery.of(context).size.height * 0.5685
                  : MediaQuery.of(context).size.height * 0.5874,
              width: MediaQuery.of(context).size.width * 0.95,
              child: Row(
                children: [
                  Container(
                    child: Expanded(
                      child: ListView.builder(
                        itemCount: this.employees1.length,
                        itemBuilder: (context, index) =>
                            this._buildRow(employees1[index]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed: () {
          setState(() {
            Navigator.pushNamed(context, '/createEmployee');
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildRow(Employee temp) {
    return GestureDetector(
      onTap: () {
        setState(() {
          Navigator.pushNamed(context, '/employeeDetails',
                              arguments: {"Employee": temp});
        });
      },
      child: Card(
        shadowColor: Colors.black45,
        margin: const EdgeInsets.all(10),
        elevation: 15.0,
        color: (temp.active == 1)
            ? Colors.blue[400]
            : Colors.grey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.644,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(
                        5.0,
                        MediaQuery.of(context).size.height * 0.02,
                        0,
                        MediaQuery.of(context).size.height * 0.02),
                    child: GestureDetector(
                        onTap: () {},
                        child: RichText(
                          text: TextSpan(
                            text: "Employee ID".tr().toString() + " : ",
                            style: TextStyle(color: Colors.black87),
                            children: [
                              TextSpan(
                                text: temp.id,
                              )
                            ],
                          ),
                        )

                        // Text(
                        //   "Employee ID".tr().toString() +" : "+ temp.id,
                        //   style: TextStyle(),
                        // ),
                        ),
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(
                          5.0, 0, 0, MediaQuery.of(context).size.height * 0.02),
                      child: GestureDetector(
                          onTap: () {},
                          child: RichText(
                            text: TextSpan(
                              text: "Employee Name".tr().toString() + " : ",
                              style: TextStyle(color: Colors.black87),
                              children: [
                                TextSpan(
                                  text: temp.name,
                                )
                              ],
                            ),
                          ))),
                  Padding(
                      padding: EdgeInsets.fromLTRB(
                          5.0, 0, 0, MediaQuery.of(context).size.height * 0.02),
                      child: GestureDetector(
                          onTap: () {},
                          child: RichText(
                            text: TextSpan(
                              text: "Employee Phone Number".tr().toString() +
                                  " : ",
                              style: TextStyle(color: Colors.black87),
                              children: [
                                TextSpan(
                                  text: temp.phone.toString(),
                                )
                              ],
                            ),
                          ))),
                  Padding(
                      padding: EdgeInsets.fromLTRB(
                          5.0,
                          0,
                          MediaQuery.of(context).size.width * 0.0,
                          MediaQuery.of(context).size.height * 0.02),
                      child: GestureDetector(
                          onTap: () {},
                          child: RichText(
                            text: TextSpan(
                              text:
                                  "Employee Speciality".tr().toString() + " : ",
                              style: TextStyle(color: Colors.black87),
                              children: [
                                TextSpan(
                                  text: temp.speciality.toString(),
                                )
                              ],
                            ),
                          ))),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.11,
                        0.0,
                        0.0,
                        0.0),
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.208,
                    decoration: BoxDecoration(

                      border: Border(
                        left: BorderSide(
                          color: Colors.grey,
                          width: 3.0,
                        ),
                        bottom: BorderSide(
                          //                   <--- left side
                          color: Colors.grey,
                          width: 3.0,
                        ),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          employees1.remove(temp);
                          deleteRequest(temp);
                        });
                      },
                      child: Icon(
                        Icons.delete,
                        size: 30.0,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.11,
                        0.0,
                        0.0,
                        0.0),
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.21,
                    decoration: BoxDecoration(

                      border: Border(
                        left: BorderSide(
                          //                   <--- left side
                          color: Colors.grey,
                          width: 3.0,
                        ),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        _launched = _makePhoneCall("tel:" + temp.phone);
                      },
                      child: Icon(
                        Icons.call,
                        size: 30.0,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<http.Response> deleteRequest (Employee temp) async {
    Uri url = Uri.https(
       consts.domain , consts.domain_employee+temp.id);

    http.Response res = await http.delete(url);
    if(res.statusCode == 200)
      {
        print('Deleted');
      }
    else
      {
        print('Error');
      }
  }

  double _getHeight(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*.5685;
    }
    return MediaQuery.of(context).size.height*.585;
  }

}
