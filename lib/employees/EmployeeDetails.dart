import 'dart:convert';

import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class EmployeeDetails extends StatefulWidget {
  @override
  _EmployeeDetailsState createState() => _EmployeeDetailsState();
}

class _EmployeeDetailsState extends State<EmployeeDetails> {
  String appTitle = 'ASANSÖRCÜ';

  TextEditingController empNameController = TextEditingController();
  TextEditingController empPhoneController = TextEditingController();
  TextEditingController empSpeciController = TextEditingController();
  TextEditingController empUsernameController = TextEditingController();

  Employee temp;
  Constants consts = new Constants();
  var _dropDownValue;
  var _dropDownCaption;
  var _dropRoleValue, _dropRoleCaption;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, Object> rcvdData = ModalRoute.of(context).settings.arguments;
    if (rcvdData["Employee"] != null) temp = rcvdData["Employee"];
    _dropDownCaption = temp.active;
    _dropRoleCaption = temp.role;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        isHome: false,
        isDetails: false,
        title: appTitle,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8155,
            width: MediaQuery.of(context).size.width * 1,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.0,
                      MediaQuery.of(context).size.height * 0.02,
                      MediaQuery.of(context).size.width * 0.0,
                      0),
                  child: Text(
                    "Employee".tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.54,
                                0),
                            child: Text(
                              "Employee Username".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey,
                                  // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              controller: empUsernameController,
                              decoration: InputDecoration(
                                hintText: temp.username,
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.phone_android_outlined),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.74,
                                0),
                            child: Text(
                              "Employee Name".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey,
                                  // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              controller: empNameController,
                              decoration: InputDecoration(
                                hintText: temp.name,
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.phone_android_outlined),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.54,
                                0),
                            child: Text(
                              "Employee Phone Number".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey,
                                  // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              controller: empPhoneController,
                              decoration: InputDecoration(
                                hintText: temp.phone,
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.phone_android_outlined),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.6,
                                0),
                            child: Text(
                              "Employee Speciality".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.06,
                            margin: EdgeInsets.all(10),
                            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey,
                                  // set border color
                                  width: 0.5,
                                  style: BorderStyle.solid), // set border width
                              // set rounded corner radius
                            ),
                            child: TextField(
                              enabled: true,
                              controller: empSpeciController,
                              decoration: InputDecoration(
                                hintText: temp.speciality,
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                                icon: Icon(Icons.build),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.1,
                                MediaQuery.of(context).size.height * 0.0,
                                MediaQuery.of(context).size.width * 0.0,
                                0),
                            child: Text(
                              "enabled".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                              height: MediaQuery.of(context).size.height * 0.04,
                              width: MediaQuery.of(context).size.width * 0.47,
                              padding: EdgeInsets.fromLTRB(
                                  MediaQuery.of(context).size.width * 0.1,
                                  0,
                                  0,
                                  0),
                              child: DropdownButton(
                                hint: _dropDownValue == null
                                    ? Text("status".tr().toString())
                                    : Text(
                                        _dropDownValue,
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                isExpanded: true,
                                iconSize: 15.0,
                                style: TextStyle(color: Colors.blue),
                                items: [
                                  "active".tr().toString(),
                                  "inactive".tr().toString(),
                                ].map(
                                  (val) {
                                    return DropdownMenuItem<String>(
                                      value: val,
                                      child: Text(val),
                                    );
                                  },
                                ).toList(),
                                onChanged: (val) {
                                  setState(
                                    () {
                                      _dropDownValue = val;
                                      if (val == 'active')
                                        _dropDownCaption = 1;
                                      else
                                        _dropDownCaption = 0;
                                      print(val);
                                    },
                                  );
                                },
                              )),
                        ],
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.1,
                                MediaQuery.of(context).size.height * 0.01,
                                MediaQuery.of(context).size.width * 0.0,
                                0),
                            child: Text(
                              "Role".tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Container(
                              height: MediaQuery.of(context).size.height * 0.04,
                              width: MediaQuery.of(context).size.width * 0.47,
                              padding: EdgeInsets.fromLTRB(
                                  MediaQuery.of(context).size.width * 0.1,
                                  0,
                                  0,
                                  0),
                              child: DropdownButton(
                                hint: _dropRoleValue == null
                                    ? Text("Role".tr().toString())
                                    : Text(
                                  _dropRoleValue,
                                  style: TextStyle(color: Colors.blue),
                                ),
                                isExpanded: true,
                                iconSize: 15.0,
                                style: TextStyle(color: Colors.blue),
                                items: [
                                  "ADMIN".tr().toString(),
                                  "USER".tr().toString(),
                                ].map(
                                      (val) {
                                    return DropdownMenuItem<String>(
                                      value: val,
                                      child: Text(val),
                                    );
                                  },
                                ).toList(),
                                onChanged: (val) {
                                  setState(
                                        () {
                                      _dropRoleValue = val;
                                    },
                                  );
                                },
                              )),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: (EasyLocalization.of(context).locale ==
                          Locale('ar', 'SY'))
                      ? EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.7,
                          MediaQuery.of(context).size.height * 0.0,
                          MediaQuery.of(context).size.width * 0.0,
                          0)
                      : EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.7,
                          MediaQuery.of(context).size.height * .01,
                          MediaQuery.of(context).size.width * 0.0,
                          0),
                  child: RaisedButton(
                    color: Colors.green,
                    elevation: 12,
                    child: Text(
                      'Save'.tr().toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () => {
                      setState(() {
                        Employee posted = Employee(
                            role: _dropRoleValue,
                            id: temp.id,
                            name: (empNameController.value.text != "")?empNameController.value.text:temp.name,
                            username: (empUsernameController.value.text != "")?empUsernameController.value.text:temp.username,
                            active: _dropDownCaption,
                            phone: (empPhoneController.value.text != "")?empPhoneController.value.text:temp.phone,
                            speciality: (empSpeciController.value.text!= "")?empSpeciController.value.text:temp.speciality);
                        putRequest(posted);
                        Navigator.pop(context);
                      })
                      //Navigator.of(context).pop()
                    },
                  ),
                ),
              ],
            ),
          ),
          copyRights(),
        ],
      ),
    );
  }

  Future<Response> putRequest(Employee temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_employee + temp.id);

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await put(url,
            headers: headers,
            //encoding: Encoding.getByName("application/json"),
            body: encodedbody)
        .then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
    });
  }
  double _getHeight(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*.8155;
    }
    return MediaQuery.of(context).size.height*.81;
  }
}
