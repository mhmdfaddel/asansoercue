import 'dart:convert';

class Employee {
  String name;
  String id;
  int active;
  String speciality;
  String username;
  String role;
  String password;
  String phone;

  Employee({this.name, this.id, this.speciality, this.phone, this.active, this.username, this.role, this.password});


  Map toRole(String id, String role) =>
        {
          'id': id,
          'name': role
        };

  Map toJson() =>
      {
        'name': this.name,
        'id': this.id,
        'speciality': this.speciality,
        'phoneNumber': this.phone,
        'username':this.username,
        'active':this.active.toString(),
        'roles': 'USER'
      };

  Employee fromJson(Map<String, dynamic> parsedJson){
    Employee temp = new Employee();
    var codeUnits = parsedJson["employee_id"].toString().codeUnits;
    temp.id = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["name"].codeUnits;
    temp.name = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["phoneNumber"].toString().codeUnits;
    temp.phone = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["speciality"].toString().codeUnits;
    temp.speciality = Utf8Decoder().convert(codeUnits);
    temp.active = parsedJson["active"];
    codeUnits = parsedJson["username"].toString().codeUnits;
    temp.username = Utf8Decoder().convert(codeUnits);
    return temp;
  }
}
