import 'dart:async';
import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';

class MyMaps extends StatefulWidget {
  @override
  State createState() {
    // TODO: implement createState
    return MyMapsState();
  }
}

class MyMapsState extends State {
  final GlobalKey scaffoldKey = GlobalKey();

  Constants consts = new Constants();
  Completer _controller = Completer();
  Map<MarkerId, Marker> markers = {};
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  CameraPosition _kGooglePlex =
      CameraPosition(target: LatLng(38.9637, 35.2433), zoom: 5.1);
  List listMarkerIds = List();

  List<Asansor> myLifts = [];
  List<Asansor> myTemps= [];
  Placemark Country;

  //final MarkerId markerId = MarkerId("current");

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() async {
        _currentPosition = position;
        _kGooglePlex = await CameraPosition(
          target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          zoom: 14.0,
        );
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng(Position position) async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          position.latitude, position.longitude);
      Placemark place = p[0];
      setState(() {
        Country = place;
        // _currentAddress =
        // "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getLifts();
    getInProgressLifts();
    super.initState();
    _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: scaffoldKey,
        appBar: myAppBar(
          isLang: false,
          isDetails: false,
          isHome: false,
        ),
        body: Container(
          child: GoogleMap(
            onCameraMove:(CameraPosition cameraPosition){
              print(cameraPosition.zoom);
            },
            initialCameraPosition: _kGooglePlex,
            onTap: (_) {},
            mapType: MapType.normal,
            markers: Set.of(markers.values),
            onMapCreated: (GoogleMapController controler) {
              _controller.complete(controler);
              setState(() {
                for(int i =0;i < myLifts.length;i++)
                {
                  MarkerId m = new MarkerId(i.toString());
                  _getAddressFromLatLng(myLifts[i].location);
                  Marker marker1 = Marker(
                      markerId: m,
                      position: LatLng(myLifts[i].location.latitude,myLifts[i].location.longitude ),
                      icon: myLifts[i].bitmap ,
                      infoWindow: InfoWindow(
                          title: myLifts[i].buildingname,
                          onTap: () {
                            var bottomSheetController = Scaffold.of(
                                scaffoldKey.currentContext)
                                .showBottomSheet((context) => Container(
                                child: getBottomSheet(myLifts[i].buildingNo,
                                  LatLng(myLifts[i].location.latitude, myLifts[i].location.longitude)),
                              height: 250,
                              color: Colors.transparent,
                            ));
                          },
                          snippet: ""));

                  listMarkerIds.add(m);
                  markers[m] = marker1;
                }
              });
            },
          ),
        ));
  }

  Widget getBottomSheet(String s, LatLng l) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 32),
          color: Colors.white,
          child: Column(
            children: [
              Container(
                color: Colors.blueAccent,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        s + "\n",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text("4.5",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12)),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Text("970 Folowers",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14))
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text("Memorial Park",
                          style: TextStyle(color: Colors.white, fontSize: 14)),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.map,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text("$s")
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.call,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text("040-123456")
                ],
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(
            alignment: Alignment.topRight,
            child: FloatingActionButton(
                child: Icon(Icons.navigation), onPressed: () {}),
          ),
        )
      ],
    );
  }

  Future<void> getLifts() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);
      print(data.toString());
      for (int i = 0; i < data.length; i++) {

        //List roles = jsonDecode(data[0]);

        Asansor temp = new Asansor();
        var codeUnits = data[i]["buildingname"].toString().codeUnits;
        temp.buildingname = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["buildingNo"].toString().codeUnits;
        temp.buildingNo = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["installerName"].toString().codeUnits;
        temp.installerName = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["installerPhone"].toString().codeUnits;
        temp.installerPhone = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["kimlikNo"].toString().codeUnits;
        temp.kimlikNo = Utf8Decoder().convert(codeUnits);
        temp.location = new Position(latitude: data[i]["latitude"] , longitude: data[i]["longitude"]);
        codeUnits = data[i]["phoneNo"].toString().codeUnits;
        temp.phoneNo = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["status"].toString().codeUnits;
        temp.status = Utf8Decoder().convert(codeUnits);
        temp.id = data[i]["lift_id"].toString();

        if(temp.status == "Working" || temp.status == "يعمل" || temp.status == "Funktioniert" || temp.status == "ÇALIŞIYOR")
          {
            temp.color = Colors.green;
            temp.bitmap = BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueGreen);
          }
        else if(temp.status == "in Progress" || temp.status == "قيد الإنشاء" || temp.status == "in Bearbeitung" || temp.status == "YAPIM AŞAMASINDA")
          {
            temp.color = Colors.green;
            temp.bitmap = BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueAzure);
          }
        else
          {
            temp.color = Colors.redAccent;
            temp.bitmap = BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueRed);
          }
        myLifts.add(temp);
      }
      myTemps.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }
  }
  Future<void> getInProgressLifts() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_inProgress);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);
      print(data.toString());
      for (int i = 0; i < data.length; i++) {

        //List roles = jsonDecode(data[0]);

        Asansor temp = new Asansor();
        temp.location = new Position(latitude: data[i]["latitude"] , longitude: data[i]["longitude"]);
        var codeUnits = data[i]["buildingname"].toString().codeUnits;
        temp.buildingname = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["buildingNo"].toString().codeUnits;
        temp.buildingNo = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["comittedName"].toString().codeUnits;
        temp.installerName = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["comittedNumber"].toString().codeUnits;
        temp.installerPhone = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["fakekimlik"].toString().codeUnits;
        temp.kimlikNo = Utf8Decoder().convert(codeUnits);


        temp.color = Colors.green;
          temp.bitmap = BitmapDescriptor.defaultMarkerWithHue(
              BitmapDescriptor.hueAzure);
        myLifts.add(temp);
      }
      myTemps.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }
  }

}
