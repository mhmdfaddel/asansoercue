import 'dart:ui';

import 'package:asansorci/Models/Lang.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flag/flag.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Language extends StatefulWidget {
  String appTitle;

  Language({Key key, this.appTitle}) : super(key: key);

  @override
  _LanguageState createState() => _LanguageState();
}

class _LanguageState extends State<Language> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    List<Lang> languages = new List();

    final String appTitle = 'ASANSÖRCÜ';
    languages.insert(
        0,
        new Lang(
            couple: Locale('en', 'US'),
            language_name: 'english'.tr().toString(),
            language_flag: 'us'));
    languages.insert(
        0,
        new Lang(
            couple: Locale('ar', 'SY'),
            language_name: 'arabic'.tr().toString(),
            language_flag: 'sy'));
    languages.insert(
        0,
        new Lang(
            couple: Locale('tr', 'TR'),
            language_name: 'turkish'.tr().toString(),
            language_flag: 'tr'));
    languages.insert(
        0,
        new Lang(
            couple: Locale('de', 'DE'),
            language_name: 'german'.tr().toString(),
            language_flag: 'de'));

    return Scaffold(
      appBar: myAppBar(
        title: this.widget.appTitle,
        isHome: false,
        isDetails: false,
        isLang: true,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.8155,
              padding: EdgeInsets.fromLTRB(
                  5.0, MediaQuery.of(context).size.height * 0.3, 0.0, 0.0),
              child: ListView.builder(
                itemCount: languages.length,
                itemBuilder: (context, index) =>
                    this._buildRow(languages[index]),
              ),
              //padding: EdgeInsets,
            ),
            copyRights(),
          ],
        ),
      ),
      //drawer: MyDrawer(),
    );
  }

  _buildRow(Lang temp) {
    return Card(
        shadowColor: Colors.black45,
        margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
        elevation: 15.0,
        color: Colors.grey,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.amber[800],
            onPrimary: Colors.grey,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                temp.language_name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Flag(temp.language_flag,
                      height: 30, width: 30, fit: BoxFit.fitWidth))
            ],
          ),
          onPressed: () {
            setState(() {
              //
              EasyLocalization.of(context).locale = temp.couple;
              Navigator.popAndPushNamed(context, "/");
            });
          },
        ));
  }
}
