import 'dart:convert';

class Test_Book {
  int id;
  String stageid, fakekimlik, lifttype;


  Test_Book({this.id, this.stageid, this.fakekimlik, this.lifttype});

  Map toJson()=>{
    "test_id":this.id,
    "stageid":this.stageid,
    "liftype":this.lifttype,
    "fakekimlik":this.fakekimlik
  };


  Test_Book fromJson(Map<String, dynamic> parsedJson)
  {
    Test_Book temp = new Test_Book();
    var codeUnits = parsedJson["test_id"].toString().codeUnits;
    temp.id = parsedJson["test_id"];
    codeUnits = parsedJson["fakekimlik"].toString().codeUnits;
    temp.fakekimlik = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["liftype"].toString().codeUnits;
    temp.lifttype = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["stageid"].toString().codeUnits;
    temp.stageid = Utf8Decoder().convert(codeUnits);
    return temp;
  }
  @override
  String toString() {
    return 'Test_Book{id: $id, stageid: $stageid, fakekimlik: $fakekimlik, lifttype: $lifttype}';
  }
}