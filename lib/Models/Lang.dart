import 'package:flutter/material.dart';

class Lang {
  String language_flag;
  Locale couple;
  String language_name;

  Lang({this.couple, this.language_name, this.language_flag});
}
