import 'package:flutter/material.dart';

class GridGroup extends StatelessWidget {
  GridGroup({
    Key key,
    @required this.funct,
    @required this.toGo,
  }) : super(key: key);

  String funct;
  String toGo;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1,
      width: MediaQuery.of(context).size.width * 0.6,
      color: Colors.pink,
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.00,
                    MediaQuery.of(context).size.height * 0.03,
                    0,
                    MediaQuery.of(context).size.height * 0.0),
                child: GestureDetector(
                  onTap: () {},
                  child: Text(
                    '$funct',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }
}
