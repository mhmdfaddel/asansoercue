import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class GridCard extends StatelessWidget {
  GridCard({
    Key key,
    @required this.funct,
    @required this.color,
    @required this.icon,
    @required this.toGo,
    @required this.cross,
  }) : super(key: key);

  String funct;
  Color color;
  IconData icon;
  String toGo;
  MainAxisAlignment cross;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1,
      width: MediaQuery.of(context).size.width * 1,
     // color: this.color,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //
        ),        // The child of a round Card should be in round shape
        gradient: (this.cross == MainAxisAlignment.start)?new LinearGradient(
            colors: [
              const Color(0xffffffff),
              this.color,
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp)
        :   new LinearGradient(
            colors: [
              this.color,
              const Color(0xffffffff),
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(3.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
          //color: this.color,
      ),
      child: buildItem(context, cross),
      );
  }

  Widget buildItem(BuildContext context,MainAxisAlignment mainaxis)
  {
    if(mainaxis == MainAxisAlignment.start)
      return Padding(
        padding: EdgeInsets.fromLTRB(
            MediaQuery.of(context).size.width * 0.06,
            MediaQuery.of(context).size.height * 0.00,
            MediaQuery.of(context).size.width * 0.00,
            MediaQuery.of(context).size.height * 0.0),
        child: Row(
          mainAxisAlignment: cross,
          children: [
            IconButton(
              icon: Icon(
                this.icon,
                color: Colors.white,
                size: 50.0,
              ),
              onPressed: () {
                Navigator.pushNamed(context, this.toGo);
              },
            ),
            Container(
              padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.06,
                  MediaQuery.of(context).size.height * 0.02,
                  0,
                  MediaQuery.of(context).size.height * 0.0),
              child: GestureDetector(
                onTap: () {},
                child: Text(
                  '$funct',
                  style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    else
      return Padding(
        padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.00,
                                MediaQuery.of(context).size.height * 0.00,
                                MediaQuery.of(context).size.width * 0.06,
                                MediaQuery.of(context).size.height * 0.0),
        child: Row(
          mainAxisAlignment: cross,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.00,
                  MediaQuery.of(context).size.height * 0.02,
                  MediaQuery.of(context).size.width * 0.08,
                  MediaQuery.of(context).size.height * 0.0),
              child: GestureDetector(
                onTap: () {},
                child: Text(
                  '$funct',
                  style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
                  overflow: TextOverflow.fade,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                this.icon,
                color: Colors.white,
                size: 50.0,
              ),
              onPressed: () {
                Navigator.pushNamed(context, this.toGo);
              },
            ),
          ],
        ),
      );
  }

}
