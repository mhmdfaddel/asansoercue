import 'dart:convert';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class Maintainance {
  String id;
  String dateTime;
  String description;
  String employeeName;
  String liftNumber;
  String liftType;
  String typeo;
  String done;
  Color color;
  Position location;
  IconData icon;
  String lift_kind;

  Maintainance(
      {this.dateTime,
      this.description,
      this.employeeName,
      this.liftNumber,
      this.typeo,
      this.id,
      this.done,
      this.location,
      this.liftType});

  Map toJson()=>{
    'done':this.done,
    'date':this.dateTime,
    'description':this.description,
    'lifte':this.liftNumber,
    'chargedEmployees': this.employeeName,
    'longitude':this.location.longitude,
    'latitude':this.location.latitude,
    'liftType':this.liftType,
    'lift_Kindo':this.lift_kind
  };

  Map toJsonEdit()=>{
    'maintenance_id':this.id,
    'done':this.done,
    'date':this.dateTime,
    'description':this.description,
    'lifte':this.liftNumber,
    'chargedEmployees': this.employeeName,
  };

  Maintainance fromJson(Map<String, dynamic> parsedJson){
    Maintainance temp = new Maintainance();
    if(parsedJson["description"] != null)
    {
      var codeUnits = parsedJson["description"]
          .toString()
          .codeUnits;
      temp.description = Utf8Decoder().convert(codeUnits);
    }
    if(parsedJson["chargedEmployees"] != null)
    {
      var _codeUnits = parsedJson["chargedEmployees"].toString().codeUnits;
      temp.employeeName = Utf8Decoder().convert(_codeUnits);
    }
    if(parsedJson["date"] != null)
    {
      var codeUnits_1 = parsedJson["date"].toString().codeUnits;
      temp.dateTime = Utf8Decoder().convert(codeUnits_1);
    }

    {
      var codeUnits = parsedJson["lifte"].toString().codeUnits;
      temp.liftNumber = Utf8Decoder().convert(codeUnits);
    }
    {
      var codeUnits = parsedJson["maintenance_id"].toString().codeUnits;
      temp.id = Utf8Decoder().convert(codeUnits);
    }
    {
      var codeUnits = parsedJson["liftType"].toString().codeUnits;
      temp.liftType = Utf8Decoder().convert(codeUnits);
    }
    {
      var codeUnits = parsedJson["lift_Kindo"].toString().codeUnits;
      temp.lift_kind = Utf8Decoder().convert(codeUnits);
    }
    temp.done = parsedJson["done"];
    Position p = new Position(latitude: parsedJson["latitude"], longitude: parsedJson["longitude"]);
    temp.location = p;
    if(temp.liftType == "Well")
      {
        temp.icon = Icons.arrow_downward_sharp;
      }
    else if(temp.liftType == "Cabine")
      {
        temp.icon = Icons.door_sliding_outlined;
      }
    else if (temp.liftType == "Engine")
      {
        temp.icon = Icons.settings;
      }

    if(temp.done == "1")
      temp.color = Colors.green;
    else
      temp.color = Colors.red;
    return temp;
  }

  @override
  String toString() {
    return 'Maintainance{dateTime: $dateTime, description: $description, employeeName: $employeeName, liftNumber: $liftNumber, liftType: $liftType, typeo: $typeo, done: $done, color: $color, location: $location}';
  }
}
