import 'dart:convert';

import 'package:asansorci/inProgress_Standard/stages/SS1.dart';
import 'package:asansorci/inProgress_Standard/stages/SS2.dart';
import 'package:asansorci/inProgress_Standard/stages/SS3.dart';
import 'package:asansorci/inProgress_Standard/stages/SS4.dart';
import 'package:asansorci/inProgress_Standard/stages/SS5.dart';
import 'package:asansorci/inProgress_Standard/stages/SS6.dart';
import 'package:asansorci/inProgress_Standard/stages/SS7.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/material/colors.dart';
import 'package:geolocator/geolocator.dart';


class ProgressClass {

  String id;

  SS1 ss1 = new SS1();
  SS2 ss2 = new SS2();
  SS3 ss3 = new SS3();
  SS4 ss4 = new SS4();
  SS5 ss5 = new SS5();
  SS6 ss6 = new SS6();
  SS7 ss7 = new SS7();

  int currentIndex;

  String fakeId;
  String buildingNumber;
  String buildingName;
  String buildingApts;
  String liftType;
  String committedName;
  String committedNumber;
  String notes;

  Position location;

  Color color;


  @override
  String toString() {
    return 'ProgressClass{id: $id, ss1: $ss1, ss2: $ss2, ss3: $ss3, ss4: $ss4, ss5: $ss5, ss6: $ss6, ss7: $ss7, currentIndex: $currentIndex, fakeId: $fakeId, buildingNumber: $buildingNumber, buildingName: $buildingName, buildingApts: $buildingApts, liftType: $liftType, committedName: $committedName, committedNumber: $committedNumber, notes: $notes, location: $location, color: $color}';
  }

  ProgressClass(
      {this.ss1,
        this.ss2,
        this.ss3,
        this.ss4,
        this.ss5,
        this.ss6,
        this.ss7,
        this.id,
        this.fakeId,
        this.location,
        this.buildingApts,
        this.buildingName,
        this.buildingNumber,
        this.committedName,
        this.committedNumber,
        this.currentIndex,
        this.liftType,
        this.notes
      });
  
  ProgressClass fromJson(Map<String, dynamic> parsedJson)
  {
    ProgressClass temp = new ProgressClass();

    var codeUnits = parsedJson["buildingname"]
        .toString()
        .codeUnits;
    temp.buildingName = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["buildingNo"]
        .toString()
        .codeUnits;
    temp.buildingNumber = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["comittedName"]
        .toString()
        .codeUnits;
    temp.committedName = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["comittedNumber"]
        .toString()
        .codeUnits;
    temp.committedNumber = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["fakekimlik"]
        .toString()
        .codeUnits;
    temp.fakeId = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["buildingapt"]
        .toString()
        .codeUnits;
    temp.buildingApts = Utf8Decoder().convert(codeUnits);
    temp.location = new Position(
        latitude: parsedJson["latitude"], longitude: parsedJson["longitude"]);
    codeUnits = parsedJson["lifttype"]
        .toString()
        .codeUnits;
    temp.liftType = Utf8Decoder().convert(codeUnits);


    return temp;
  }
}