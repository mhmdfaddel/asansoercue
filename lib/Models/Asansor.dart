import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'Maintainance.dart';

class Asansor {
  String id;
  String kimlikNo;
  String phoneNo;
  String buildingNo;
  String buildingname;
  String installerName;
  String installerPhone;
  String liftkind;
  List<Maintainance> maintanances;
  Color color;
  List jsonList;
  bool taken;
  Icon icon;
  BitmapDescriptor bitmap;

  Position location;
  List<Maintainance> operations = new List();
  String status;

  Asansor(
      {this.installerName,
      this.kimlikNo,
      this.buildingNo,
      this.location,
      this.status,
      this.installerPhone,
      this.phoneNo,
      this.buildingname,
      this.id,
      this.operations,
      this.liftkind});

  Map toJson()=>{
    'buildingNo':this.buildingNo,
    'buildingname':this.buildingname,
    'installerName':this.installerName,
    'installerPhone':this.installerPhone,
    'kimlikNo':this.kimlikNo,
    'longitude':this.location.longitude,
    'latitude':this.location.latitude,
    'phoneNo':this.phoneNo,
    'status':this.status,
    'liftkind':this.liftkind,
    'blockede':this.taken,
    'maintenances':this.jsonList
  };

  Asansor fromJson(Map<String, dynamic> parsedJson){
    Asansor temp = new Asansor();
    var codeUnits = parsedJson["buildingname"].toString().codeUnits;
    temp.buildingname = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["buildingNo"].toString().codeUnits;
    temp.buildingNo = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["installerName"].toString().codeUnits;
    temp.installerName = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["installerPhone"].toString().codeUnits;
    temp.installerPhone = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["kimlikNo"].toString().codeUnits;
    temp.kimlikNo = Utf8Decoder().convert(codeUnits);
    temp.location = new Position(latitude: parsedJson["latitude"] , longitude: parsedJson["longitude"]);
    codeUnits = parsedJson["phoneNo"].toString().codeUnits;
    temp.phoneNo = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["status"].toString().codeUnits;
    temp.status = Utf8Decoder().convert(codeUnits);
    temp.id = parsedJson["lift_id"].toString();
    codeUnits = parsedJson["liftkind"].toString().codeUnits;
    temp.liftkind = Utf8Decoder().convert(codeUnits);
    temp.taken = parsedJson["blockede"];
    if(!temp.taken)
      temp.icon = new Icon(Icons.add,
        size: 30.0,
        color: Colors.white,);

    temp.color = Colors.white;
    temp.maintanances = (parsedJson["maintenances"] as List).map((e) => new Maintainance().fromJson(e)).toList();
    return temp;
  }
}
