
import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class Group {
  String id;
  String name;
  List<Asansor> lifts;
  Position location;
  List jsonlifts;
  Color color;
  String startdate, enddate;
  bool done;
  int doneLifts;

  Group(
      {this.name,this.lifts, this.location,this.id});

  Map toJson()=>{
    'name':this.name,
    'start_date':this.startdate,
    'end_date':this.enddate,
    'done':this.done,
    'lifts':this.jsonlifts
  };
  
  Group fromJson(Map<String, dynamic> parsedJson)
  {
    Group temp = new Group();

    var codeUnits = parsedJson["name"].toString().codeUnits;
    String name = Utf8Decoder().convert(codeUnits);
    codeUnits = parsedJson["groupe_id"].toString().codeUnits;
    String id = Utf8Decoder().convert(codeUnits);
    temp.id = id;
    if(name.isNotEmpty)
      temp.name = name;
    else
      temp.name = "---------";
    codeUnits = parsedJson["start_date"].toString().codeUnits;
    if(codeUnits != null) {
      String startdate = Utf8Decoder().convert(codeUnits);
      temp.startdate = startdate;

    }
    codeUnits = parsedJson["end_date"].toString().codeUnits;
    if(codeUnits != null) {
      String enddate = Utf8Decoder().convert(codeUnits);
      temp.enddate = enddate;

    }

    int donelifts = parsedJson["done_lifts"];
    temp.doneLifts = donelifts;
    bool done = parsedJson["done"];
    temp.done = done;
    var list = parsedJson['lifts'] as List;
    temp.lifts = [];
    temp.lifts = list
        .map((data) => new Asansor().fromJson(data))
        .toList();
    DateTime tempDate;
    if(temp.enddate != null && temp.enddate.isNotEmpty)
       {
         tempDate = new DateFormat("yyyy-MM-dd").parse(temp.enddate);
         if(tempDate.isBefore(DateTime.now()) && !temp.done)
         {
           temp.color = Colors.red;
         }
         else
           temp.color = Colors.white;
       }
    else
      {
        temp.color = Colors.red;
      }

    return temp;
  }

  @override
  String toString() {
    return 'Group{id: $id, name: $name, location: $location}';
  }
}
