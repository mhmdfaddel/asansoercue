import 'package:asansorci/Models/GridCard.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 20.0,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color(0xFF0CA8F1),
            ),
            child: Text(
              'UMUT ASANSÖRÜ',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          ListTile(
            title: Text('lifts'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/lifts');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('list_of_maintenance'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/maintenOps');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('InProgresslifts'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/inProgressLiftsView');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Tests Book'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/StandardView');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Employees'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/employees');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Groups'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/groups');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Hyper View'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/hypeView');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Maps'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/maps');
            },
          ),

          Divider(indent: 0,endIndent: 10,),

          ListTile(
            title: Text('Contact Us & Report Bugs'.tr().toString()),
            onTap: () {
              Navigator.pushNamed(context, '/contactus');
            },
          ),
        ],
      ),
    );
  }
}
