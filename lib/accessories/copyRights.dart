import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class copyRights extends StatelessWidget {
  copyRights({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.08,
      width: MediaQuery.of(context).size.width,
      child: Container(
        color: Colors.black45,
        child: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.05,
                      MediaQuery.of(context).size.width * 0.05,
                      MediaQuery.of(context).size.width * 0.010,
                      MediaQuery.of(context).size.width * 0.05),
                  child: Icon(
                    Icons.copyright_outlined,
                    size: 20.0,
                    color: Colors.white,
                  ),
                ),
              ),
              WidgetSpan(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.00001,
                      MediaQuery.of(context).size.height * 0.003,
                      MediaQuery.of(context).size.width * 0.0125,
                      MediaQuery.of(context).size.height * 0.023),
                  child: Text(
                    'UMUT ASANSÖR 2021',
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
