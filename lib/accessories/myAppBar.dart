import 'package:asansorci/Pages/contactus/ContactUs.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class myAppBar extends StatefulWidget implements PreferredSizeWidget {
  myAppBar({
    Key key,
    this.title,
    this.isHome,
    this.isDetails,
    this.isLang
  })  : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  static const routeName = '/langs';

  @override
  final Size preferredSize;
  final bool isHome;
  final String title;
  final bool isDetails;
  final bool isLang;

  @override
  _myAppBarState createState() => _myAppBarState();
}

void handleClick(String value) {}

class _myAppBarState extends State<myAppBar> {
  final String appTitle = 'UMUT ASANSÖR';

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        appTitle,
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.white,
        ),
      ),
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.white),
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                if (widget.isHome) {
                  Navigator.pushNamed(context, '/langs');
                } else if (widget.isDetails) {}
              },
              child: (widget.isHome)
                  ? Icon(
                      Icons.language,
                      color: Colors.white,
                      size: 25.0,
                    )
                  : (widget.isLang)
              ? GestureDetector(
                    onTap: (){
                      Navigator.popAndPushNamed(context, "/");
                    },
                        child: Icon(
                            Icons.home,
                            color: Colors.white,
                             size: 25.0,
                          ),
                      )
              : Text(""),
            )),
        if (widget.isDetails)
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {'delete'.tr().toString()}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
      ],
    );
  }
}
