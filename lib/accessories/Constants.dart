class Constants{
  final String password = "umut_asansor1234";
  final String domain = "umutapp.me";
  final String domain_employee = "/rest-service-0.0.1-SNAPSHOT/employees/";
  final String domain_groups = "/rest-service-0.0.1-SNAPSHOT/groups/";
  final String domain_lifts = "/rest-service-0.0.1-SNAPSHOT/lifts/";
  final String domain_maintainances = "/rest-service-0.0.1-SNAPSHOT/maintenances/";
  final String domain_test_book = "/rest-service-0.0.1-SNAPSHOT/test_book/";
  final String domain_inProgress = "/rest-service-0.0.1-SNAPSHOT/in_progress/";

}