import 'dart:convert';

import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/Pages/HyView/Controller.dart';
import 'package:asansorci/Pages/contactus/ContactUs.dart';
import 'package:asansorci/Pages/groups/CreateGroup.dart';
import 'package:asansorci/Pages/groups/GroupDetails.dart';
import 'package:asansorci/Pages/groups/GroupDetails1.dart';
import 'package:asansorci/Pages/groups/GroupMaintainance.dart';
import 'package:asansorci/Pages/groups/Groups.dart';
import 'package:asansorci/Pages/maintenances/CreateMaintainance.dart';
import 'package:asansorci/Pages/maintenances/MaintenanceDetails.dart';
import 'package:asansorci/Pages/maintenances/MaintenanceOperations.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/employees/EmployeeDetails.dart';
import 'package:asansorci/employees/Employees.dart';
import 'package:asansorci/inProgress_Standard/ProgressStandardView.dart';
import 'package:asansorci/inProgress_Standard/Standard_view.dart';
import 'package:asansorci/inProgress_short/InProgressLiftView.dart';
import 'package:asansorci/inProgress_short/InProgrssLiftsView.dart';
import 'package:asansorci/inProgress_short/StageView.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:asansorci/maps/MyMaps.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart';
import 'package:overlay_support/overlay_support.dart';
import 'Languages/Language.dart';
import 'Pages/Asansors/AsansorDetails.dart';
import 'Pages/Asansors/CreateAsansor.dart';
import 'Pages/Home.dart';
import 'Pages/Asansors/Lifts.dart';
import 'employees/CreateEmployee.dart';


 const simplePeriodicTask = "Umut Asansör";
//
// // flutter local notification setup

int i=0;
void showNotification( v, flp) async {

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
Time t = new Time();


   var android = AndroidNotificationDetails(
       (i++).toString(), 'channel NAME', 'CHANNEL DESCRIPTION',
       priority: Priority.high, importance: Importance.max);
   var iOS = IOSNotificationDetails();
   var platform = NotificationDetails(android: android, iOS: iOS);

  await flp.show(i++, 'Maintenance Notification'.tr().toString(), '$v', platform,
      payload: 'VIS \n $v');
}

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();

  callbackDispatcher();

  runApp(EasyLocalization(
    child: InitialHome(),
    supportedLocales: [
      Locale('en', 'US'),
      Locale('tr', 'TR'),
      Locale('ar', 'SY'),
      Locale('de', 'DE'),
    ],
    startLocale: Locale('tr', 'TR'),
    fallbackLocale: Locale('tr','TR'),
    path: 'lib/langs',
    saveLocale: true,
  ));

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}
String desc, emplo,dateo,kimlik,id, lifttype;
List<Maintainance>myMaintenance = [];
Constants consts = new Constants();



Future<void> getMaintenance() async {
  var client = new Client();
  try {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances);
    Response response = await client.get(url);
    List data = jsonDecode(response.body);
    print(data.toString());
    for (int i = 0; i < data.length; i++) {

      //List roles = jsonDecode(data[0]);
      if(data[i]["description"] != null)
      {
        var codeUnits = data[i]["description"]
            .toString()
            .codeUnits;
        desc = Utf8Decoder().convert(codeUnits);
      }
      if(data[i]["chargedEmployees"] != null)
      {
        var _codeUnits = data[i]["chargedEmployees"].toString().codeUnits;
        emplo = Utf8Decoder().convert(_codeUnits);
      }
      if(data[i]["date"] != null)
      {
        var codeUnits_1 = data[i]["date"].toString().codeUnits;
        dateo = Utf8Decoder().convert(codeUnits_1);
      }

      {
        var codeUnits = data[i]["lifte"].toString().codeUnits;
        kimlik = Utf8Decoder().convert(codeUnits);
      }
      {
        var codeUnits = data[i]["maintenance_id"].toString().codeUnits;
        id = Utf8Decoder().convert(codeUnits);
      }
      {
        var codeUnits = data[i]["liftType"].toString().codeUnits;
        lifttype = Utf8Decoder().convert(codeUnits);
      }

      Maintainance temp = new Maintainance(
          id: id,
          dateTime: dateo,
          description: desc,
          liftNumber: kimlik,
          done: data[i]["done"],
          employeeName: emplo,
          liftType: lifttype
        //Get The Lift ID ALSO
      );
      if(temp.done == "1")
        temp.color = Colors.green;
      else
        temp.color = Colors.blueAccent;

      myMaintenance.add(temp);
    }
  } finally {
    client.close();
  }  }


void callbackDispatcher() async {
    // await getMaintenance();
    // FlutterLocalNotificationsPlugin flp = FlutterLocalNotificationsPlugin();
    // var android = AndroidInitializationSettings('@mipmap/launcher_icon');
    // var iOS = IOSInitializationSettings();
    // var initSetttings = InitializationSettings(android: android, iOS: iOS);
    // flp.initialize(initSetttings);
    // for(Maintainance m in myMaintenance)
    // {
    //   DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(m.dateTime);
    //   print(tempDate);
    //
    //   if(tempDate.isBefore(DateTime.now()) && m.done != "1")
    //   {
    //     String msg = "The Maintenance in the Building is forgotten ".tr().toString();
    //     showNotification(msg+' '+m.liftNumber, flp);
    //   }
    // }

}


class InitialHome extends StatefulWidget {
  const InitialHome({Key key}) : super(key: key);

  @override
  _InitialHomeState createState() => _InitialHomeState();
}

class _InitialHomeState extends State<InitialHome> {
  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
      child: MaterialApp(
        themeMode: ThemeMode.dark,
        darkTheme: ThemeData(
          brightness: Brightness.dark,
        ),
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        debugShowCheckedModeBanner: false,
        debugShowMaterialGrid: false,
        initialRoute: '/',
        routes: {
          '/': (context) => Home(),
          '/lifts': (context) => Lifts(),
          '/createLift': (context) => CreateAsansor(),
          '/createMainten': (context) => CreateMaintainance(),
          '/maintenOps': (context) => MaintenanceOperations(),
          '/asansorDetails': (context) => AsansorDetails(),
          '/employeeDetails': (context) => EmployeeDetails(),
          '/maintenanceDetails': (context) => MaintenanceDetails(),
          '/langs': (context) => Language(),
          '/employees': (context) => Employees(),
          '/createEmployee': (context) => CreateEmployee(),
          '/stageView': (context) => StageView(),
          '/inProgressLiftsView': (context) => InProgressLiftsView(),
          '/inProgressLiftView': (context) => InProgressLiftView(),
          '/maps':(context)=> MyMaps(),
          '/groups': (context)=> Groups(),
          '/createGroup': (context) => CreateGroup(),
          '/groupDetails': (context) => GroupDetails1(),
          '/groupLiftMaintain':(context)=> GroupMaintainance(),
          '/hypeView':(context)=> TabBarController(),
          '/StandardView':(context)=> ProgressStandardView(),
          '/standard_stage':(context)=> StandardView(),
          '/contactus':(context)=> ContactUs(),
        },
      ),
    );
  }


}
