import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../SubStageColored.dart';


class SS1 extends StatefulWidget {
   SS1({Key key, this.test_id}) : super(key: key)
   {
     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "makine dairesine ulaşımda merdivenin etrafında 1,5 m yatay mesafede düşme engellenmelidir. (merdiven boyu min 3m ise)"),
       new SubStageColored(name: "MD geçiş yolunda 50 lüx aydınlatma olmalıdır."),
       new SubStageColored(name: "MD ulaşım için kullanılan merdivenin en üst ucunda en az bir tutamak bulunmalıdır"),
     ];
     List<SubStageColored> ss1_2= [
       new SubStageColored(name: "MD kapıları deliksiz ve kat kapıları ile aynı mukavemette olmalıdır. (yanmamalıdır.)"),
       new SubStageColored(name: "MD giriş kapıları 200*60 cm olmalıdır. A3'te bu ölçü 180*60 cm olmalıdır."),
       new SubStageColored(name: "Makara dairesi giriş kapıları en az 140*60 cm olmalıdır."),
       new SubStageColored(name: "Makine/ Makara mekanlarına girişte kullanılan döşeme kapakları 80*80 cm olmalıdır. Ve üzerinde uyarıcı levha bulunmalıdır ve deliksiz olmalıdır."),
       new SubStageColored(name: "makine/makara dairesi kapıları içe doğru açılmamalıdır."),
       new SubStageColored(name: "MD kapı/ kapakları dışarıdan kilitlenememeli ve içeriden anahtarsız açılabilmelidir"),
     ];

     List<SubStageColored> ss1_4=[
       new SubStageColored(name: "Makine/makara dairesi aydınlatması döşeme seviyesinde min. 200 lüx sabit aydınlatma olmalıdır."),
     ];

     List<SubStageColored> ss1_8=[
       new SubStageColored(name: "kumanda panosunda bulunan ekipmanların üzerinde adreslemesi olmalıdır."),
       new SubStageColored(name: "hız regülatörü üzerinde imal yılı firması ve ce işareti olmalıdır."),
       new SubStageColored(name: "asansör bakım ve kayıt defteri bulunmalıdır."),
       new SubStageColored(name: "birden fazla asansörün kuyusu ve Mdsi ortaksa pano, regülatör, elektrik panosu,kabin vb. parçaları işaretlenmelidir."),
       new SubStageColored(name: "ana anahtar, priz, aydınlatma anahtarı adreslenmelidir."),
       new SubStageColored(name: "elektrikli elle kurtarma kumandalarında (normal,revizyon,aşağı,yukarı (varsa) ortak) işaretlemesi yapılmalıdır."),
     ];

     List<SubStageColored> ss1_9=[
       new SubStageColored(name: "kablo veya halatların geçtiği delikler min. Genişlikte olmalı ve halat sürtmemelidir. Etrafındaki koruma yüksekliği min. 5 cm olmalıdır."),
       new SubStageColored(name: "Mdndeki farklı seviyedeki döşemeler arasında en az 50 cm fark varsa 70 cmlik korkuluk yapılmalıdır."),
     ];

     List<SubStageColored> ss1_10=[
       new SubStageColored(name: "Makine platformuna çıkan merdiven sabit olmalıdır."),
       new SubStageColored(name: "makine platformuna çıkan merdivenin en az bir yanında tutamak olmalıdır."),
     ];

     List<SubStageColored> ss1_11=[
       new SubStageColored(name: "hareketli parçaların bakımı ve elle acil kurtarma için gerekli olan yerlerde 50*60 cmlik bir serbest yatay alan bulunmalıdır."),
       new SubStageColored(name: "geçiş yolları en az 50 cm genişliğinde olmalıdır. Hareketli parçaların olmadığı yerlerde bu genişlik 40 cm'e kadar düşebilir."),
       new SubStageColored(name: "MD'nde geçiş yolları üzerindeki serbest yükseklik min. 180 cm olmalıdır."),
       new SubStageColored(name: "MD'nde özellikle çalışma alanları üstünde en az 210 cm serbest yükseklik bulunmalıdır."),
       new SubStageColored(name: "kumanda panosunun önünde 50*70 cm'lik bir alan bulunmalıdır"),
     ];

     List<SubStageColored> ss1_16=[
       new SubStageColored(name: "tahrik makinası montaj civatalarına kontra somun veya yaylı rondela takılmalıdır."),
       new SubStageColored(name: "tahrik makinası elektromekanik fren tiji gevşek konra somunları sıkılmalıdır."),
       new SubStageColored(name: "fren kolu elle açılabilmeli ve bırakıldığında kendiliğinde kapanmalıdır."),
       new SubStageColored(name: "motor kablo girişleri rekorlanmalı veya izole edilmelidir."),
       new SubStageColored(name: "motor terminal bağlantı kapağı takılmalıdır."),
       new SubStageColored(name: "makine sehpasının duvara/betona teması engellenmelidir."),
       new SubStageColored(name: "makine sehpasının mntajındaki dengesizlikler giderilmelidir. (sehpaya çapraz atılacak.)"),
     ];

     List<SubStageColored> ss1_17=[
       new SubStageColored(name: "tahrik makinası elektromekanik freni her yön için seri iki kontaktörden enerjilendirilmelidir."),
     ];

     List<SubStageColored> ss1_18=[
       new SubStageColored(name: "tahrik kasnağının duvara ve zemine teması önlenmelidir."),
       new SubStageColored(name: "tahrik kasnağı çıkışında halatların birbirine teması önlenmelidir."),
     ];

     List<SubStageColored> ss1_19=[
       new SubStageColored(name: "saptırma kasnağının duvara ve zemine teması önlenmelidir."),

     ];

     List<SubStageColored> ss1_20=[
       new SubStageColored(name: "makaralı şalter olmalıdır. (ce işareti)")
     ];

     List<SubStageColored> ss1_31=[
       new SubStageColored(name: "Kuyu aydınlatma sigortası takılmalı ve adreslenmelidir."),
       new SubStageColored(name: "Elektrik panosunda motor hattı için 4lü grup W otomat otomat takılmalıdır."),
       new SubStageColored(name: "4lü grup W otomatın beslemesi kaçak akım rölesinden sonra olmalıdır."),
       new SubStageColored(name: "MD aydınlatma sigortası takılmalı ve adreslenmelidir."),
       new SubStageColored(name: "Kabin aydınlatma sigortası takılmalı ve adreslenmelidir"),

     ];

     List<SubStageColored> ss1_33=[
       new SubStageColored(name: "50 V AC'den daha büyük gerilim altında olan kontrol devrelerine sahip kat kumanda ve kat göstergeleri ile emniyet devrelerinde hata akımına karşı koruma (30 mA)"),
       new SubStageColored(name: "50 V AC'den daha büyük gerilim altında çalışan asansör kabini üzerindeki devrelerde hata akımına karşı koruma (30 mA)"),
       new SubStageColored(name: "Kabin ve kuyu aydınlatmasına dahil priz devreleri (30 mA)"),
       new SubStageColored(name: "Güç devresi ve buna bağlı devrelerin ana şalterinde eşik değeri topraklama direncine bağlı olarak seçilen ve uygulanan hata akımına karşı koruma"),
     ];

     List<SubStageColored> ss1_34=[
       new SubStageColored(name: "elektrik kuvvet panosu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "hidrolik tamponların topraklama bağlantıları yapılmalıdır."),
       new SubStageColored(name: "kabin topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "topraklama kablo bağlantıları yüksük veya civatalı veya kablo pabucu ile yapılmalıdır."),
       new SubStageColored(name: "kumanda panosu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "makine motor grubu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "hız regülatörü topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "sınır kesici şalter topraklama bağlantısı yapılmalıdır.(3 faz sınır kesiciler için geçerlidir.)"),
       new SubStageColored(name: "kabinve kat butonyer topraklama bağlantısı yapılmalıdır."),
     ];

     List<SubStageColored> ss1_35=[
       new SubStageColored(name: "kumanda panosunda bulunan tüm kompenentler panoya sabitlenmelidir." ),
       new SubStageColored(name: "kumanda panosu kompenent rumuzları devre şemasına uygun hale getirilmelidir." ),
       new SubStageColored(name: "kumanda panosu kablo kanal kapakları kapatılmalıdır."),
       new SubStageColored(name: "kumanda panosu sabitlenmelidir."),
     ];

     List<SubStageColored> ss1_38=[
       new SubStageColored(name: "by-pass işareti yada yazısı olmalı ve elektrik diyagramı bulunmalıdır."),
     ];

     List<SubStageColored> ss1_44=[
       new SubStageColored(name: "hız regülatörü kuyu içinde ise dışarıdan erişilebilir olmalıdır.50*60 cm'lik bir yatay alan olmalıdır."),
       new SubStageColored(name: "hız regülatörü sehpası sabitlenmelidir."),
       new SubStageColored(name: "hız regülatör makarasının duvara teması engellenmelidir." ),
       new SubStageColored(name: "hız regülatörü emniyet kontağı sabitlenmelidir" ),
     ];

     ss1 = [
       new SubStageColored(number: 1.1, color: Colors.blueAccent[400], name: "Makina ve makara dairesine güvenli erişim", subs: ss1_1),
       new SubStageColored(number: 1.2, color: Colors.blueAccent[400], name:"Makine veya makara dairesi giriş kapısı (kilit, açılma yönü ve uyarı levhası)", subs: ss1_2),
       new SubStageColored(number: 1.3,color: Colors.yellowAccent[400], name:"Kurtarma talimatı *(Türkçe)"),
       new SubStageColored(number: 1.4,color: Colors.yellowAccent[400], name:"Makina ve makara dairesinde yeterli aydınlatma*",subs: ss1_4),
       new SubStageColored(number: 1.5, color: Colors.blueAccent[400], name:"Makina veya makara dairesinde kaymayan zemin "),
       new SubStageColored(number: 1.6, color: Colors.blueAccent[400], name:"Yeterli havalandırma (atmosfere açılmalı ve binadan gelen pis hava makine/makara mekanına girmemelidir.)"),
       new SubStageColored(number: 1.7, color: Colors.blueAccent[400], name:"Taşıma vasıtaları için metal destek veya halkalar (taşıma kancası ve üzerine taşıma kapasitesi yazılmalıdır.)"),
       new SubStageColored(number: 1.8,color: Colors.yellowAccent[400], name:"Asansörün güvenli kullanımına ve bakımına ilişkin bilgiler*",subs: ss1_8),
       new SubStageColored(number: 1.9,color: Colors.blueAccent[400], name:" Makina dairesinde farklı seviyeler ve çıkıntılar ",subs: ss1_9),
       new SubStageColored(number: 1.10,color: Colors.blueAccent[400], name:"Makina platformuna çıkış merdiveni ve korkuluk",subs: ss1_10),
       new SubStageColored(number: 1.11,color: Colors.blueAccent[400], name:"Makina dairesinde yatay ve dikey açıklıklar ",subs: ss1_11),
       new SubStageColored(number: 1.12,color: Colors.yellowAccent[400], name:"Volan üzerindeki yön ve kabin katta işareti*"),
       new SubStageColored(number: 1.13,color: Colors.redAccent[400], name:"Acil durum çalıştırma sistemi** UPS çalışmalıdır."),
       new SubStageColored(number: 1.14,color: Colors.yellowAccent[400], name:" Asansör beyan hızı*"),
       new SubStageColored(number: 1.15,color: Colors.blueAccent[400], name:" Kabin kapıları açıkken kabinin kontrolsüz hareketini önlemek için tahrik makinası tasarımı (UCM denenecek.)"),
       new SubStageColored(number: 1.16,color: Colors.yellowAccent[400], name:" Tahrik makinası*",subs: ss1_16),
       new SubStageColored(number: 1.17,color: Colors.yellowAccent[400], name:" Tahrik makinasının durdurulması ve durma konumunun kontrolü* ",subs: ss1_17),
       new SubStageColored(number: 1.18,color: Colors.yellowAccent[400], name:" Tahrik ve saptırma kasnağı ile kasnak mili yatağının kontrolü* ",subs: ss1_18),
       new SubStageColored(number: 1.19,color: Colors.blueAccent[400], name:" Saptırma kasnağı ",subs: ss1_19),
       new SubStageColored(number: 1.20,color: Colors.redAccent[400], name:"Gerektiği durumda, elektronik aksamları içeren güvenlik şalterleri şeklindeki elektrikli güvenlik ekipmanları**",subs: ss1_20),
       new SubStageColored(number: 1.21,color: Colors.blueAccent[400], name:"Kabin tampona oturmuş iken kasnak kaydırma kontrolü ve enerji kesintisinde ani duruş kontrolü (sınır kesicilerin kontrolü)"),
       new SubStageColored(number: 1.22,color: Colors.blueAccent[400], name:"Kasnaktan veya makaradan çıkan halat/zincire karşı koruma (halat atma pimleri regülatörde 3; tahrik ve saptırmada 2 adet olmalıdır. Konumuna göre değişir.)halat atma pimi mesafesi uygun olmalıdır."),
       new SubStageColored(number: 1.23,color: Colors.blueAccent[400], name:"Halatlar veya zincirler ile kasnak veya makara arasına yabancı cisim girmesine karşı koruma (kasnak koruma aparatı)"),
       new SubStageColored(number: 1.24,color: Colors.blueAccent[400], name:"Kasnak, zincir makaraları yaralamalarına karşı koruma"),
       new SubStageColored(number: 1.25,color: Colors.yellowAccent[400], name:"Acil durdurma tertibatı* (makine motor grubu yakınında 1m içinde doğrudan ulaşılabilir stop butonu olmalıdır.)"),
       new SubStageColored(number: 1.27,color: Colors.yellowAccent[400], name:"Makine dairesinde kilitlenebilir ana anahtarın bulunması* (pako kapatılınca ups dahil bütü güç devrelerinin enerjisi kesilecek. Pako aydınlatma ve priz devrelerinienerjisini kesmeyecek.)"),
       new SubStageColored(number: 1.28,color: Colors.blueAccent[400], name:"Elektrik kuvvet panosunun muhafazası ve pano içerisindeki işaretlemeler (muhafazalı ve adreslenmelidir.) MD dışında veya dişlisiz asansörde elektrik panosukilitlemeli olacak."),
       new SubStageColored(number: 1.29,color: Colors.blueAccent[400], name:"Priz ve makine dairesi mekanı aydınlatma anahtarı (sabit,adresli ve topraklı olmalıdır.)"),
       new SubStageColored(number: 1.30,color: Colors.blueAccent[400], name:"Makine dairesi/mekanında kuyu aydınlatma anahtarı (sabit,adresli olmalıdır.)"),
       new SubStageColored(number: 1.31,color: Colors.blueAccent[400], name:"Sigortalar",subs: ss1_31),
       new SubStageColored(number: 1.32,color: Colors.blueAccent[400], name:"Kablo bağlantıları ve klemensler (Kuvvet panosunda)( IP2X koruma, kablo kanalı kapatma, adresleme, uyarı işareti, kilitleyebilme)"),
       new SubStageColored(number: 1.33,color: Colors.redAccent[400], name:"Hata akımına karşı koruma**",subs: ss1_33),
       new SubStageColored(number: 1.34,color: Colors.redAccent[400], name:"Topraklama**",subs: ss1_34),
       new SubStageColored(number: 1.35,color: Colors.blueAccent[400], name:"Kumanda panosunun muhafazası ve pano içerisindeki işaretlemeler",subs: ss1_35),
       new SubStageColored(number: 1.36,color: Colors.blueAccent[400], name:" Kumanda kartı ve kontaktör (kumanda kartı sabitlenmelidir, kablo bağlantıları düzeltilmelidir, ark yapmamalıdır.)"),
       new SubStageColored(number: 1.37,color: Colors.yellowAccent[400], name:" Tahrik makinası motoru koruması* (Kısa devre, aşırı ısınma (PTC devresi vb.) )"),
       new SubStageColored(number: 1.38,color: Colors.redAccent[400], name:" Emniyet devresi koruma**",subs: ss1_38),
       new SubStageColored(number: 1.39,color: Colors.yellowAccent[400], name:"Güç faz sırası değişiminden kaynaklanan hatalı çalışmaların olmaması* (faz koruma rölesi denenir. İnvertörlü sistemlerde aranmaz.)"),
       new SubStageColored(number: 1.40,color: Colors.yellowAccent[400], name:"Elektrik çarpmalarına karşı koruma (IP2X) ile elektrik donanımın korunması ve işaretlenmesi*"),
       new SubStageColored(number: 1.42,color: Colors.blueAccent[400], name:"Emniyet devre (Kumanda) şeması bulunmalıdır ve emniyet devreleri emniyet devre şemasına göre düzenlenmelidir."),
       new SubStageColored(number: 1.43,color: Colors.blueAccent[400], name:"Kablo bağlantıları ve klemensler (Kumanda panosunda) (kablo uçları izole edilmeli, sigortalarla korunmalıdır.)"),
       new SubStageColored(number: 1.44,color: Colors.redAccent[400], name:"Düzgün çalışan güvenlik tertibatına uygun aşırı hız regülâtörü**",subs: ss1_44),
       new SubStageColored(number: 1.45,color: Colors.redAccent[400], name:"Sınır güvenlik kesicileri**"),
       new SubStageColored(number: 1.46,color: Colors.yellowAccent[400], name:"İstem dışı kabin hareketlerine karşı koruma** (01/01/2012 den sonra piyasaya arz edilen asansörler için)"),
       new SubStageColored(number: 1.47,color: Colors.blueAccent[400], name:"Asansöre ait olmayan kanallar, kablolar ve diğer cihazlar"),
       new SubStageColored(number: 1.48,color: Colors.blueAccent[400], name:"Temizlik (MD'nde yanıcı madde de olmamalıdır.)"),
     ];
   }
   List<SubStageColored> ss1;
   final  int test_id;



   @override
  _SS1State createState() => _SS1State();
}

class _SS1State extends State<SS1> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)

        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name.tr().toString(), super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,0, index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}

