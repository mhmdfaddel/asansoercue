import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';

class SS3 extends StatefulWidget {
   SS3({Key key, int id, this.test_id}) : super(key: key){

     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "kuyu dibi acil durum durdurma tertibatı bulunmalı ve sabit olmalıdır."),
       new SubStageColored(name: "kuyu dibi stop butonu derinliği 1,60 m den daha az veya eşit olan kuyularda kapı eşiğinden en az 0,40 m yükseklikte ve kuyu boşluğu zeminnden en fazla 2,0 m mesafe içinde ve kapı çerçevesi iç kenarından en fazla 0,75 m yatay mesafede bulunmalıdır"),
       new SubStageColored(name: "kuyu dibi stop butonu derinliği 1,60 m den fazla kuyularda iki adet stop butonu bulunur; üstteki anahtar durak kapısı zemininden en az 1 m yğkseklikte ve kapı çerçevesiiç kenarından en fazla 0,75 m yatay mesafede bulunmalıdır. Kuyu boşluğu zemininden en fazla 1,20m dikey mesafe içinde bulunan alttaki anahtar, bir sığınak alanından kullanılabilir"),
       new SubStageColored(name: "kuyu dibi sığınma alanında 30 cm yatay mesafede bir muayene istasyonu olmalıdır. Sabit olması şart değil ancak sökülüp kuyudan götürülemez olmalıdır"),
       new SubStageColored(name: "kuyu dibi muayene istasyonunda herhangi bir şekilde 1 mm çapında tel ile içine ulaşılamayacak korumada olmalıdır"),
       new SubStageColored(name: "kuyu boşluğundaki kabinüstü ve kabinaltı ayakta durma mesafesi 2 m veya daha az olduğunda kabin hızı 0,3 m/sn yi aşmamalıdır."),
       new SubStageColored(name: "asansörün normal çalışmasına geri dönüşü revizyon kumandasının normale çevirilmesi veya reset anahtarı ile mümkün olmalıdır."),
       new SubStageColored(name: "by-pass tertibatı devredeyken muayene kumanda istasyonu çalıştırıldığında kabinde bir ses sinyali ve kabin altında yanıp sönen bir ışık hareket sırasınd aktif olmalıdır. Bu sesli uyarının ses sinyali kabin altında 1 m mesafede asgari 55 Db(A) olmalıdır.")
     ];

     List<SubStageColored> ss1_2=[
       new SubStageColored(name: "karşı ağırlık tamponu sabitlenmeli ve karşı ağırlık ray ekseninde olmalıdır."),
       new SubStageColored(name: "kabin tam kapalı tampon üzerinde iken kabin altı ekipmanların regülatör halatı gergi kasnağına çarpması engellenmelidir."),
       new SubStageColored(name: "kabin tam kapalı tampon üzerine oturduğunda kabin altı ekipmanların kuyu dibine çarpması önlenmelidir"),
       new SubStageColored(name: "kabin tamponu kaidesi sabitlenmelidir"),
       new SubStageColored(name: "kabin en üst seviyede iken karşı ağırlığın tampona teması önlenmelidir ve sınır kesici mesafesi ayarlanmalıd"),
       new SubStageColored(name: "kabin altı tampon çarpma plakası merkezlenmeli ve sabitlenmelidir."),
       new SubStageColored(name: "kabin tamponu sabitlenmelidir."),
       new SubStageColored(name: "beyan hızı 1 m/sn üzerinde olan asansörlerde kabin ve karşı ağırlık için hidrolik tampon kullanılmalıdır."),
     ];

     List<SubStageColored> ss1_3= [
       new SubStageColored(name: "MD kapıları deliksiz ve kat kapıları ile aynı mukavemette olmalıdır. (yanmamalıdır.)"),
       new SubStageColored(name: "MD giriş kapıları 200*60 cm olmalıdır. A3'te bu ölçü 180*60 cm olmalıdır."),
       new SubStageColored(name: "Makara dairesi giriş kapıları en az 140*60 cm olmalıdır."),
       new SubStageColored(name: "Makine/ Makara mekanlarına girişte kullanılan döşeme kapakları 80*80 cm olmalıdır. Ve üzerinde uyarıcı levha bulunmalıdır ve deliksiz olmalıdır."),
       new SubStageColored(name: "makine/makara dairesi kapıları içe doğru açılmamalıdır."),
       new SubStageColored(name: "MD kapı/ kapakları dışarıdan kilitlenememeli ve içeriden anahtarsız açılabilmelidir"),
     ];

     List<SubStageColored> ss1_4=[
       new SubStageColored(name: "kuyu aydınlatması kapı eşiğinden min 1 m yükseklikte kapı iç çerçevesinden max 0,75 m yatay mesafede olmalıdır"),
       new SubStageColored(name: "kuyu zemininden 1 m yükseklikte 50 lüx şiddetinde aydınlatma olmalıdır. 50 lüxtü kuyu boyunca sağlanmalıdır"),
       new SubStageColored(name: "kuyu aydınlatma anahtarı vaevien özellikte olmalıdır."),
       new SubStageColored(name: "kuyu aydınlatması tesisatı etanj olmalıdır."),
     ];

     List<SubStageColored> ss1_5=[
       new SubStageColored(name: "kabin güvenlik tertibatı emniyet kontağında normalde kapalı kontak (NC) kullanılmalıdır."),
       new SubStageColored(name: "kabin güvenlşik tertibatı emniyet kontağı ile baskı sacı arasındaki mesafe ayarlanmalıdır"),
       new SubStageColored(name: "kabin güvenlik tertibatı emniyet kontağı sabitlenmelidir"),
       new SubStageColored(name: "kabin güvenlik tertibatı emniyet kontağı kapağı takılmalıd "),
       new SubStageColored(name: "kabin güvenlik tertibatı çalışır hale getirilmelidir"),
       new SubStageColored(name: "kabin güvenlik tertibatı fren tiji bağlantı yayları takılmalıdır"),
       new SubStageColored(name: "kabin güvenlik tertibatı fren tiji gevşek kontra somunları sıkılmalıdır ve eksik olanlar takılmalıd"),
       new SubStageColored(name: "kabin güvenlik tertibatı halat bağlantıları eksik radansalar takılmalıdır."),
       new SubStageColored(name: "kabin güvenlik tertibatı halat bağlantıları kelepçeleri takılmalı(en az 2 adet kelepçe olmalıdır.) ve ters kelepçeler düzeltilmelidir"),
       new SubStageColored(name: "paraşüt kontağı çalışır hale gelmelidir. (fren bloğu switchi)"),
     ];

     List<SubStageColored> ss1_7=[
       new SubStageColored(name: "Regülatör stop switchi çalışır hale getirilmelidi"),
       new SubStageColored(name: "kabin regülatör mafsal koluna kontra somun takılmalıdır."),
       new SubStageColored(name: "kabin regülaör gergi makarasındaki gevşek somunlar sıkılmalıdır."),
       new SubStageColored(name: "kabin regülatör gergi makarasına hareketli mafsal kolu takılmalıdır"),
       new SubStageColored(name: "kabin regülatör gergi makarasına hareketli mafsal kolu yere paralel hale getirilmelidir."),
       new SubStageColored(name: "kabin regülatör gergi makarası ağırlığının duvara temas etmesi engellenmelidir"),
       new SubStageColored(name: "kabin regülatör halatı ekli olmamalıdır."),
       new SubStageColored(name: "kabin regülatör halatının duvara temas etmesi engellenmelidir."),
       new SubStageColored(name: "kabin regülatör gergi makara halat atma emniyet kontağı çalışır hale getirilmelidir"),
       new SubStageColored(name: "kbin regülatör gergi tertibatı emniyet kontağı pimi ile baskı sacı arasındaki mesafe ayarlanmalı ve emniyet kontağı sabitlenmelidir."),
       new SubStageColored(name: "kabin güvenlik tertibatı (paraşüt sistemi) halat bağlantıları standarda uygun hale getirilmelidir. (eksik kelepçe,ters kelepçe, gevşek bağlantı vb.)"),
       new SubStageColored(name: "kabin regülatör mafsal kolu ve emnniyet kontağı pimi arasındaki mesafe ayarlanmalı ve emniyet kontağı sabitlenmelidir")
     ];

     List<SubStageColored> ss1_8=[
       new SubStageColored(name: "karşı ağırlığın hareket sahası, karşı ağırlığın tam baskısı altındaki tampon üzerinde oturan karşı ağırlığın en alt noktasından kuyu boşluğu zemininden en az 2,0 m yüksekliğe kadar ayırıcı bölme ile korunmalıdır"),
       new SubStageColored(name: "yerden seperatörün en alt kısmı en fazla 0,30 m den başlamalıdır"),
       new SubStageColored(name: "eğer separatör delikli ise, separatör ve karşı ağırlık arası 8 cm'den az ise bu delikler 1 cm'den az olamaz."),
       new SubStageColored(name: "karşı ağırlık separatörü genişliği en az karşı ağırlık genişliğine eşit olmalıdır."),
       new SubStageColored(name: " karşı ağırlık rayından arka duvara ve/ veya rayın yanından yan duvara olan mesafe 30 cm i geçmesi halinde separatör duvara kadar uzatılır."),
       new SubStageColored(name: " karşı ağırlık separatörü esnek olmamalıdır"),
       new SubStageColored(name: "karşı ağırlık ve tampon arasındaki mesafe ölçülerek etikete yazılmalıdır.")
     ];

     List<SubStageColored> ss1_9=[
       new SubStageColored(name: "aynı kuyuda birden fazla asansör bulunması durumunda ayırıcı bölme zeminden başlayıp min 2,5 m yükseklikte olmalıdır"),

     ];

     List<SubStageColored> ss1_10=[
       new SubStageColored(name: "aynı kuyuda birden fazla asansör olması durumunda seperatör delikli malzemeden yapılmışsa hareketli parçalar arası mesafe 20 cmden az ise bölme hücre aralığı 3 cmden fazla olmamalıdır."),
       new SubStageColored(name: "iki asansör arasında birinin korkuluğundan diğerinin en yakın hareketli parçasına ölçülen mesafe 50cmden az ise separatör kuyu boyunca uzanmalıdır."),
       new SubStageColored(name: "separatör genişliği hareketli parçaların iki yanına 10 cm konarak hesaplanır")
     ];

     List<SubStageColored> ss1_11=[
       new SubStageColored(name: "dengeleme tertibatı olarak hızı 3 m/s'yi aşmayan beyan hızlarında zincirler, halatlar veya kayışlar gibi vasıtalar kullanılabilir."),
       new SubStageColored(name: "dengeleme tertibatı halatlı kullanılırsa gergi makarası/halat çapı oranı en az 30 olmalıdır."),
       new SubStageColored(name: "3 m/s'yi aşan beyan hızlarında dengeleme halatları kullanılmalıdır."),
     ];

     List<SubStageColored> ss1_13=[
       new SubStageColored(name: "kabin tam kapalı tampon üzerine oturduğunda güvenlik hacimleri; 40*50*200 cm ayakta;50*70*100 cm çömelerek; 70*100*50 yatarak bu üç standarttan biri sağlanmalıdır.(yatay*yatay*dikey ölçü şeklindedir.)"),
       new SubStageColored(name: "kuyudaki sığınma alanları işaretlenmeli, kişi sayısı belirtilmeli, ölçüleri yazılmalı ve pigtogram ile gösterilmelidir."),
       new SubStageColored(name: "kuyudaki sığınma alanları işaretlenmeli, kişi sayısı belirtilmeli, ölçüleri yazılmalı ve pigtogram ile gösterilmelidir."),
       new SubStageColored(name: "kuyu dibinde bulunan ekipmanların kabin tam kapalı tampon üzerinde iken kabin altına olan mesafesi min 30 cm olmalıdır."),
       new SubStageColored(name: "kabin tam kapalı tampon üzerine oturduğunda kabin patenlerinin raydan çıkması önlenmelidir"),
     ];

     List<SubStageColored> ss1_14=[
       new SubStageColored(name: "karşı ağırlık kasnak halat atma pimleri takılmalı ve mesafesi ayarlanmalıdır.(mesafe max kullanılan halat çapının yarısı şeklindedir. Yani 10 mmlik halatın halat atma pimi mesafesi max 5 mm olmalıdır.)"),
       new SubStageColored(name: "kabin kasnak halat atma pimleri takılmalı ve mesafesi ayarlanmalıdır.(mesafe karşı ağırlık kriterleri ile aynıdır.)"),
       new SubStageColored(name: "kabin ve karşı ağırlık kasnak koruma aparatları takılmalıdır."),
       new SubStageColored(name: "kabin üstü kasnakları için kişilerin yaralanmasını önleyici koruma veya siper bulunmalıdır. Kasnaklarda halat atma pimi olmalıdır"),
       new SubStageColored(name: "hız regülatörü makarasının halat giriş çıkışlarında halatın çıkmasını engelleyici tutucular,yabancı madde girişi engelleyen aparatlar olmalıdır.."),
     ];

     List<SubStageColored> ss1_15=[
       new SubStageColored(name: "kuyu duvarı her giriş kapısı eşiğinde kilit açılma bölgesinin (kapı kaşığı ve ya pompasının uzunluğu) yarısına 5 cm ilave edilerek ölçüsü bulunan bir kapı etek sacı ile kapatılmalıdır. Kapı etek sacının genişliği kapı genişliğinin sağına ve soluna 2,5 cmlik paylar konarak hesaplanır. (örn: 90 cmlik kapının kapı etek sacı genişliği 95 cm olmalıdır."),
       new SubStageColored(name: "kapı etek sacının alt kısmında 60°lik ve en az 2 cm uzunluğunda kıvrılma (pah) payı olmalıdır. Ve esnememelidir. 5 cm den fazla olmamalıdır. 2 cmden büyük pahlarda 75°lik açı olmalıdır"),
       new SubStageColored(name: " kabin girişine bakan duvarlar kesintisiz yani içeri boşluk yada kıvrım yapmamalıdır. (kat kapısı yan duvarları deliksiz olmalıdır.)"),
     ];

     List<SubStageColored> ss1_16=[
       new SubStageColored(name: " kabin etek sacı esnememelidir."),
       new SubStageColored(name: "birkaç parçadan oluşan kabin etek sacı üçgen anahtar ile açılabilmeli ve serbest bırakıldığında geri dönmelidir.daha sonra açılır ve etek sacı en kısa halinden açılmaya başlandığı anda emniyet kontağı devreye girmelidir."),
       new SubStageColored(name: " kabin etek sacı en az kapı genişlğinde ve düşeyde pah harici 75 cm olacaktır"),
       new SubStageColored(name: "kabin etek sacı esnenemelidir"),
       new SubStageColored(name: "kabin etek sacının pahı yatayda 60°'lik açı yapacak şekilde en az 2 cm olmalıdır"),
       new SubStageColored(name: "kabin etek sacı kat kapısına paralel olmalıdır"),
       new SubStageColored(name: "etek sacı bağlantı cıvata başları 5 mm'den fazla olmamalıdır"),
     ];

     List<SubStageColored> ss1_17=[
       new SubStageColored(name: "kuyu duvarları, tavanı ve tabanı yangına dayanıklı ve yanıcı olmayan malzemeden yapılmalıdır"),
       new SubStageColored(name: "kuyu içinde 15 cm'den fazla olan bir insanın durabileceği kiriş veya çıkıntı varsa pahlanmalıdır."),
       new SubStageColored(name: "durak kapılarının etrafında açıklıklar, delikler olmamalıdır."),
       new SubStageColored(name: "tam kapalı kuyu duvarlarında boşluklar olmamalıdır."),
       new SubStageColored(name: "kuyu duvarları camdan imal edildiyse lamine cam olmalıdır"),
     ];

     List<SubStageColored> ss1_18=[
       new SubStageColored(name: "karşı ağırlık ray konsolları duvara sabitlenmelidir"),
       new SubStageColored(name: " karşı ağırlık ray patenleri konrol edilmelidir."),
       new SubStageColored(name: " karşı ağırlık rayları yağlanmalıdır."),
       new SubStageColored(name: "karşı ağırlık karkası altına tampon çarpma plakası takılmalı ve plaka tampona merkezlenmelidir."),
       new SubStageColored(name: "karşı ağırlık karkasının eksik civataları tamamlanmalıdır."),
       new SubStageColored(name: "karşı ağırlık bloklarına zıplama aparatı takılmalıdır."),
       new SubStageColored(name: "hasarlı olan karşı ağırlık baritleri yenilenmelidir"),
       new SubStageColored(name: "karşı ağırlık ile kabin arasında en az 5 cm mesafe olmalıdır"),
       new SubStageColored(name: "Karşı ağırlık duvara temas etmemelidir."),
       new SubStageColored(name: "karşı ağırlık paten lastiklerinin yerinden çıkması önlenmeli ve boşluk ayarı yapılmalıdır"),
       new SubStageColored(name: "karşı ağırlık raylarındaki eksen kaçıklıkları giderilmelidi"),
       new SubStageColored(name: "karşı ağırlık alt üst paten lastikleri takılmalıdır."),
       new SubStageColored(name: " karşı ağırlık patenleri raya tam oturur hale getirilmelidir"),
       new SubStageColored(name: "karşı ağırlık tam kapalı tampon üzerine oturduğunda patenlerin yere çarpması ve patenlerin raydan çıkması önlenmelidir."),
       new SubStageColored(name: "kabin tam kapalı tampon üzerine oturuduğunda karşı ağırlık patenlerinin tavana çarpması önlenmelidir."),
       new SubStageColored(name: "karşı ağırlık klavuz rayı birleşimlerinde kaynaklı kısımlar flanşlı ve cıvata bağlantılı olmalıdır"),
       new SubStageColored(name: "Karşı ağırlık ray flanşlarının eksik cıvata ve somunları tamamlanmalıd"),
       new SubStageColored(name: "karşı ağırlık ray konsol bağlantılarındaki eksik tırnaklar ve civatalar tamamlanmalıdır. Eksik civaalar sıkılmalıdır."),
       new SubStageColored(name: "kabin ile karşı ağırlık aynı kuyu içerisinde bulunmalıdır."),
     ];

     List<SubStageColored> ss1_19=[
       new SubStageColored(name: "kuyu tabanı dayanıklı olmalıdır"),
     ];

     List<SubStageColored> ss1_21=[
       new SubStageColored(name: "kabin üstü alarm butonu çalışmalıdır."),
       new SubStageColored(name: "kuyu dibi alarm butonu güvenlik hacminden ulaşılabilir olmalıdır"),
       new SubStageColored(name: "kabin üstünde elektriğin kesildiği acil durumlarda ikincil bir elektrik kaynağından beslenen 1 saat boyunca 5 lüx aydınlatma şiddeti verecek olan acil aydınlatma bulunmalıdır"),
       new SubStageColored(name: "kuyu dibi alarm tertibatı çalışmalıdır"),
       new SubStageColored(name: "kabin üstü ve kuyu dibi alarm tertibatları aküye bağlı olmalıdır"),
     ];

     List<SubStageColored> ss1_22=[
       new SubStageColored(name: " tamponlar üzerinde (enerji depolayan tamponlar hariç) imalatçı firmanın adı, tip kontrolü ile ilgili işaret ve referansları ve CE işareti içeren bilgi levhası bulunmalıdır"),
       new SubStageColored(name: "kabin güvenlik tertibatı üzerinde (enerji depolayan tamponlar hariç) imalatçı firmanın adı, tip inceleme serifika numarası ve CE işareti, güvenlik tertibat tipi içeren bilgi levhası bulunmalıdır"),
       new SubStageColored(name: "kuyu dışında bakım kapıları yakınında: '' asansör kuyusu- tehlike yetkili olmayan giremez.'' ikaz levhaları bulunmalıdır."),
       new SubStageColored(name: "kuyu dibi stop ta ''STOP/DUR'' kelimesi bulunmalıdır."),
       new SubStageColored(name: "elle açılan durak kapısı varsa ''ASANSÖR'' yazılı ikaz levhası konulmalıdır"),
     ];

     List<SubStageColored> ss1_23=[
       new SubStageColored(name: "kuyuda asansöre ait olmayan kablo techizatı ve techizat sökülmeli veya izole edilmelidir"),
     ];

     List<SubStageColored> ss1_24=[
       new SubStageColored(name: "kuyu dibi prizi topraklama hattı bağlanmalıdır"),
       new SubStageColored(name: "kuyu dibi prizi sabitlenmeli ve çalışır hale getirilmelidir"),
     ];

     List<SubStageColored> ss1_25=[
       new SubStageColored(name: "kabine bağlı olan bükülgen kablonun yere teması önlenmelidir."),
       new SubStageColored(name: "kabin altı kumanda kablosu uygun takozla düşey konumda kabine bağlanmalıdır."),
       new SubStageColored(name: "kabine bağlı bükülgen kablonun hareketli kısmının ray konsollarına ve raya teması önlenmelidir"),
       new SubStageColored(name: "kabin bağlantı bükülgen kablosu ile ilave çekilen kablolar birbirine bağlanmalıdır"),
       new SubStageColored(name: "kabin altı bükülgen kablo izolasyonu uygun hale getirilmelidir."),
     ];

     List<SubStageColored> ss1_26=[
       new SubStageColored(name: "kuyu dibi temizlenmelidir."),
       new SubStageColored(name: "kuyu alt boşluğuna su sızması önlenmelidir"),
     ];
     ss1 = [
       new SubStageColored(number: 3.1, color: Colors.blue, name: "Kuyu alt boşluğuna güvenli erişim", subs: ss1_1),
       new SubStageColored(number: 3.2, color: Colors.red, name:"Kabin ve karşı ağırlıkta yeterli tampon veya eşdeğeri**", subs: ss1_2),
       new SubStageColored(number: 3.3,color: Colors.yellow, name:"Kuyu dibi acil durum durdurma tertibatı*",subs: ss1_3),
       new SubStageColored(number: 3.4,color: Colors.blue, name:"Kuyu aydınlatma anahtarı ve yeterli kuyu aydınlatması",subs: ss1_4),
       new SubStageColored(number: 3.5, color: Colors.red, name:"Kabin ve/veya karşı ağırlık için uygun aşırı hız regülâtörü tarafından harekete geçirilen güvenlik tertibatı**",subs: ss1_5),
       new SubStageColored(number: 3.6, color: Colors.blue, name:"Kabinin yukarı doğru aşırı hızlanmasına karşı koruma (Yukarı yön fren testi)"),
       new SubStageColored(number: 3.7, color: Colors.red, name:"Kabin ve/veya karşı ağırlık hız regülâtörü halat gerginliği ve halat gergi tertibatında elektrikli güvenlik tertibatı**",subs: ss1_7),
       new SubStageColored(number: 3.8,color: Colors.blue, name:"Karşı veya dengeleme ağırlığı ayırıcı bölmesi",subs: ss1_8),
       new SubStageColored(number: 3.9,color: Colors.blue, name:" Aynı asansör kuyusu içerisinde birden fazla asansör bulunduğunda asansörler arasında ayırıcı bölme",subs: ss1_9),
       new SubStageColored(number: 3.10,color: Colors.blue, name:"Aynı asansör kuyusu içerisinde birden fazla asansör bulunduğunda asansörler ile kuyu arasında ayırıcı bölme",subs: ss1_10),
       new SubStageColored(number: 3.11,color: Colors.blue, name:"Denge halatı klavuzlaması ve denge halatı makarası bağlantılarının kontrolü ve denge halatı kontağı testi",subs: ss1_11),
       new SubStageColored(number: 3.12,color: Colors.yellow, name:"Gevşek halat veya zincir güvenlik tertibatı*"),
       new SubStageColored(number: 3.13,color: Colors.blue, name:"Kuyu alt boşluğunda güvenlik alanı",subs: ss1_13),
       new SubStageColored(number: 3.14,color: Colors.blue, name:"Halatlar veya zincirler ile kasnak veya makara arasına yabancı cisim girmesine karşı koruma",subs: ss1_14),
       new SubStageColored(number: 3.15,color: Colors.blue, name:"Kat kapısı eşiği altında kuyu duvarı",subs: ss1_15),
       new SubStageColored(number: 3.16,color: Colors.red, name:"Kabin etek sacı**",subs: ss1_16),
       new SubStageColored(number: 3.17,color: Colors.blue, name:"Deliksiz duvarlı kuyu mahfazaları",subs: ss1_17),
       new SubStageColored(number: 3.18,color: Colors.blue, name:"Karşı ağırlık/dengeleme ağırlığı kılavuzlama sistemi",subs: ss1_18),
       new SubStageColored(number: 3.19,color: Colors.yellow, name:"Kabin, karşı ağırlık/dengeleme ağırlığı altında erişilebilir alanlara karşı koruma önlemleri*",subs: ss1_19),
       new SubStageColored(number: 3.20,color: Colors.red, name:"Karşı ağırlık için düzgün çalışan güvenlik tertibatı bulunması durumunda uygun aşırı hız regülâtörü** (karşı ağırlıkta regülatör bulunması halinde ek maddelere bakılacaktır.)"),
       new SubStageColored(number: 3.21,color: Colors.blue, name:"Kuyu içinde mahsur kalan kişilerinin acil kurtulması veya kurtarılması",subs: ss1_21),
       new SubStageColored(number: 3.22,color: Colors.blue, name:"Asansörün güvenli kullanımına ve bakımına ilişkin bilgiler",subs: ss1_22),
       new SubStageColored(number: 3.23,color: Colors.blue, name:"Asansöre ait olmayan teçhizat",subs: ss1_23),
       new SubStageColored(number: 3.24,color: Colors.blue, name:"Kuyu dibi prizi",subs: ss1_24),
       new SubStageColored(number: 3.25,color: Colors.blue, name:"Bükülgen kablo ve takoz bağlantısı",subs: ss1_25),
       new SubStageColored(number: 3.26,color: Colors.blue, name:"Temizlik ve rutubet durumu",subs: ss1_26),
     ];

   }

   final int test_id;

   List<SubStageColored> ss1;

   @override
  _SS3State createState() => _SS3State();
}

class _SS3State extends State<SS3> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)

        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,2,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
