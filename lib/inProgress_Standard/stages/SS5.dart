import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';

class SS5 extends StatefulWidget {
   SS5({Key key, int id, this.test_id}) : super(key: key){
     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "kabin içinde imalatçı firmanın ismi, montaj seri numarası, imal yılı, beyan yükü, insan sayısının yazdığı bir bilgilendirme etiketi bulunmalıdır."),
       new SubStageColored(name: "kabin içinde ağırlık ve kişi pigtogram ile belirtilmelidir."),
     ];
     List<SubStageColored> ss1_2= [
       new SubStageColored(name: "kabin içinde hangi katta olduğunun göstergesi olmalıdır."),
       new SubStageColored(name: "varsa stop butonu kırmızı; alarm butonu sarı renkte olacaktır. Diğer butonlar sarı ve/veya kırmızı renkte olamaz."),
       new SubStageColored(name: "kabin içinde asansör işletme talimatı bulunmalıdır"),
       new SubStageColored(name: "kapı açma/kapama butonu kabin içinde bulunmalıdır"),
       new SubStageColored(name: "kabin içi butonlar açıkça adreslendirilmelidir"),
     ];

     List<SubStageColored> ss1_4=[
       new SubStageColored(name: "kabin duvarları alev almaz malzemeden yapılmalıdır. Camlar kırılmaz temperli cam etiketli olmalıdır."),
       new SubStageColored(name: "kabin üstü eksik montaj civataları tamamlanmalıdır."),
       new SubStageColored(name: "camdan yapılan kabin duvarları 110 cm'den daha alçakta ise, döşemeden 90 cm ile 110 cm arasında yüksekliğe bir el tutamağı konulmalıdır. Bu tutamak camdan bağımsız olmalıdır."),
       new SubStageColored(name: "kabin altı eksik montaj civataları tamamlanmalıdır"),
       new SubStageColored(name: "kabin altı bağlantı takozları/lastikleri değiştirilmelidir"),
       new SubStageColored(name: "kabin üstü kaynaklı bağlantılar(süspansiyon, paraşüt tertibatı, kapılar vb. ) cıvata bağlantıları ile desteklenmelidir"),
       new SubStageColored(name: "kabin duvarlarındaki lamine cam üzerinde imalatçının adı, ticari markası, camın tipi ve kalınlığını belirten etiket bulunmalıdır."),
       new SubStageColored(name: "camdan yapılan kabin duvarlarında lamine cam kullanılmalıdır."),
       new SubStageColored(name: "kabin ve karkas bağlantılarındaki eksik kontra somunlar takılmalıdır."),
       new SubStageColored(name: "kabin üstü temizlenmelidir."),
     ];

     List<SubStageColored> ss1_6=[
       new SubStageColored(name: "kabin içerisinde kapı açma butonu bulunmalı, uygun şekilde adreslenmeli ve çalışmalı; kat arasında kapıyı açmamalıdır."),
     ];

     List<SubStageColored> ss1_7=[
       new SubStageColored(name: "kumanda buton adreslemeleri yapılmalıdır."),
       new SubStageColored(name: "toplama kumanda asansörlerde kumanda butonlarının kayıt ışıkları yanar hale getirilmelidir."),
       new SubStageColored(name: "katlar ve kabindeki kumanda göstergeleri çalışır hale getirilmelidir"),
     ];

     List<SubStageColored> ss1_8=[
       new SubStageColored(name: "kabin içi aydınlatma zeminden 1 m yukarıda en az 100 lüx olmalıdır."),
     ];

     List<SubStageColored> ss1_10=[
       new SubStageColored(name: "iki yönlü haberleşme tertibatı çalışır hale getirilmelidir."),
       new SubStageColored(name: "sesli alarm tertibatı akü devresine bağlı çalışır hale getirilmelidir."),
       new SubStageColored(name: "sesli alarm tertibatı ses şiddeti kuyu dışından duyulabilecek şekilde olmalıdır."),
     ];

     List<SubStageColored> ss1_11=[
       new SubStageColored(name: "seyir mesafesinin 30 m'yi aştığı durumlarda, kabin içi ile makine dairesi arasında acil durum kaynağından beslenen bir interkom sistemi olmalıdır."),
     ];

     List<SubStageColored> ss1_12=[
       new SubStageColored(name: "kabinde aşırı yük tertibatı bulunmalıdır"),
       new SubStageColored(name: "kabin hareket halindeyken aşırı yük devreye girmemelidir"),
       new SubStageColored(name: "kabin aşırı yüklendiğinde kabin içinde sesli ve görünür bir sinyal olmalıdır."),
     ];

     List<SubStageColored> ss1_14=[
       new SubStageColored(name: "kabinin katlarda durma hassasiyeti -+ 10 mm olmalıdır."),
       new SubStageColored(name: "kabinin katlarda +- 20 mm kayması durumunda otomatik seviyeleme olmalıdır")
     ];

     ss1 = [
       new SubStageColored(number: 5.1, color: Colors.yellow, name: "Beyan yükü, kişi sayısı, imal yılı ve asansör monte edene ilişkin bilgileri içeren etiket*", subs: ss1_1),
       new SubStageColored(number: 5.2, color: Colors.blue, name:"Asansörün güvenli kullanımına ve bakımına ilişkin bilgiler", subs: ss1_2),
       new SubStageColored(number: 5.3,color: Colors.yellow, name:"Güvenli kabin taban alanı beyan yükü oranı* (kabin alanı beyan yükünü karşılamalıdır.)"),
       new SubStageColored(number: 5.4,color: Colors.blue, name:"Kabin duvarlarının, taban ve tavan yapısının uygunluğu",subs: ss1_4),
       new SubStageColored(number: 5.5, color: Colors.red, name:"Kabin kapısız asansörlerde çift konumda kararlı acil durdurma fonksiyonu** (iç kapısız asansörler için geçerlidir.)"),
       new SubStageColored(number: 5.6, color: Colors.blue, name:"Kabin kapısı otomatik olan asansörlerde kapı açma butonu",subs: ss1_6),
       new SubStageColored(number: 5.7, color: Colors.blue, name:"Kumanda butonları ve göstergeler",subs: ss1_7),
       new SubStageColored(number: 5.8,color: Colors.blue, name:"Kabin içerisinde normal aydınlatma",subs: ss1_8),
       new SubStageColored(number: 5.9,color: Colors.yellow, name:"Kabin içerisinde acil durum aydınlatması*"),
       new SubStageColored(number: 5.10,color: Colors.yellow, name:"Alarm tertibatı ve iki yönlü haberleşme*",subs: ss1_10),
       new SubStageColored(number: 5.11,color: Colors.blue, name:"Makina dairesi ile kabin arasında doğrudan haberleşme",subs: ss1_11),
       new SubStageColored(number: 5.12,color: Colors.yellow, name:"Kabin yükü kontrol tertibatı*",subs: ss1_12),
       new SubStageColored(number: 5.13,color: Colors.blue, name:"Yeterli kabin havalandırması (kabin fanı çalışması kontrol edilir.)"),
       new SubStageColored(number: 5.14,color: Colors.blue, name:"Katta durma ve seviyeleme doğruluğu",subs: ss1_14),
     ];
   }

   final int test_id;

   List<SubStageColored> ss1;

   @override
  _SS5State createState() => _SS5State();
}

class _SS5State extends State<SS5> {



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)

        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,4,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
