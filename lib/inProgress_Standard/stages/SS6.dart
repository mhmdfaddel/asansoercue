import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';
import 'package:asansorci/inProgress_Standard/Expansione.dart';


class SS6 extends StatefulWidget {
   SS6({Key key, int id, this.test_id}) : super(key: key){
     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "Makine grubu montaj civataları sıkılmalı ve kontra somun takılmalıdır."),
       new SubStageColored(name: "Makina platformu eğer duvara montajı yapılmışsa çelik dübellerin atılma şekline yeterli sayıda atılıp atılmadığına bakılır"),
       new SubStageColored(name: "asansör kuyularının kısmen kapalı olduğu durumlarda makinalar çevresel etkilere karşı uygun şekilde korunmalıdır."),
       new SubStageColored(name: "makinanın montajındaki dengesizlikler giderilmelidir (stabil olmalı)."),
       new SubStageColored(name: "kuyu içerisinde bir çalışma alanından diğer çalışma alanına hareket için serbest yükseklik 1,80 m olmalıdır.")
     ];
     List<SubStageColored> ss1_2= [
       new SubStageColored(name: "kabin üstü mekanik kilitleme tertibatının kenetleme noktasından itibaren çıkış alanı olarak 50 cm olmalıdır. Kapı ölçüsü 70 cm'i geçeceğinden 50 cm *70 cm'lik bir alanı sağlamalıdır"),
       new SubStageColored(name: "kabin üstünde mekanik kilitleme tertibatı bulunmalıdır"),
       new SubStageColored(name: "mekanik tertibat kabin üzerinde kilitlendikten sonra çalışma alanı olarak en az yükseklik 2,10 m olmalıdır."),
       new SubStageColored(name: "mekanik tertibatın stop switchi çalışmalıdır."),

     ];

     List<SubStageColored> ss1_3=[
       new SubStageColored(name: "makine kuyu dibinde ise kuyu dibindeki kapı açılınca bir emniyet kontağı ile tüm hareketleri önlemelidir. Sadece muayene istasyonları sayesiyle bu mekanik tertibat aktif durumda iken çalıştırılabilir."),
       new SubStageColored(name: "makinalar kuyu dibinde ise kuyu dibindeki kapı açılınca bir emniyet kontağı ile tüm hareketleri önlenmelidir. Sadece muayene istasyonları sayesiyle, bu mekanik tertibat aktif durumda iken çalıştırılabilir."),
     ];

     List<SubStageColored> ss1_4=[
       new SubStageColored(name: "kumanda panosunun aydınlatması panonun yakınında veya üzerine yerleştirilmiş bir anahtar ile sağlanmalıdır."),
       new SubStageColored(name: "kumanda panosunda bulunan revizyon kumandası; dış çağrı iptali, otomatik kapı çalışması iptali, kabini en üst/ en alt kata çağırabilecek özelliklere sahip olmalıdır"),
       new SubStageColored(name: "kurtarmanın yapılacağı alanda (panoda) 200 lüx aydınlatma olmalıdır."),
       new SubStageColored(name: "kumanda panosunda bir interkom sistemi, dinamik deneylerin yapılmasını sağlayan kumanda tertibatları, asansör tahrik makinasının doğrudan ögzlenmesi veya göstegelerden kilit açılma bölgesine ulaşıldığı veya asansör kabininin hızı görülebilmelidir"),
       new SubStageColored(name: "acil durum elektrikli müdahale tertibatı 1 mm çapındaki tel içerisine girmeyecek şekilde korumalı olmalıdır."),
       new SubStageColored(name: "yetkili olmayan personel donanıma erişebildiğinde, doğrudan temasa karşı IP2XD karşılık gelen asgari koruma derecesi uygulanmalıdır. Kurtarma çalışmaları için üzerinde tehlikeli elektrikli parçalar bulunan kuyu mahfaza duvarı açıldığında, tehlikeli gerilime erişim, IPXXB asgari koruma derecesi ile engellenmelidir."),
       new SubStageColored(name: "makinanın kuyuda olması durumunda, motor %125 yükte ve en sert frenleme etkisi ile oluşan kuvvetlere dayanacak şekilde yerinde kalması gerekir"),
       new SubStageColored(name: "tüm acil durum ve deney testleri makine dairesiz asansörlerde kuyu dışından yapılabilmelidir"),
     ];

     ss1 = [
       new SubStageColored(number: 6.1, color: Colors.blue, name: "Makinanın kuyu içerisindeki bağlantılarının uyunluğu*", subs: ss1_1),
       new SubStageColored(number: 6.2, color: Colors.blue, name:"Gerekli olduğu durumda kabinin hareketini önlemek için mekanik tertibat**", subs: ss1_2),
       new SubStageColored(number: 6.3,color: Colors.yellow, name:"Gerekli olduğu durumda kuyu dibinde mekanik tertibatın (makina kuyu dibinde ise) kontrolü*", subs: ss1_3),
       new SubStageColored(number: 6.4,color: Colors.yellow, name:"Acil durum çalışması ve deney işlemleri için tertibat**",subs: ss1_4),
     ];
   }



  final int test_id;
   List<SubStageColored> ss1;

  @override
  _SS6State createState() => _SS6State();
}

class _SS6State extends State<SS6> {

  bool done;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)

        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,5,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
