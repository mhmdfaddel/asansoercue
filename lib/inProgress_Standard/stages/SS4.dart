import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';

class SS4 extends StatefulWidget {
  SS4({Key key, int id, this.test_id}) : super(key: key)
  {
    List<SubStageColored> ss1_1 = [
      new SubStageColored(name: " asansörü güvenle kullanma talimatı asansörün içine takılmalıdır."),
    ];
    List<SubStageColored> ss1_2= [
      new SubStageColored(name: "kabin kapısında bir elektrik güvenlik tertibatı olmalıdır. (iç kapı switchi)"),
      new SubStageColored(name: "kapılar, normal işletmede sıkışmayacak ve hareket mesafesi sonunda klavuzlarından çıkmayacak bir yapıya sahip olmalıdır"),
      new SubStageColored(name: " kabin girişine kabin kapısı takılmalı ve kapının asgari net yüksekliği 2 m olmalıdır."),
      new SubStageColored(name: "asansör revizyonda iken kabin kapısı kapalı olmalıdır. Kapalı olmadığı durumda asansörün hareket etmesini engellemelidir"),
      new SubStageColored(name: "kabin kapısı çalışır hale getirilmelidir."),
    ];

    List<SubStageColored> ss1_3=[
      new SubStageColored(name: "durak ve kabin kapısı kapalı durumda iken kapı kanatları veya kanatlar ile kasa, eşik veya kasa üstü arasındaki açıklıklar mümkün olduğu kadar küçük olmalı ve 10mm'yi geçmemelidir."),
      new SubStageColored(name: "kabin ve durak kapıları deliksiz olmalıdır.")
    ];

    List<SubStageColored> ss1_4=[
      new SubStageColored(name: " kapı panelleri camdanyapılmış ise, camlar lamine olmalı ve etiketli olmalıdır."),
      new SubStageColored(name: "elle açılan durak kapısı varsa kabinin katta olduğunu görebilmek için cam panel kullanıldıysa lamine cam olacaktır")

    ];

    List<SubStageColored> ss1_8=[
      new SubStageColored(name: "durak kapılarının önündeki aydınlatma şiddeti zeminde 50 lüx olmalıdır."),
      new SubStageColored(name: "kat kapısı sahanlık aydınlatmaları çalışır hale getirilmelidir"),
      new SubStageColored(name: "asansör bakım ve kayıt defteri bulunmalıdır."),
      new SubStageColored(name: "birden fazla asansörün kuyusu ve Mdsi ortaksa pano, regülatör, elektrik panosu,kabin vb. parçaları işaretlenmelidir."),
      new SubStageColored(name: "ana anahtar, priz, aydınlatma anahtarı adreslenmelidir."),
      new SubStageColored(name: "elektrikli elle kurtarma kumandalarında (normal,revizyon,aşağı,yukarı (varsa) ortak) işaretlemesi yapılmalıdır."),
    ];

    List<SubStageColored> ss1_9=[
      new SubStageColored(name: "kat kapıları, normal işletmede sıkışmayacak ve hareket mesafesi sonunda klavuzlarından çıkmayacak bir yapıya sahip olmalıdır."),

    ];

    List<SubStageColored> ss1_10=[
      new SubStageColored(name: "kabin kapısı emniyet kontağı takılmalıdır."),
      new SubStageColored(name: "boy fotoseli ve sıkışma kontağı çalışır hale getirilmelidir."),
      new SubStageColored(name: "boy fotosel zeminden 2,5 cmden başlamalı ve minimum 160 cmde bitmelidir"),
    ];

    List<SubStageColored> ss1_11=[
      new SubStageColored(name: "kat kapısı topraklama bağlantıları yapılmalı ve yüksük veya civatalı veya kablo pabucu ile olmalıdır"),
    ];

    List<SubStageColored> ss1_12=[
      new SubStageColored(name: "kabin eşiği ile durak kapısı eşiği arasındaki yatay mesafe max 3,5 cm olmalıdır"),
    ];

    List<SubStageColored> ss1_13=[
      new SubStageColored(name: "kabin kapısı ile kapalı kat kapıları arasındaki yatay açıklık normal çalışmada 12 cm'i aşmamalıdır."),
    ];

    List<SubStageColored> ss1_14=[
      new SubStageColored(name: " asansör kuyusu iç yüzeyi ile kabin eşiği veya kabin kapısının çerçevesi veya sürgülü kapılarda kapanan kenar arasındaki yatay açıklık, kuyu boyunca 15 cm'igeçmemelidir. Dikeydeki mesafe 50 cm'i aşmıyorsa yatayda 15 cm; 50 cm'den fazla ise yatayda 20 cm'i aşmamalıdır."),
      new SubStageColored(name: "kabinin katlarda +- 20 mm kayması durumunda otomatik seviyeleme olmalıdır")

    ];

    List<SubStageColored> ss1_16=[
      new SubStageColored(name: "kat butonları çalışır hale getirilmelidir."),
      new SubStageColored(name: "kat buton ışıkları yanar hale getirilmeldir"),
      new SubStageColored(name: "kat göstergeleri sabitlenmelidir."),
      new SubStageColored(name: "katlarda gösterge yönleri düzeltilmelidir"),
      new SubStageColored(name: "(varsa) katlarda gösterge yön ışıkları çalışır hale getirilmelidir. (dubleks çalışan asansörler için zorunludur.)")
    ];

    List<SubStageColored> ss1_17=[
      new SubStageColored(name: "üçgen anahtar ile durak kapısı kilit açılma işleminden sonra durak kapısı kapanınca kilitleme tertibatı açık konumda kalmamalıdır."),
      new SubStageColored(name: "makine dairesinde veya kumanda panosunda kilit açma anahtarı ile ilgili talimat olmalıdır."),
    ];

    List<SubStageColored> ss1_19=[
      new SubStageColored(name: "Yangına karşı dirençli kat kapıları (yapı yüksekliği 51,50 m'den az olan binalarda E30; 5150 m'den fazla olan binalarda E60 yangına dayanıklı kapı sertifikası bulunmalıdır."),

    ];
    ss1 = [
      new SubStageColored(number: 4.1, color: Colors.blue, name: "Asansör işletme talimatı", subs: ss1_1),
      new SubStageColored(number: 4.2, color: Colors.blue, name:"Kabin kapısı/kapıları", subs: ss1_2),
      new SubStageColored(number: 4.3,color: Colors.blue, name:"Deliksiz kat ve kabin kapıları"),
      new SubStageColored(number: 4.4,color: Colors.blue, name:"Camlı kat ve kabin kapıları",subs: ss1_4),
      new SubStageColored(number: 4.5, color: Colors.blue, name:"Camlı kat kapıları veya yatay sürmeli kabin kapılarında çocukların ellerinin sürüklenmesine karşı tedbirler"),
      new SubStageColored(number: 4.6, color: Colors.blue, name:"Çok panelli sürmeli kapılar"),
      new SubStageColored(number: 4.7, color: Colors.blue, name:"Menteşeli kat kapısı kapatıldığında çalışan, makina gücü ile çalışan kabin kapıları"),
      new SubStageColored(number: 4.8,color: Colors.blue, name:"Kat kapılarında aydınlatma",subs: ss1_8),
      new SubStageColored(number: 4.9,color: Colors.yellow, name:"Kat kapı bağlantılarının mukavemeti*",subs: ss1_9),
      new SubStageColored(number: 4.10,color: Colors.yellow, name:"Engelliler tarafından kullanılması amaçlanan/amaçlanmayan kabin ve kat kapılarında koruyucu tertibat*",subs: ss1_10),
      new SubStageColored(number: 4.11,color: Colors.yellow, name:"Kapı topraklama bağlantıları*",subs: ss1_11),
      new SubStageColored(number: 4.12,color: Colors.blue, name:"Kabin ile kabin girişine bakan kuyu duvarı arasındaki açıklık",subs: ss1_12),
      new SubStageColored(number: 4.13,color: Colors.blue, name:"Kabin kapısı ile kat kapısı arasındaki yatay mesafe",subs: ss1_13),
      new SubStageColored(number: 4.14,color: Colors.yellow, name:"Kuyu iç yüzeyi ile kabin eşiği/kabin kapısının çerçevesi /sürmeli kapılarda kapanan kenar arasındaki yatay mesafe*",subs: ss1_14),
      new SubStageColored(number: 4.15,color: Colors.blue, name:"Yay, amortisör, paten ve makara (kapı yayları, ptenleri ve amortisörleri kontrol edilir.)"),
      new SubStageColored(number: 4.16,color: Colors.yellow, name:"Kat butonları ve göstergeler*",subs: ss1_16),
      new SubStageColored(number: 4.17,color: Colors.blue, name:"Acil durumlarda kat kapılarının özel alet kullanılarak açılması",subs: ss1_17),
      new SubStageColored(number: 4.18,color: Colors.blue, name:"Yatay sürmeli kapıların otomatik olarak kapanması (kat kapı kilitlerinin denenmesi)"),
      new SubStageColored(number: 4.19,color: Colors.blue, name:"Yangına karşı dirençli kat kapıları (yapı yüksekliği 51,50 m'den az olan binalarda E30; 5150 m'den fazla olan binalarda E60 yangına dayanıklı kapı sertifikası bulunmalıdır."),
    ];

  }

  final int test_id;

  List<SubStageColored> ss1;

  @override
  _SS4State createState() => _SS4State();
}

class _SS4State extends State<SS4> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)

        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,3,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
