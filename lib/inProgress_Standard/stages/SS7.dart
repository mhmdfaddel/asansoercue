import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';


class SS7 extends StatefulWidget {
   SS7({Key key, int id, this.test_id}) : super(key: key){
     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "giriş ve acil durum kapakları ile muyene kapakları kuyu içine doğru açılmamalıdır"),
       new SubStageColored(name: "giriş ve acil durum kapıları/ kapakları için emniyet kontağı bulunmalıdır."),
       new SubStageColored(name: "muayene, giriş ve acil durum kapıları/ kapakları içeriden elle dışarıdan anahtarla açıbilme özelliğinde olmalıdır"),
     ];
     List<SubStageColored> ss1_2= [
       new SubStageColored(name: "komşu durak kapısı eşikleri arasındaki mesafe 11 m'yi geçtiği durumlarda a) ara acil durum kapıları konur veya b) her biri acil durum kapılarıyla donatılmış birbirne komşu (ardışık) kabinler bulunmalıdır. (aynı kuyuda iki asansör bulunması durumunda)"),
       new SubStageColored(name: "giriş ve acil durum kapıları deliksiz olmalı ve mekanik dayanıklılık açısından durak kapıları ile aynı mukavemette olmalıdır. İlgili binanın yangından korunması için geçerli düzenlemelere uygun olmalıdır"),
       new SubStageColored(name: "giriş ve acil durum kapıları esnememelidir."),
       new SubStageColored(name: "asansörün çalışması muayene kapakları/kapılarının kapalı olması durumunda mümkün olmalıdır."),
       new SubStageColored(name: "acil durum kapıları; 1,8 m asgari yüksekliğe ve 0,50 m asgari gebişliğe sahip olmalıdır."),
       new SubStageColored(name: "muayene kapakları en fazla 0,5 m yükseklikte ve en fazla 0,5 m genişlikte olmalıdır."),
     ];

     List<SubStageColored> ss1_3 =[
       new SubStageColored(name: "spor salonlarında, hastanelerin acil servis bölümlerinde, üst geçitlerde, sosyal konut alanlarında ve tren istasyonlarındaki asansörlerde kasıtlı tahribata karşı TS EN 81-71 standardı kategori-2 şartlarını; metrolardaki asansörlerde kategori-0 şartlarını, alışveriş merkezlerindeki asansörlerde isekategori -1 şartlarını karşılayacak tedbirler alınmalıdır"),
     ];

     List<SubStageColored> ss1_4=[
       new SubStageColored(name: "asansörün kabin ölçüleri, kapı ölçüleri, kapı tipi, hızı ve durak sayısı ilgili idarenin onaylı avan veya uygulama projelerine uygun olmalıdır"),
       new SubStageColored(name: "kamuya açık binalardaki asansörlerde tüm katlara hizmet eden mevcut binalarda en az bir adet) engelli kişiler için erişim sağlamaya yönelik tedbirler alınmalıdır. (1 ekim 2017 sonrası tüm asansörlerde engelli erişim şartları aranır.)"),
       new SubStageColored(name: "engelli kullanımı için tasarlanan asansör bina girşinden açıkça görülemiyor ise girişten itibaren yönlendirme işaretleri olmalıdır."),
       new SubStageColored(name: "asansörün önündeki sahanlıktan asansör kabinine erişimde eşik, basamak veya kot farkı olmamalıdır"),
       new SubStageColored(name: "kabin durduğunda sözlü olarak (Türkçe), kabin konumu bildirilmelidir"),
       new SubStageColored(name: "engelli standardı için kabin içi göstergesi bitmiş zeminden merkez hattı olarak 1,60-1,80 m arasında olacaktır. Rakamların yüksekliği ise 30-60 mm arasında olmalıdır. Katlar rakamlar ile gösterilmeli (-1,0,1,2 vb.) harf veya diziler kullanılmamalıdır (B,D1 vb. )."),
       new SubStageColored(name: " binanın çıkış katını gösteren buton: diğer butonlardan 5+-1 mm daha önde (tercihen yeşil renkte) olmalıdır."),
       new SubStageColored(name: "kabine girmeden önce, kumanda sistemi yeni seyir yönünü belirliyorsa (müşterek kumanda) aydınlatılmış işaret okları kapının üstüne veya yakınına yerleştirilmelidir. Sesli işaret okların aydınlatılması ile birlikte verilmelidir. Yukarı ve aşağı farklı sesli işaretleri kullanılmalıdır. işaret okları zeminden 1,8 m -2,50 m yukarıda durak tarafından görüş açısı 140̊ olacak şekilde konumlandırılmalıdır"),
       new SubStageColored(name: "durak butonlarına basıldığında kabinin kata geldiğini belirten bir işaret olmalıdır. (bunlar içerisinde anons, dış ok buzzeri vs. olabilir)"),
       new SubStageColored(name: "dış ve iç kat kayıt butonlarına basıldığında önce sadece ışıklı ikaz verilmelidir. Kayıt butonlarındaki sesli işaret butona her basıldığında ve ancak kayıt tutulmuş ise verilmelidir"),
       new SubStageColored(name: " durak kumanda butonunun en üstteki butonun merkez hattı ile zemin seviyesi arasındaki azami mesafe 110 cm olmalıdır"),
       new SubStageColored(name: "kabin iç kumanda butonlarının en üstteki butonun merkez hattı ile zemin seviyesi arasındaki azami mesafe 120 cm asgari 90 cm olmalıdır"),
       new SubStageColored(name: "yana açılan kapılı asansörlerde, kabin kumanda paneli kapının kapanma kenarı tarafında yer almalıdır"),
       new SubStageColored(name: "ortadan açılan kapılı asansörlerde, kabin kumanda paneli kabine girerken sağ tarafta olmalıdır"),
       new SubStageColored(name: "yatay butonlar soldan sağa; dikey butonlar aşağıdan yukarı sıralanışta olmalıdır"),
       new SubStageColored(name: "alarm ve kapı aç butonu çağrı butonlarının altında olacak ve çalışma mesafelerinin iki katı uzaklıkta olacaktır. Kapı kapat yada diğer ilave butonlar başka yüksekliklerde olabilir"),
       new SubStageColored(name: "görme engelliler için butonların üzerinde kabartma ve ya braille alfabesi ile işaretlenmelidir."),
       new SubStageColored(name: "kabin zemini kaymayan bir yüzeye sahip olmalıdır."),
       new SubStageColored(name: "yana açılan kapıların önündeki genişlik en az 120 cm, dışa açılan kapılar için sahanlık genişliği en az 150 cm olacaktır."),
       new SubStageColored(name: "engelli kullanımı için tasarlanan asansörlerde katta durma hassasiyeti +- 10 mm olmalıdır ve kabinin katlarda kat seviyesinden +- 20 mm kayması durumunda otomatik seviyeleme olmalıdır."),
       new SubStageColored(name: "engelliye uygun asansörlerde asansör kapısının yanında bilgilendirici işaret ve/veya işaretler olmalıdır.yerden 1,80 ila 2,50 m yukarıda ''ASANSÖR'' yazısı olmalıdır."),
       new SubStageColored(name: "imdat ve kapı butonları kabin tabanından min 90 cmde başlamalıdır."),
       new SubStageColored(name: "Kabinin en az bir yan duvarına tutamak takılmalıdır. Tutamağın yerden yüksekliği 90+- 2,5 cm şeklinde olamalıdır"),
       new SubStageColored(name: "planlı alanlar imar yönetmeliğine göre asansör kabinin dar kenarı min. 120 cm; kabin alanı min 1,8 m²; kapı genişliği min 90 cm olmalıdır. (1 ekim 2017)"),
       new SubStageColored(name: "kapının yerinin kolayca bulunabilmesi için, kpı rengi ve renk tonu kendini çevreleyen duvarın rengiyle zıtlık oluşturmalıdır"),
       new SubStageColored(name: "engeliler için kabin ve durak kapıları tam otomatik olmak zorundadır."),
       new SubStageColored(name: "birden fazla asansör bulunan yapılarda asansör sayısının yarısı kadar asansör engelli yönetmeliği ölçülerini sağlamalıdır. (örn: 3 asansörde 1 asansör sağlaması yeterlidir.)"),
       new SubStageColored(name: "herhangi bir buton ile komşu duvar arasındaki mesafe, kat için en az 50 cm; kabin butonları için ise en az 40 cm'dir.")

     ];

     List<SubStageColored> ss1_5=[
       new SubStageColored(name: " asansör yaptırıcısı tarafından kontrol panosu ucuna kadar yangın algılama sensörünün tesisat uçları getirilmelidir"),
       new SubStageColored(name: "yangın söndürücülerin etkin hale gelmesi; sadece asansör durakta duruyorken ve asansör elektrik beslemesi ve aydınlatma donanımı otomatik bir şekilde yangın veya duman tespit sistemi tarafından kapatıldığında mümkün olmalıdır"),
       new SubStageColored(name: " bina yüksekliği 21,5 m'den fazla; yapı yüksekliği 30,5 m'den fazla olan binalarda, asansör yangın uyarısı aldığında otomatik kapılı asansörler, durakta park halindeyken kapılarını kapatıp belirlenmiş durağa duraksız hareket etmelidir"),
       new SubStageColored(name: "tüm yapılarda ve tüm asansörlerde asansörün yangın anında kullanılmayacağına ait işaret ve gösterge bütün duraklarda bulunmalıdır"),
       new SubStageColored(name: "yüksek binalarda, topluma açık yapılarda yangın anında asansör kabininin belirlenmiş durağa gitmesi ve yolcuların çıkmasının sağlanması amacaıyla elketrik sinyali otomatik yangın algılama ve alarm sistemi veya elle çağırma tertibatı tarafından sağlanmalıdır"),
     ];
     ss1 = [
       new SubStageColored(number: 1.1, color: Colors.blue, name: "Kuyuya ve kuyu alt boşluğuna erişim için kilitleme tertibatları**", subs: ss1_1),
       new SubStageColored(number: 1.2, color: Colors.blue, name:"Makine veya makara dairesi giriş kapısı (kilit, açılma yönü ve uyarı levhası)", subs: ss1_2),
       new SubStageColored(number: 1.3,color: Colors.yellow, name:"Kurtarma talimatı *(Türkçe)",subs: ss1_3),
       new SubStageColored(number: 1.4,color: Colors.yellow, name:"Makina ve makara dairesinde yeterli aydınlatma*",subs: ss1_4),
       new SubStageColored(number: 1.5, color: Colors.blue, name:"Makina veya makara dairesinde kaymayan zemin ",subs: ss1_5),
     ];
   }
   
   List<SubStageColored> ss1;

   final int test_id;


   @override
  _SS7State createState() => _SS7State();
}

class _SS7State extends State<SS7> {
  

  bool done;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
        if(super.widget.ss1[i].subs != null)
        
          for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
          {
            super.widget.ss1[i].subs[j].done = false;
          }

    }
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,6,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
