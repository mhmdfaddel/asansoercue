import 'package:asansorci/inProgress_Standard/Expansione.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';


class SS2 extends StatefulWidget {
   SS2({Key key, int id, this.test_id}) : super(key: key) {

     List<SubStageColored> ss1_1 = [
       new SubStageColored(name: "bakım kumanda tertibatı kabin üstünde sığınma alanının yatayla 30 cm mesafesinde olmalıdır. 81-20'deYukarı yön beyaz;aşağı yön siyah ortak yön butonu bulunmalı ve çalıştırma butonu mavi renkli olmalıdır (üzerinde 'çalıştırma' yazmalıdır).",done: false),
       new SubStageColored(name: "revizyon modunda bütün emniyet devreleri aktif olmalıdır ve herhangi bir güvenlik devresi aktif değilse çalışmamalıdır.",done: false),
       new SubStageColored(name: "kabin üstü bakım kumandası devrede iken asansörün hızı 0,63 m/s'yi aşmamalıdır.",done: false),
       new SubStageColored(name: "kabin üstü bakım kumandasının butonları yanlışlıkla çalıştırmaya karşı (0-1 konumlu) korumalı olmalıdır.",done: false),
       new SubStageColored(name: "kabin üstü bakım kumandası üzerinde yönler ve işaretlemeler açıkça belirtilmelidir.",done: false),
       new SubStageColored(name: "bakım kumandası üzerinde çift konumlu kararlı stop butonu bulunmalıdır.",done: false),
       new SubStageColored(name: "kabin üstü ve kuyu dibinden revizyona alındığında asansörün hareketi her iki istasyondaki aynı yöndeki butonlara aynı anda basılmasıyla olmalıdır.",done: false),
     ];
     List<SubStageColored> ss1_2= [
       new SubStageColored(name: " kabin üstünde kolay erişilen bir yerde, bakım veya kontrol elemanlarının giriş yerinden en çok 1 m uzaklıkta durdurma tertibatı olmalıdır. (bakım kumandası üzerindeki durdurma tertibatı bu şartı sağlıyorsa ilave durdurma tertibatı aranmaz.)"),
     ];

     List<SubStageColored> ss1_3=[
       new SubStageColored(name: " kabin üstünden yatayda dik mesafe 30 cm olunca kabin üstü korkuluk zorunludur. Korkuluk yüksekliği 30-50 cm arası yatay açıklıkta 70 cm; 50 cm üstü açıklıkta 110 cm olmalıdır."),
       new SubStageColored(name: "kabin üstü korkuluk sabitlenmelidir"),
       new SubStageColored(name: " kabin üstü korkuluk bir el tutamağı 10 cm yükseklikte bir ayak koruyucu ve korkuluğun yarı yüksekliğinde yerleştirilmiş bir ara çubuktan meydana gelmelidir."),
       new SubStageColored(name: "kabin üstü korkuluğun dış kenarı ile kuyu içindeki herhagi bir komponent arasındaki mesafe en az 10 cm olmalıdır"),
       new SubStageColored(name: "kabin üstü korkuluk, kabin üstünün kenarından en fazla 15 cm mesafeye konulmalıdır"),

     ];

     List<SubStageColored> ss1_5=[
       new SubStageColored(name: "kabin çatısına acil durumda monte edilen kapağın ölçüsü 0,40x0,50 m net açıklık boyutlarınd olmalıdır. Eğer yer varsa bu ölçü 0,50x0,70 olabilir."),
     ];

     List<SubStageColored> ss1_6=[
       new SubStageColored(name: "karşı ağırlık askı halat bağlantıları ters kelepçeleri düzeltilmeli; eksik kelepçeleri tamamlanmalıdır"),
       new SubStageColored(name: "karşı ağırlık askı halat bağlantıları gevşek kontra somunları sıkılmalıdır."),
       new SubStageColored(name: "askı halatlarındaki gerilmeleri dengelemek için, bunların en az bir ucunda kendiliğinden çalışan bir tertibat bulunmalıdır. (en az bir tarafında yay veya esnek eleman olmalıdır.)"),
       new SubStageColored(name: "kabin askı halat bağlantırları gevşek kontra somunları sıkılmalıdır"),
       new SubStageColored(name: "karşı ağırlık askı halat bağlantıları lastik takozları/yayları yenilenmelidir"),
       new SubStageColored(name: "karşı ağırlık askı halat bağlantı şişeleri yenilenmelidir."),
       new SubStageColored(name: "kabin askı halat bağlantı şişeleri yenilenmelidir"),
       new SubStageColored(name: "kabin askı halat bağlantıları eksik kontra somunları tamamlanmalıdır"),
       new SubStageColored(name: "karşı ağırlık askı halat bağlantıları eksik kontra somunları tamamlanmalıd"),
       new SubStageColored(name: "kabin askı halat bağlantıları lastik takozları/yayları yenilenmelidir."),
       new SubStageColored(name: "kabinin tüm seyri boyunca askı halatlarının kabin/kabin karkası veya kuyu duvarlarına sürtünmesi önlenmelidi"),
       new SubStageColored(name: "kabin askı halat bağlantıları eksik kelepçeleri tamamlanmalıdır. (en az 2 adet olmalıdır.)"),
       new SubStageColored(name: "kabin askı halat bağlantıları eksik gupilyaları tamamlanmalı ve ters kelepçeleri düzeltilmelidir"),
     ];

     List<SubStageColored> ss1_7=[
       new SubStageColored(name: "kapı emniyet kontakları köprülenmiş. (köprülenmemelidir.)"),
       new SubStageColored(name: "kapı emniyet kontakları ayarsız."),
       new SubStageColored(name: "kapı kilitleri çalışmıyor."),
       new SubStageColored(name: "kapı kilitleri ayarsız"),
       new SubStageColored(name: "kapı kilidi 2. emniyetleri yok; çalışmıyor; ayarsız."),
       new SubStageColored(name: "kat kapı kilit muhafazaları takılmalı"),
       new SubStageColored(name: "kat kapı kilit kolları makara ve lastikleri takılmalıdır."),
       new SubStageColored(name: "kat kapı kilit pimi yuvasına en az 7 mm girecek şekilde ayarlanmalıdır")
     ];

     List<SubStageColored> ss1_10=[
       new SubStageColored(name: "kuyu duvarı cam ise lamine camdan imal edilmiş olmalıdır."),
       new SubStageColored(name: "kuyu zemini kaygan olmayan malzemelerden olmalıdır"),

     ];

     List<SubStageColored> ss1_11=[
       new SubStageColored(name: "kabin ray konsolları eksik montaj cıvata- somunları tamamlanmalıdır"),
       new SubStageColored(name: "kabin ray konsolları duvara sabitlenmelidir"),
       new SubStageColored(name: "kabin eksik ray konsolları tamamlanmalıdır."),
       new SubStageColored(name: "kabin altı paten tutucuların eksik civataları tamamlanmalıdır"),
       new SubStageColored(name: "kabin paten boşlukları ayarlanmalıdır"),
       new SubStageColored(name: "kabin kılavuz rayları tek taraftan sabitlenmelidir"),
       new SubStageColored(name: "kabin ray flanşlarının eksik cıvata somunları tamamlanmalıdır"),
       new SubStageColored(name: "kabin kılavuz rayları birleştirmelerinde kaynaklı kısımlar flanşlı ve civatalı bağlantı olmalıdır"),
       new SubStageColored(name: "kabin ray konsol bağlantılarındaki eksik tırnaklar tamamlanmalıdır."),
       new SubStageColored(name: "kabin ray konsol bağlantılarındaki gevşek montaj civataları sıkılmalıdır."),

     ];

     List<SubStageColored> ss1_12=[
       new SubStageColored(name: "kuyu içi elektrik tesisatı buat kapakları kapatılmalıdır. Ve kablo ekleri buat içine alnmalıdır."),
       new SubStageColored(name: "kuyu içi elektrik tesisatı kablo kanal kapakları kapatılmalıdır"),
       new SubStageColored(name: "kuyu içinde açıkta olan elektrik kablo bağlantıları koruma altına alınmalıdır"),
       new SubStageColored(name: "kabin üstü dağınık kablo bağlantıları düzenlenmelidir.sabitlenmeli ve koruma altına alınmalıdır"),
       new SubStageColored(name: "(varsa) kabin üstü bakımcı kumandasının üzerindeki korumasız lamba/duy etanj olmalıdır.bu lambalar kabin aydınlatma devresinebağlı ve kabin üzerinden anahtarlamalı olmalıdır."),
       new SubStageColored(name: "kabin üstü priz çalışırhale getirilmelidir."),
       new SubStageColored(name: "kabin üstüne topraklı priz takılmalıdır"),
       new SubStageColored(name: "kabin üstünde kablo bağlantılarındaki izolesiz kısımlar koruma altına alınmalıd"),
       new SubStageColored(name: "kabin üstü havalandırma fanı muhafaza içine alınmalıdır."),
       new SubStageColored(name: "kabin üstü kablo bağlantı ek kısımları klemens kutusu içerisine alınmalıdır"),
       new SubStageColored(name: "asansör kuyusundaki tesisata doğrudan dokunmaya karşı korunma, en az IP2X koruma derecesine sahip mahfazalarla sağlanmalıdır bunların haricindeki bağlantılar, klemensler ve konnektörler, bu amaç için yapılan pano buat veya tabloların içinde bulunmalıdır")
     ];

     List<SubStageColored> ss1_13=[
       new SubStageColored(name: "kabin üstü sığınma alanı dik duruş için; 0,40*0,50*2,00 m ; çömelme için; 0,50*0,70*1,00 m olmalıdır"),
       new SubStageColored(name: "kabin üzerinde bulunan en yüksek parça ile kuyu tavanı altında bulunan ilk parçaya olan uzaklık yatay ve dikey olarak ölçüldüğünde en az 50 cm olmalıdır"),
     ];

     List<SubStageColored> ss1_14=[
       new SubStageColored(name: "kabin üstü bakım kumandası butonlarının üstünde veya yakınında, hareket yönü işaretlenmelidir."),
       new SubStageColored(name: " kabin üstü durdurma butonunun üstünde veya yakınında durdurma konumunun karıştırılması riski olmayacak bir şekilde (DUR/STOP) kelimesi olmalıdır."),
       new SubStageColored(name: "kabin üstü bakım kumandası anahtarının üstünde veya yakınında (NORMAL, BAKIM) kelimeleri olmalıdır"),
       new SubStageColored(name: "kabin üstü korkuluk üzerinde uyarı levhası veya yazısı olmalıdır"),
       new SubStageColored(name: " durak kapılarının kilitleme tertibatı üzerinde klitleme tertibatını imal eden firmanın adı ve tip kontrolü ile ilgili işaret ve referansları ve CE işareti içeren bir bilgi levhası bulunmalıdır"),
       new SubStageColored(name: "kabin çatısı üzerinde bulunan ve kabin çatısına erişim imkanı veren duraklardan okunabilir bir işaret, sığınma alanı/alanları için ayrılması düşünülen alanlara müsaade edilen kişi sayısını ve duruş tipini açıkça belirtmelidir"),
     ];

     List<SubStageColored> ss1_18=[
       new SubStageColored(name: "tahrik kasnağının duvara ve zemine teması önlenmelidir."),
       new SubStageColored(name: "tahrik kasnağı çıkışında halatların birbirine teması önlenmelidir."),
     ];

     List<SubStageColored> ss1_19=[
       new SubStageColored(name: "saptırma kasnağının duvara ve zemine teması önlenmelidir."),

     ];

     List<SubStageColored> ss1_20=[
       new SubStageColored(name: "makaralı şalter olmalıdır. (ce işareti)")
     ];

     List<SubStageColored> ss1_31=[
       new SubStageColored(name: "Kuyu aydınlatma sigortası takılmalı ve adreslenmelidir."),
       new SubStageColored(name: "Elektrik panosunda motor hattı için 4lü grup W otomat otomat takılmalıdır."),
       new SubStageColored(name: "4lü grup W otomatın beslemesi kaçak akım rölesinden sonra olmalıdır."),
       new SubStageColored(name: "MD aydınlatma sigortası takılmalı ve adreslenmelidir."),
       new SubStageColored(name: "Kabin aydınlatma sigortası takılmalı ve adreslenmelidir"),

     ];

     List<SubStageColored> ss1_33=[
       new SubStageColored(name: "50 V AC'den daha büyük gerilim altında olan kontrol devrelerine sahip kat kumanda ve kat göstergeleri ile emniyet devrelerinde hata akımına karşı koruma (30 mA)"),
       new SubStageColored(name: "50 V AC'den daha büyük gerilim altında çalışan asansör kabini üzerindeki devrelerde hata akımına karşı koruma (30 mA)"),
       new SubStageColored(name: "Kabin ve kuyu aydınlatmasına dahil priz devreleri (30 mA)"),
       new SubStageColored(name: "Güç devresi ve buna bağlı devrelerin ana şalterinde eşik değeri topraklama direncine bağlı olarak seçilen ve uygulanan hata akımına karşı koruma"),
     ];

     List<SubStageColored> ss1_34=[
       new SubStageColored(name: "elektrik kuvvet panosu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "hidrolik tamponların topraklama bağlantıları yapılmalıdır."),
       new SubStageColored(name: "kabin topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "topraklama kablo bağlantıları yüksük veya civatalı veya kablo pabucu ile yapılmalıdır."),
       new SubStageColored(name: "kumanda panosu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "makine motor grubu topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "hız regülatörü topraklama bağlantısı yapılmalıdır."),
       new SubStageColored(name: "sınır kesici şalter topraklama bağlantısı yapılmalıdır.(3 faz sınır kesiciler için geçerlidir.)"),
       new SubStageColored(name: "kabinve kat butonyer topraklama bağlantısı yapılmalıdır."),
     ];

     List<SubStageColored> ss1_35=[
       new SubStageColored(name: "kumanda panosunda bulunan tüm kompenentler panoya sabitlenmelidir."),
       new SubStageColored(name: "kumanda panosu kompenent rumuzları devre şemasına uygun hale getirilmelidir."),
       new SubStageColored(name: "kumanda panosu kablo kanal kapakları kapatılmalıdır."),
       new SubStageColored(name: "kumanda panosu sabitlenmelidir."),
     ];

     List<SubStageColored> ss1_38=[
       new SubStageColored(name: "by-pass işareti yada yazısı olmalı ve elektrik diyagramı bulunmalıdır."),
     ];

     List<SubStageColored> ss1_44=[
       new SubStageColored(name: "hız regülatörü kuyu içinde ise dışarıdan erişilebilir olmalıdır.50*60 cm'lik bir yatay alan olmalıdır."),
       new SubStageColored(name: "hız regülatörü sehpası sabitlenmelidir."),
       new SubStageColored(name: "hız regülatör makarasının duvara teması engellenmelidir."),
       new SubStageColored(name: "hız regülatörü emniyet kontağı sabitlenmelidir"),
     ];
     ss1 = [
       new SubStageColored(number: 2.1, color: Colors.yellow, name: "Kabin üstünde bakım kumandası*", subs: ss1_1),
       new SubStageColored(number: 2.2, color: Colors.yellow, name:"Kabin üstünde durdurma tertibatı*", subs: ss1_2),
       new SubStageColored(number: 2.3,color: Colors.yellow, name:"Kabin üstünden düşmeye karşı koruma*",subs: ss1_3),
       new SubStageColored(number: 2.4,color: Colors.blue, name:"Yeterli kabin tavanı ve varsa imdat kapağı mukavemeti"),
       new SubStageColored(number: 2.5, color: Colors.yellow, name:"kabin İmdat kapı ve kapaklarının kilitlenmesi*",subs: ss1_5),
       new SubStageColored(number: 2.6, color: Colors.red, name:"Kabin ve karşı ağırlık askı halatlarının ve bağlantı elemanlarının genel durumu**",subs: ss1_6),
       new SubStageColored(number: 2.7, color: Colors.red, name:"Kat kapısı kilitleme tertibatı**",subs: ss1_7),
       new SubStageColored(number: 2.8,color: Colors.yellow, name:"Kat kapısı kilitleme tertibatına yetkisiz kişilerce erişilememesi* (kuyu duvarları yetkisiz kişi erişmesin diye deliksiz olmalıdır.)"),
       new SubStageColored(number: 2.9,color: Colors.yellow, name:"Kısmen kapalı kuyularda koruma önlemleri* (gerek duyulunca genel checklistten bakılacak.)"),
       new SubStageColored(number: 2.10,color: Colors.blue, name:"Kuyu duvarı, kuyu tabanı ve tavanının uygunluğu",subs: ss1_10),
       new SubStageColored(number: 2.11,color: Colors.blue, name:"Klavuz raylar, bağlatı elemanları ve bağlantının uygunluğu",subs: ss1_11),
       new SubStageColored(number: 2.12,color: Colors.blue, name:" Kuyu ve kabin üstü elektrik tesisatı",subs: ss1_12),
       new SubStageColored(number: 2.13,color: Colors.blue, name:"Kuyu üst boşluğunda güvenlik alanı",subs: ss1_13),
       new SubStageColored(number: 2.14,color: Colors.blue, name:"Asansörün güvenli kullanımına ve bakımına ilişkin bilgiler",subs: ss1_14),
     ];

   }

   List<SubStageColored> ss1;

   final int test_id;

  @override
  _SS2State createState() => _SS2State();
}

class _SS2State extends State<SS2> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    for(int i =0; i < super.widget.ss1.length;i++)
    {
      super.widget.ss1[i].done = false;
      if(super.widget.ss1[i].subs != null)
        for(int j = 0; j < super.widget.ss1[i].subs.length; j++)
        {
          super.widget.ss1[i].subs[j].done = false;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: List.generate(super.widget.ss1.length, (index) =>
                  Expansione(super.widget.ss1[index].name, super.widget.ss1[index].color, super.widget.ss1[index].subs,super.widget.ss1[index].done, super.widget.test_id,1,index)
              ),
            ),
          ),
        ],
      ),
    );
  }
}
