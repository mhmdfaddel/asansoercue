import 'package:asansorci/Models/ProgressClass.dart';
import 'package:asansorci/Models/Test_Book.dart';
import 'package:asansorci/inProgress_Standard/stages/SS1.dart';
import 'package:asansorci/inProgress_Standard/stages/SS2.dart';
import 'package:asansorci/inProgress_Standard/stages/SS3.dart';
import 'package:asansorci/inProgress_Standard/stages/SS4.dart';
import 'package:asansorci/inProgress_Standard/stages/SS5.dart';
import 'package:asansorci/inProgress_Standard/stages/SS6.dart';
import 'package:asansorci/inProgress_Standard/stages/SS7.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:easy_localization/easy_localization.dart';


class StandardView extends StatefulWidget {
  StandardView({Key key}) : super(key: key);



  @override
  _StandardViewState createState() => _StandardViewState();
}

class _StandardViewState extends State<StandardView> with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  bool first_time = true;
  Test_Book temp;

  List<String> labels = ["MAKİNA VEYA MAKARA DAİRESİNDEKİ KONTROLLER",
    "KABİN ÜSTÜNDEKİ VE KUYU İÇERİSİNDEKİ KONTROLLER",
    "KUYU DİBİ VEYA KUYU İÇERİSİNDEKİ KONTROLLER",
    "KABİN VE KAT KAPILARINDAKİ KONTROLLER",
    "KABİN İÇERİSİNDEKİ KONTROLLER",
    "MAKİNE DAİRESİZ ASANSÖRLER İÇİN EK KONTROLLER",
    "DİĞER KONTROLLER"];

  SS1 ss1 = new SS1();
  SS2 ss2 = new SS2();
  SS3 ss3 = new SS3();
  SS4 ss4 = new SS4();
  SS5 ss5 = new SS5();
  SS6 ss6 = new SS6();
  SS7 ss7 = new SS7();

  List<Widget> list = [
    Tab(text: "1"),
    Tab(text: "2"),
    Tab(text: "3"),
    Tab(text: "4"),
    Tab(text: "5"),
    Tab(text: "6"),
    Tab(text: "7"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft
    ]);
    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
    });


  }

  @override
  void dispose() {
    // TODO: implement dispose
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, Object> rcvdData = ModalRoute.of(context).settings.arguments;


    if (rcvdData["ProgressClassview"] != null) temp = rcvdData["ProgressClassview"];

    if(first_time) {
      first_time = false;
      int currentindex = int.parse(temp.stageid.substring(0, temp.stageid.indexOf('.'))),
          mainindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('.')+1, temp.stageid.indexOf('_'))),
          subindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('_')+1, temp.stageid.length));
      prepareSS(currentindex, mainindex, subindex, temp);

      _controller.index = currentindex;

    }
    ss1 = new SS1(test_id: temp.id,);
    ss2 = new SS2(test_id: temp.id,);
    ss3 = new SS3(test_id: temp.id,);
    ss4= new SS4(test_id: temp.id,);
    ss5= new SS5(test_id: temp.id,);
    ss6= new SS6(test_id: temp.id,);
    ss7= new SS7(test_id: temp.id,);
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[700],
        bottom: TabBar(
          onTap: (index) {
            // Should not used it as it only called when tab options are clicked,
            // not when user swapped
          },
          controller: _controller,
          tabs: list,
        ),
        title: Text(labels[_selectedIndex].tr().toString()),
      ),
      body: TabBarView(
        controller: _controller,
        children: [
          ss1,
          ss2,
          ss3,
          ss4,
          ss5,
          ss6,
          ss7,
        ],
      ),
    );
  }

  void prepareSS(int currentindex, int mainindex, int subindex, Test_Book tempo)
  {
    if(currentindex ==0)
    {
      for(int i =0; i< mainindex;i++)
      {
        ss1.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
          ss1.ss1[i].subs[j].done = true;
      }
    }
    else if(currentindex ==1)
    {
      for(int i =0; i< mainindex;i++)
      {
        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
        {
          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
        }
      }
    }
    else if(currentindex ==2)
    {
      for(int i =0; i< mainindex;i++)
      {

        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        ss3.ss1[i].done = true;
        for(int j = 0; j < subindex;j++) {

          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
          ss3.ss1[i].subs[j].done = true;

        }
      }
    }
    else if(currentindex ==3){
      for(int i =0; i< mainindex;i++)
      {

        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        ss3.ss1[i].done = true;
        ss4.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
        {

          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
          ss3.ss1[i].subs[j].done = true;
          ss4.ss1[i].subs[j].done = true;
        }
      }
    }
    else if(currentindex ==4){
      for(int i =0; i< mainindex;i++)
      {
        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        ss3.ss1[i].done = true;
        ss4.ss1[i].done = true;
        ss5.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
        {

          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
          ss3.ss1[i].subs[j].done = true;
          ss4.ss1[i].subs[j].done = true;
          ss5.ss1[i].subs[j].done = true;
        }
      }
    }
    else if(currentindex ==5){
      for(int i =0; i< mainindex;i++)
      {
        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        ss3.ss1[i].done = true;
        ss4.ss1[i].done = true;
        ss5.ss1[i].done = true;
        ss6.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
        {
          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
          ss3.ss1[i].subs[j].done = true;
          ss4.ss1[i].subs[j].done = true;
          ss5.ss1[i].subs[j].done = true;
          ss6.ss1[i].subs[j].done = true;
        }
      }
    }
    else if(currentindex ==6){
      for(int i =0; i< mainindex;i++)
      {
        ss1.ss1[i].done = true;
        ss2.ss1[i].done = true;
        ss3.ss1[i].done = true;
        ss4.ss1[i].done = true;
        ss5.ss1[i].done = true;
        ss6.ss1[i].done = true;
        ss7.ss1[i].done = true;
        for(int j = 0; j < subindex;j++)
        {
          ss1.ss1[i].subs[j].done = true;
          ss2.ss1[i].subs[j].done = true;
          ss3.ss1[i].subs[j].done = true;
          ss4.ss1[i].subs[j].done = true;
          ss5.ss1[i].subs[j].done = true;
          ss6.ss1[i].subs[j].done = true;
          ss7.ss1[i].subs[j].done = true;
        }
      }
    }
  }
}



