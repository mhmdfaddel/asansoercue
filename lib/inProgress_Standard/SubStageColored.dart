import 'dart:ui';

class SubStageColored {
  double number;
  bool done;
  String name;
  Color color;

  List<SubStageColored> subs = new List<SubStageColored>();
  SubStageColored({this.name, this.color,this.done,this.number,this.subs});
}