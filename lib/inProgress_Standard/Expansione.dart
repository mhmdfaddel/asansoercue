import 'dart:convert';
import 'package:asansorci/accessories/Constants.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:asansorci/Models/Test_Book.dart';
import 'package:asansorci/inProgress_Standard/SubStageColored.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';


class Expansione extends StatefulWidget {
  String name;
  Color color;
  bool value;
  int id;
  int mainterm;
  List<SubStageColored> subse = new List<SubStageColored>();
  int test_id;

  Expansione(String s, Color color, List<SubStageColored> subs, bool done, int id, int thisid, int main)
  {
    this.mainterm = main;
    this.id = thisid;
    this.test_id = id;
    this.name = s;
    this.color = color;
    this.value = done;
    if(subs != null)
      this.subse.addAll(subs);
  }

  @override
  _ExpansioneState createState() => _ExpansioneState();
}

class _ExpansioneState extends State<Expansione> {

  bool test=false;
  Constants consts = new Constants();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ExpansionTile(
          collapsedBackgroundColor: super.widget.color,
          title: Row(
            children: [
              Expanded(
                child: Text(
                  "${super.widget.name}".tr().toString(),
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Checkbox(
                  activeColor: Colors.green,
                  value: test,
                  onChanged: (value){
                setState(() {
                  test = value;
                  if(value){
                    if(value)
                    {
                      String stageid = super.widget.id.toString()+'.'+super.widget.mainterm.toString()+'_0';
                      Test_Book tempo = new Test_Book();
                      tempo.id = super.widget.test_id;
                      tempo.stageid = stageid;
                      postTest_book(tempo);
                    }
                  }
                });
              })
            ],
          ),
          children: List.generate(super.widget.subse.length, (index) => Column(
            children: [
              new ListTile(
                title: Text(
                    super.widget.subse[index].name.tr().toString()
                ),
                leading: Checkbox(
                  activeColor: Colors.green,
                  value: (super.widget.subse[index].done != null)?super.widget.subse[index].done:false,
                  onChanged: (value){
                    setState(() {
                      super.widget.subse[index].done = value;
                      if(value)
                        {
                          String stageid = super.widget.id.toString()+'.'+super.widget.mainterm.toString()+'_'+index.toString();
                          Test_Book tempo = new Test_Book();
                          print(super.widget.id);
                          tempo.id = super.widget.test_id;
                          tempo.stageid = stageid;
                          postTest_book(tempo);
                        }

                    });
                  },
                ),
              ),
              Divider(height: 5.0, color: Colors.black45,)
            ],
          )
          ),
        ),
        Divider(height: 2.0, color: Colors.black45,),
      ],
    );
  }

  Future<void> postTest_book(Test_Book temp) async{
    Uri url = Uri.https(
        consts.domain, consts.domain_test_book+'edit/'+temp.id.toString());
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });

  }
}
