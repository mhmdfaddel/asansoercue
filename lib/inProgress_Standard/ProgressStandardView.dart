import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/ProgressClass.dart';
import 'package:asansorci/Models/Test_Book.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/employees/Employee.dart';
import 'package:asansorci/inProgress_Standard/stages/SS1.dart';
import 'package:asansorci/inProgress_Standard/stages/SS2.dart';
import 'package:asansorci/inProgress_Standard/stages/SS3.dart';
import 'package:asansorci/inProgress_Standard/stages/SS4.dart';
import 'package:asansorci/inProgress_Standard/stages/SS5.dart';
import 'package:asansorci/inProgress_Standard/stages/SS6.dart';
import 'package:asansorci/inProgress_Standard/stages/SS7.dart';
import 'package:asansorci/inProgress_short/InProgressLift.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProgressStandardView extends StatefulWidget {
  @override
  _ProgressStandardViewState createState() => _ProgressStandardViewState();
}

class _ProgressStandardViewState extends State<ProgressStandardView> {
  final String appTitle = 'ASANSÖRCÜ';

  int _currentStep = 1;
  Constants consts = new Constants();
  List<Test_Book> tests;

  List<ProgressClass> myLifts;

  TextEditingController search = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    tests = [];
    myLifts = [];
    getTests();
    super.initState();
  }

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        myLifts.clear();
        temps.clear();
        tests.clear();
        getTests();
      });
    _refreshController.loadComplete();
  }
  String _dropDownValue;

  List<ProgressClass> temps = [];

  List<ProgressClass> existLift(String temp, String cirtic) {
    List<ProgressClass> testo = new List<ProgressClass>();


    if(cirtic == 'Fake ID'.tr().toString())
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].fakeId.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    else if(cirtic == 'BINA NUMARASI'.tr().toString()){
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].buildingNumber.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    }
    else if(cirtic == 'BINA'.tr().toString()){
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].buildingName.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    }
    return testo;
  }

  _showDialog(ProgressClass temp, Test_Book tempo) async {
    await showDialog<String>(
      context: context,
      builder: (context) =>  new AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                autofocus: true,
                controller: passwordController,
                decoration: new InputDecoration(
                    labelText: 'Password'.tr().toString()),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child:  Text('Cancel'.tr().toString()),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child:  Text('Ok'),
              onPressed: () {
                if(passwordController.text == (new Constants()).password)
                {
                  myLifts.remove(temp);
                  deleteRequest(tempo);
                  Navigator.pop(context);
                }
              })
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Tests Book".tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid), // set border width
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: TextField(
                controller: search,
                onChanged: (value) =>{
                  setState(() {
                    if(this.search.value.text == "")
                    {
                      myLifts = temps;
                    }
                    else if(_dropDownValue == 'Fake ID'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'Fake ID'.tr().toString());
                    }
                    else if(_dropDownValue == 'BINA NUMARASI'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'BINA NUMARASI'.tr().toString());
                    }
                    else if(_dropDownValue == 'BINA'.tr().toString())
                    {
                      myLifts = existLift(search.value.text, 'BINA'.tr().toString());
                    }
                  })
                },
                decoration: InputDecoration(
                  hintText: "search_elevator".tr().toString(),
                  hintStyle: TextStyle(
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.02,
                  MediaQuery.of(context).size.width * 0.02,
                  0,
                  MediaQuery.of(context).size.width * 0.02),
              child: Row(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.width * 0.37,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text("search_by".tr().toString())
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: Colors.blue),
                              ),
                        isExpanded: true,
                        iconSize: 15.0,
                        style: TextStyle(color: Colors.blue),
                        items: [
                          "Fake ID".tr().toString(),
                          "building_number".tr().toString(),
                          "building_name".tr().toString()
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val;
                            },
                          );
                        },
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.32, 0, 0, 0),
                    child: Text(
                      'total'.tr().toString() +
                          ' : ' +
                          this.tests.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                height:
                    (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                        ? MediaQuery.of(context).size.height * 0.5887
                        : MediaQuery.of(context).size.height * 0.602,
                width: MediaQuery.of(context).size.width * 1,
                child: Row(
                  children: [
                    Container(
                      child: Expanded(
                        child: ListView.builder(
                          itemCount: this.tests.length,
                          itemBuilder: (context, index) =>
                              this._buildRow(this.myLifts[index], this.tests[index]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            copyRights(),
          ],
        ),
      ),

    );
  }

  Widget _buildRow(ProgressClass temp, Test_Book tempo) {
    var height = MediaQuery.of(context).size.height,
        width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onHorizontalDragStart: (DragStartDetails details){
        setState(() {
          temp.color = Colors.redAccent;
        });
      },
      onHorizontalDragEnd: (DragEndDetails details){
        if (details.primaryVelocity > 0) {
          setState(() {
            _showDialog(temp, tempo);

          });
        }
        else
        {
          setState(() {
            temp.color = Colors.white;
          });
        }
      },

      child: Card(
        shadowColor: Colors.blue,
        margin: const EdgeInsets.all(10),
        elevation: 30.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        color: temp.color,
        child: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RawMaterialButton(
                  onPressed: () {
                    print(tempo.stageid.substring(tempo.stageid.indexOf('.')+1, tempo.stageid.indexOf('_')));
                    int currentindex = int.parse(tempo.stageid.substring(0, tempo.stageid.indexOf('.'))),
                        mainindex = int.parse(tempo.stageid.substring(tempo.stageid.indexOf('.')+1, tempo.stageid.indexOf('_'))),
                        subindex = int.parse(tempo.stageid.substring(tempo.stageid.indexOf('_')+1, tempo.stageid.length));
                    _awaitReturnValueFromSecondScreen(context, tempo,currentindex, mainindex, subindex);
                  },
                  elevation: 2.0,
                  fillColor: Colors.blue,
                  child: Text((int.parse(tempo.stageid.substring(0, tempo.stageid.indexOf('.')))+1).toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          )),
                  padding: EdgeInsets.all(15.0),
                  shape: CircleBorder(),
                ),
                SizedBox(height: 2,),
                RawMaterialButton(onPressed: (){

                },
                elevation: 2.0,
                fillColor: Colors.blueGrey,
                  shape: CircleBorder(),
                  child: IconButton(
                  padding: EdgeInsets.all(10.0),
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.green,
                    size: 20.0,
                  ),
                    onPressed: () {

                    },
                  ),
                ),

                SizedBox(height: 2,),

                RawMaterialButton(onPressed: (){

                },
                  elevation: 2.0,
                  fillColor: Colors.blueGrey,
                  shape: CircleBorder(),
                  child: IconButton(
                    padding: EdgeInsets.all(10.0),
                    icon: Icon(
                      Icons.info,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    onPressed: () {
                      setState(() {
                        showDialog(context: context, builder: (BuildContext){
                          return SingleChildScrollView(
                            child: Dialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.8,
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        color: Colors.white24,
                                        child: SizedBox.expand(
                                          child: Padding(
                                            padding: const EdgeInsets.all(1.0),
                                            child: Column(
                                              mainAxisAlignment:
                                              MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.13,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        enabled: false,
                                                        decoration: InputDecoration(
                                                          hintText:
                                                          '#        '+temp.buildingNumber,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                        ),
                                                      ),
                                                      TextField(
                                                        enabled: false,
                                                        decoration: InputDecoration(
                                                          hintText: temp.buildingName,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                           icon:
                                                           Icon(Icons.house_siding_sharp),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.13,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        enabled: false,
                                                        decoration: InputDecoration(
                                                          hintText: temp
                                                              .committedName,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons
                                                              .person),
                                                        ),
                                                      ),
                                                      TextField(
                                                        enabled: false,
                                                        decoration: InputDecoration(
                                                          hintText: temp
                                                              .committedNumber,
                                                          hintStyle: TextStyle(
                                                          ),
                                                          border: InputBorder.none,
                                                          icon: Icon(Icons
                                                              .phone),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),

                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.06,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: TextField(
                                                    enabled: false,
                                                    decoration: InputDecoration(
                                                      hintText: temp.liftType.tr().toString(),
                                                      hintStyle: TextStyle(
                                                      ),
                                                      border: InputBorder.none,
                                                      icon: Icon(Icons
                                                          .merge_type),
                                                    ),
                                                  ),
                                                ),


                                                Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                      0.2,
                                                  margin: EdgeInsets.all(10),
                                                  padding: EdgeInsets.fromLTRB(
                                                      10.0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        // set border color
                                                        width: 0.5,
                                                        style: BorderStyle
                                                            .solid), // set border width
                                                    // set rounded corner radius
                                                  ),
                                                  child: SingleChildScrollView(
                                                    scrollDirection: Axis.vertical,//.horizontal

                                                    child: Wrap(

                                                      children: [
                                                        Text('+ '+currentStage(tempo).substring(0, currentStage(tempo).indexOf(',')).tr().toString(), style: TextStyle(
                                                          fontSize: 20,
                                                        ),),

                                                        Text('+ '+currentStage(tempo).substring(currentStage(tempo).indexOf(',')+1, currentStage(tempo).length).tr().toString(), style: TextStyle(
                                                          fontSize: 18,
                                                        ),),
                                                      ],
                                                    ),
                                                  ),
                                                ),

                                                RaisedButton(
                                                  child: Text(
                                                    'Okay'.tr().toString(),
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  onPressed: () =>
                                                  {Navigator.of(context).pop()},
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        });
                      });
                    },
                  ),
                ),
              ],
            ),
            Container( height: height*.18,width: width*.005,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'building_number'.tr().toString() + " : ",
                          children: [
                            TextSpan(
                              text: temp.buildingNumber.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'building_name'.tr().toString() + " : ",
                          children: [
                            TextSpan(
                              text: temp.buildingName.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'Comitted Name'.tr().toString() + ' : ',
                          children: [
                            TextSpan(
                              text: temp.committedName.toString(),
                            )
                          ],
                        ),
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      MediaQuery.of(context).size.height * 0.01,
                      0,
                      MediaQuery.of(context).size.height * 0.02),
                  child: GestureDetector(
                      onTap: () {},
                      child: RichText(
                        text: TextSpan(
                          text: 'Comitted Phone'.tr().toString() + ' : ',
                          children: [
                            TextSpan(
                              text: temp.committedNumber.toString(),
                            )
                          ],
                        ),
                      )),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }


  String currentStage(Test_Book temp){
    String currentStage="";
    int currentindex = int.parse(temp.stageid.substring(0, temp.stageid.indexOf('.'))),
        mainindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('.')+1, temp.stageid.indexOf('_'))),
        subindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('_')+1, temp.stageid.length));

    if(currentindex == 0)
    {
      String sub = new SS1().ss1[mainindex].subs[subindex].name,
              main = new SS1().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 1)
    {
      String sub = new SS2().ss1[mainindex].subs[subindex].name,
          main = new SS2().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 2)
    {
      String sub = new SS3().ss1[mainindex].subs[subindex].name,
          main = new SS3().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 3)
    {
      String sub = new SS4().ss1[mainindex].subs[subindex].name,
          main = new SS4().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 4)
    {
      String sub = new SS5().ss1[mainindex].subs[subindex].name,
          main = new SS5().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 5)
    {
      String sub = new SS6().ss1[mainindex].subs[subindex].name,
          main = new SS6().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    else if(currentindex == 6)
    {
      String sub = new SS7().ss1[mainindex].subs[subindex].name,
          main = new SS7().ss1[mainindex].name;
      currentStage = main+', '+sub;
    }
    
    return currentStage;
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context, Test_Book temp, int currentindex, int mainindex, int subindex) async {
    int currentindex = int.parse(temp.stageid.substring(0, temp.stageid.indexOf('.'))),
        mainindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('.')+1, temp.stageid.indexOf('_'))),
        subindex = int.parse(temp.stageid.substring(temp.stageid.indexOf('_')+1, temp.stageid.length));

    await Navigator.pushNamed(context, "/standard_stage",
        arguments: {"ProgressClassview": temp});
  }


  Future<void> getTests() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_test_book);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);
      print(data);
      tests = data.map((testo) => new Test_Book().fromJson(testo)).toList();


      url = Uri.https(
          consts.domain, consts.domain_inProgress);

      response = await client.get(url);
      data.clear();
      data = jsonDecode(response.body);

      List<ProgressClass> compacting = data.map((progres) => new ProgressClass().fromJson(progres)).toList();

      for(Test_Book a in tests)
        {
          for(ProgressClass temp in compacting) {
            if(temp.fakeId == a.fakekimlik){
              temp.currentIndex =
                  int.parse(a.stageid.substring(0, a.stageid.indexOf(".")));
              myLifts.add(temp);
            }
          }
        }
      temps.addAll(myLifts);
      setState(() {
      });
    } finally {
      client.close();
    }
  }


  Future<Response> deleteRequest (Test_Book temp) async {
    print(temp.toJson());
    Uri url = Uri.https(
        consts.domain, consts.domain_test_book+temp.id.toString());

    await delete(url
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
        {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => super.widget));
        }
    });
  }




}
