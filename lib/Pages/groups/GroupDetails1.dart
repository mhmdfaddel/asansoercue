import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Group.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';


class GroupDetails1 extends StatefulWidget {
  const GroupDetails1({Key key}) : super(key: key);

  @override
  _GroupDetails1State createState() => _GroupDetails1State();
}

class _GroupDetails1State extends State<GroupDetails1> with TickerProviderStateMixin {
   TabController tabController;
  @override
  void initState() {
    super.initState();
    _controller = TabController(length: list.length, vsync: this);

  }
   final String appTitle = 'UMUT ASANSÖR';
   final List<Asansor> duplicateItems = [];
   Constants consts = new Constants();

   List<Asansor> myLifts = [];
   List<Asansor> asansors=[];
   List<Asansor> deleted = [];
   List<Asansor> objectAsansors = [];
   List<Asansor> items = [];


   TextEditingController editingController = TextEditingController();


   List<Widget> list = [
     Tab(text: "groupDetails".tr().toString(),icon: Icon(Icons.info_outline)),
     Tab(text: "lifts".tr().toString(),icon: Icon(Icons.door_sliding)),
     Tab(text: "non added".tr().toString(),icon: Icon(Icons.door_sliding_outlined)),
   ];
  TabController _controller;

   TextEditingController startcontroller = TextEditingController();
   TextEditingController endcontroller = TextEditingController();

   LocaleType localetype;
   DateTime realdate1, realdate2;
   Map<String, Object> rcvdData;
   Group temp = Group();
   bool firsttime = true;
   bool secondtime = false;
   final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
   bool editable = false;
   TextEditingController groupName = TextEditingController();

   @override
  Widget build(BuildContext context) {

    if(firsttime)
    {
      rcvdData = ModalRoute.of(context).settings.arguments;
      if (rcvdData["group"] != null) temp = rcvdData["group"];
      getLifts(temp.lifts);
      startcontroller.text = temp.startdate;
      endcontroller.text = temp.enddate;
      firsttime = false;
      for(Asansor asansor in temp.lifts)
        {
          asansor.taken = true;
          asansor.icon = new Icon(Icons.delete,
            size: 30.0,
            color: Colors.white,);
        }
    }


    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          appTitle,
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
        centerTitle: true,

        bottom: TabBar(
          onTap: (index) {
            // Should not used it as it only called when tab options are clicked,
            // not when user swapped
          },
          controller: _controller,
          tabs: list,
        ),
      ),
    body: Container(
      child: TabBarView(
        controller: _controller,
        children: [
          Column(
            children:[
              Padding(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.0,
                    MediaQuery.of(context).size.height * 0.02,
                    MediaQuery.of(context).size.width * 0.75,
                    0),
                child: Text(
                  'groupName'.tr().toString(),
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.05,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.grey, // set border color
                      width: 0.5,
                      style: BorderStyle.solid), // set border width
                  // set rounded corner radius
                ),
                child: TextField(
                  controller: groupName,
                  decoration: InputDecoration(
                    hintText: temp.name,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    border: InputBorder.none,
                    icon: Icon(Icons.description),
                  ),
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.0,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.67,
                        0),
                    child: Text(
                      'Start Date'.tr().toString(),
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),
                  ),
                  Row(

                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.date_range),
                        color: Colors.grey,
                        iconSize: 20,
                        onPressed: () {
                          setState(() {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                  setState(() {
                                    startcontroller = new TextEditingController(
                                        text: date.year.toString() +
                                            "-" +
                                            date.month.toString() +
                                            "-" +
                                            date.day.toString());
                                    realdate1 = date;
                                  });
                                  print('change $date');
                                }, onConfirm: (date) {
                                  setState(() {
                                    startcontroller = new TextEditingController(
                                        text: date.year.toString() +
                                            "-" +
                                            date.month.toString() +
                                            "-" +
                                            date.day.toString());
                                    realdate1 = date;
                                  });
                                  print('confirm $date');
                                },
                                onCancel: () {},
                                currentTime: DateTime.now(),
                                locale: _changeLocal(context));
                          });
                        },
                      ),
                      new Expanded(
                        child: TextField(
                          controller: startcontroller,
                          enabled: false,
                          keyboardType: TextInputType.datetime,
                          decoration: InputDecoration(

                            hintText: "Pick Date".tr().toString(),
                            hintStyle: TextStyle(color: Colors.grey),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                            isDense: true,
                          ),
                          onTap: () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                  print('change $date');
                                }, onConfirm: (date) {
                                  print('confirm $date');
                                }, currentTime: DateTime.now(), locale: _changeLocal(context));
                          },
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.0,
                        MediaQuery.of(context).size.height * 0.02,
                        MediaQuery.of(context).size.width * 0.75,
                        0),
                    child: Text(
                      'End Date'.tr().toString(),
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),
                  ),
                  Row(

                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.date_range),
                        color: Colors.grey,
                        iconSize: 20,
                        onPressed: () {
                          setState(() {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                  setState(() {
                                    endcontroller = new TextEditingController(
                                        text: date.year.toString() +
                                            "-" +
                                            date.month.toString() +
                                            "-" +
                                            date.day.toString());
                                    realdate2 = date;
                                  });
                                  print('change $date');
                                }, onConfirm: (date) {
                                  setState(() {
                                    endcontroller = new TextEditingController(
                                        text: date.year.toString() +
                                            "-" +
                                            date.month.toString() +
                                            "-" +
                                            date.day.toString());
                                    realdate2 = date;
                                  });
                                  print('confirm $date');
                                },
                                onCancel: () {},
                                currentTime: DateTime.now(),
                                locale: _changeLocal(context));
                          });
                        },
                      ),
                      new Expanded(
                        child: TextField(
                          controller: endcontroller,
                          enabled: false,
                          keyboardType: TextInputType.datetime,
                          decoration: InputDecoration(

                            hintText: "Pick Date".tr().toString(),
                            hintStyle: TextStyle(color: Colors.grey),
                            contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                            isDense: true,
                          ),
                          onTap: () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                  print('change $date');
                                }, onConfirm: (date) {
                                  print('confirm $date');
                                }, currentTime: DateTime.now(), locale: _changeLocal(context));
                          },
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.393),
                      child: copyRights()),
                ],
              ),

            ] ,
          ),
          Container(
            height: MediaQuery.of(context).size.height*0.611,
            width: MediaQuery.of(context).size.width*1,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: temp.lifts.length,
                    itemBuilder: (context, index) {
                      return _buildRow(temp.lifts,temp.lifts[index]);
                    },
                  ),
                ),
                copyRights(),

              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height*0.611,
            width: MediaQuery.of(context).size.width*1,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: myLifts.length,
                    itemBuilder: (context, index) {
                      return _buildRow(temp.lifts, myLifts[index]);
                    },
                  ),
                ),
                copyRights(),

              ],
            ),
          ),
        ],
      ),
    ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {

            if(groupName.text != "") temp.name = groupName.text;
            postGroupRequest(temp);
            postRequest(myLifts);
          });
        },
        child: Icon(Icons.save),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
   LocaleType _changeLocal(BuildContext context)
   {
     if(EasyLocalization.of(context).locale == Locale('ar', 'SY'))
     {
       localetype = LocaleType.ar;
     }
     else if(EasyLocalization.of(context).locale == Locale('en', 'US'))
     {
       localetype = LocaleType.en;
     }
     else if(EasyLocalization.of(context).locale == Locale('tr', 'TR'))
     {
       localetype = LocaleType.tr;
     }
     else
     {
       localetype = LocaleType.de;
     }
     return localetype;
   }

   Future<void> getLifts(List<Asansor> lifts) async {
     var client = new Client();
     try{
       Uri url = Uri.https(
           consts.domain, consts.domain_lifts);
       Response response = await client.get(url);
       List data = jsonDecode(response.body);
       List<Asansor> asansorData = data.map((asansor) => new Asansor().fromJson(asansor)).toList();
       for (Asansor temp in asansorData) {
         if(temp.taken)
           {
             temp.icon = new Icon(Icons.delete, size: 30.0, color: Colors.white,);
           }
         else
           {
             temp.icon = new Icon(Icons.add, size: 30.0, color: Colors.white,);
           }
          if(!temp.taken)
          {
            print(temp.kimlikNo);
            myLifts.add(temp);
          }
       }

       items.addAll(myLifts);

       duplicateItems.addAll(myLifts);
       setState(() {});
     } finally {
        client.close();
     }
   }

   Widget _buildRow(List<Asansor> listo,Asansor temp) {
     return Row(
       children: [
         Container(
           width: MediaQuery.of(context).size.width * 0.7,
           child: Card(
             shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.circular(15),
             ),
             shadowColor: Colors.orange[400],
             margin: const EdgeInsets.all(10),
             elevation: 15.0,
             child: Row(
               children: [
                 Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: [
                     Container(
                       padding: EdgeInsets.fromLTRB(
                           MediaQuery.of(context).size.width * 0.01,
                           0,
                           MediaQuery.of(context).size.width * 0.02,
                           MediaQuery.of(context).size.height * 0.02),
                       child: GestureDetector(
                         onTap: () {},
                         child: Text(
                           'id_number'.tr().toString() + ' : ' + temp.kimlikNo.toString(),
                           style: TextStyle(),
                         ),
                       ),
                     ),
                     Padding(
                         padding: EdgeInsets.fromLTRB(
                             MediaQuery.of(context).size.width * 0.01,
                             0,
                             MediaQuery.of(context).size.width * 0.01,
                             MediaQuery.of(context).size.height * 0.02),
                         child: GestureDetector(
                           onTap: () {},
                           child: Text(
                             'building_number'.tr().toString() +
                                 ' : ' +
                                 temp.buildingNo.toString(),
                             style: TextStyle(
                               color: Colors.grey,
                             ),
                           ),
                         )),
                     Padding(
                         padding: EdgeInsets.fromLTRB(
                             MediaQuery.of(context).size.width * 0.01,
                             0,
                             MediaQuery.of(context).size.width * 0.0,
                             MediaQuery.of(context).size.height * 0.02),
                         child: GestureDetector(
                           onTap: () {},
                           child: Text('constructor_name'.tr().toString() +
                               ' : ' +
                               temp.installerName.toString()),
                         )),
                   ],
                 ),
               ],
             ),
           ),
         ),
         RawMaterialButton(
           onPressed: () {
             setState(() {
               if(!temp.taken)
               {
               temp.taken = true;
                 temp.icon = new Icon(Icons.delete,
                   size: 30.0,
                   color: Colors.white,);
                 for(Asansor a in myLifts)
                   if(a.id == temp.id) {
                     myLifts.remove(a);
                     break;
                   }
                 listo.add(temp);
               }
               else
               {
                 for(Asansor a in listo)
                   if(a.id == temp.id) {
                     listo.remove(a);
                     a.taken = false;
                     myLifts.add(a);
                     break;
                   }
                 temp.taken = false;
                 temp.icon = new Icon(Icons.add,
                   size: 30.0,
                   color: Colors.white,);
               }
             });
           },
           elevation: 2.0,
           fillColor: Colors.amber[700],
           child: temp.icon,
           padding: EdgeInsets.all(15.0),
           shape: CircleBorder(),
         ),
       ],
     );
   }

   Future<Response> postRequest (List<Asansor> lifts) async {
     Uri url = Uri.https(
         consts.domain, consts.domain_lifts+"/updateblock");

     List data = lifts.map((lift) => lift.toJson()).toList();

     var body = data;
     var encodedbody = json.encode(body);
     Map<String,String> headers = {'Content-Type':'application/json'};

     await put(url,
         headers: headers,
         body: encodedbody
     ).then((Response response) {
       print("Response status: ${response.statusCode}");
       print("Response body: ${response.contentLength}");
       print(response.headers);
       print(response.request);

     });
   }


   Future<Response> postGroupRequest(Group group) async {
     Uri url = Uri.https(
         consts.domain, consts.domain_groups+"update/"+group.id);
     temp.startdate = startcontroller.value.text;
     temp.enddate = endcontroller.value.text;

     temp.jsonlifts =  temp.lifts.map((lift) => lift.toJson()).toList();

     var body = temp.toJson();
     var encodedbody = json.encode(body);
     Map<String,String> headers = {'Content-Type':'application/json'};

     await post(url,
         headers: headers,
         body: encodedbody
     ).then((Response response) {
       print("Response status: ${response.statusCode}");
       print("Response body: ${response.contentLength}");
       print(response.headers);
       print(response.request);
       if(response.statusCode == 200)
       {
         Fluttertoast.showToast(
             msg: "Success".tr().toString(),
             toastLength: Toast.LENGTH_SHORT,
             gravity: ToastGravity.CENTER,
             backgroundColor: Colors.red,
             textColor: Colors.white,
             fontSize: 16.0
         );

       }
       else
       {
         Fluttertoast.showToast(
             msg: "Failed".tr().toString(),
             toastLength: Toast.LENGTH_SHORT,
             gravity: ToastGravity.CENTER,
             backgroundColor: Colors.red,
             textColor: Colors.white,
             fontSize: 16.0
         );
       }
       Navigator.pop(context);
     });
   }
}