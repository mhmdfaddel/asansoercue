import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Group.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:material_floating_search_bar/material_floating_search_bar.dart';


class CreateGroup extends StatefulWidget {
  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {

  TextEditingController groupName = TextEditingController();
  Constants consts = new Constants();
  int value = 0;
  String Country = " ";
  List<Asansor> myLifts = [];
  List<Asansor> asansors=[];

  TextEditingController editingController = TextEditingController();
  TextEditingController startcontroller = TextEditingController();
  TextEditingController endcontroller = TextEditingController();

  LocaleType localetype;
  DateTime realdate1, realdate2;
  final duplicateItems = List<Asansor>();
  var items = List<Asansor>();


  @override
  void initState() {
    // TODO: implement initState
    getLifts();
    items.addAll(duplicateItems);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: "",
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8156,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'createGroup'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),

                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'groupName'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.05,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: groupName,
                          decoration: InputDecoration(
                            hintText: 'groupName'.tr().toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.description),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'Start Date'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Row(

                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.date_range),
                            color: Colors.grey,
                            iconSize: 20,
                            onPressed: () {
                              setState(() {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(2000, 1, 1),
                                    maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                      setState(() {
                                        startcontroller = new TextEditingController(
                                            text: date.year.toString() +
                                                "-" +
                                                date.month.toString() +
                                                "-" +
                                                date.day.toString());
                                        realdate1 = date;
                                      });
                                      print('change $date');
                                    }, onConfirm: (date) {
                                      setState(() {
                                        startcontroller = new TextEditingController(
                                            text: date.year.toString() +
                                                "-" +
                                                date.month.toString() +
                                                "-" +
                                                date.day.toString());
                                        realdate1 = date;
                                      });
                                      print('confirm $date');
                                    },
                                    onCancel: () {},
                                    currentTime: DateTime.now(),
                                    locale: _changeLocal(context));
                              });
                            },
                          ),
                          new Expanded(
                            child: TextField(
                              controller: startcontroller,
                              enabled: false,
                              keyboardType: TextInputType.datetime,
                              decoration: InputDecoration(

                                hintText: "Pick Date".tr().toString(),
                                hintStyle: TextStyle(color: Colors.grey),
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                isDense: true,
                              ),
                              onTap: () {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(2000, 1, 1),
                                    maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                      print('change $date');
                                    }, onConfirm: (date) {
                                      print('confirm $date');
                                    }, currentTime: DateTime.now(), locale: _changeLocal(context));
                              },
                              style: TextStyle(
                                fontSize: 14.0,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'End Date'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Row(

                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.date_range),
                            color: Colors.grey,
                            iconSize: 20,
                            onPressed: () {
                              setState(() {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(2000, 1, 1),
                                    maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                      setState(() {
                                        endcontroller = new TextEditingController(
                                            text: date.year.toString() +
                                                "-" +
                                                date.month.toString() +
                                                "-" +
                                                date.day.toString());
                                        realdate2 = date;
                                      });
                                      print('change $date');
                                    }, onConfirm: (date) {
                                      setState(() {
                                        endcontroller = new TextEditingController(
                                            text: date.year.toString() +
                                                "-" +
                                                date.month.toString() +
                                                "-" +
                                                date.day.toString());
                                        realdate2 = date;
                                      });
                                      print('confirm $date');
                                    },
                                    onCancel: () {},
                                    currentTime: DateTime.now(),
                                    locale: _changeLocal(context));
                              });
                            },
                          ),
                          new Expanded(
                            child: TextField(
                              controller: endcontroller,
                              enabled: false,
                              keyboardType: TextInputType.datetime,
                              decoration: InputDecoration(

                                hintText: "Pick Date".tr().toString(),
                                hintStyle: TextStyle(color: Colors.grey),
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                isDense: true,
                              ),
                              onTap: () {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(2000, 1, 1),
                                    maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                      print('change $date');
                                    }, onConfirm: (date) {
                                      print('confirm $date');
                                    }, currentTime: DateTime.now(), locale: _changeLocal(context));
                              },
                              style: TextStyle(
                                fontSize: 14.0,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                    Container(
                      height: MediaQuery.of(context).size.height*0.641,
                      width: MediaQuery.of(context).size.width*1,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                              onChanged: (value) {
                                filterSearchResults(value);
                              },
                              controller: editingController,
                              decoration: InputDecoration(
                                  labelText: "search_elevator".tr().toString(),
                                  hintText: "search_elevator".tr().toString(),
                                  prefixIcon: Icon(Icons.search),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: items.length,
                              itemBuilder: (context, index) {
                                return _buildRow(items[index]);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {
            postGroupRequest(groupName.value.text);
          });
        },
        child: Icon(Icons.save),
      ),
    );
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result

    Navigator.popAndPushNamed(context, "/maintenOps");
    // after the SecondScreen result comes back update the Text widget with it
  }

  void filterSearchResults(String query) {
    List<Asansor> dummySearchList = List<Asansor>();
    dummySearchList.addAll(duplicateItems);
    if(query.isNotEmpty) {
      List<Asansor> dummyListData = List<Asansor>();
      dummySearchList.forEach((item) {
        if(item.kimlikNo.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  Future<void> getLifts() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);
      http.Response response = await client.get(url);
      List data = jsonDecode(response.body);
      myLifts = data
          .map((data) => new Asansor().fromJson(data))
          .toList();

      List<Asansor> toRemove = [];
      var it = myLifts.iterator;
      while (it.moveNext()) {
        if(it.current.taken)
          {
            toRemove.add(it.current);
          }
      }

      myLifts.removeWhere((element) => toRemove.contains(element));
      items.addAll(myLifts);
      duplicateItems.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }
  }
  Widget _buildRow(Asansor temp) {
    return Row(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.7,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            shadowColor: Colors.orange[400],
            margin: const EdgeInsets.all(10),
            elevation: 15.0,
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.01,
                          0,
                          MediaQuery.of(context).size.width * 0.02,
                          MediaQuery.of(context).size.height * 0.02),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'id_number'.tr().toString() + ' : ' + temp.kimlikNo.toString(),
                          style: TextStyle(),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.01,
                            0,
                            MediaQuery.of(context).size.width * 0.01,
                            MediaQuery.of(context).size.height * 0.02),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text(
                            'building_number'.tr().toString() +
                                ' : ' +
                                temp.buildingNo.toString(),
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        )),
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.01,
                            0,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text('constructor_name'.tr().toString() +
                              ' : ' +
                              temp.installerName.toString()),
                        )),
                  ],
                ),
              ],
            ),
          ),
        ),
        RawMaterialButton(
          onPressed: () {
            setState(() {
              if(temp.color == Colors.white)
              {
                print("Added");
                asansors.add(temp);
                temp.color = Colors.green[300];
                temp.icon = new Icon(Icons.delete,
                  size: 30.0,
                  color: Colors.redAccent,);
              }
              else
                {
                  temp.color = Colors.white;
                  asansors.remove(temp);
                  temp.icon = new Icon(Icons.add,
                    size: 30.0,
                    color: Colors.white,);
                }
            });
          },
          elevation: 2.0,
          fillColor: Colors.amber[700],
          child: temp.icon,
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
      ],
    );
  }

  Future<http.Response> postRequest (String groupid, Asansor temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_groups+groupid+'/newlift');

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.put(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

  Future<http.Response> putRequest (Asansor temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_lifts+temp.id+"/taking");


    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.put(url,
        headers: headers,
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
    });
  }

  LocaleType _changeLocal(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY'))
    {
      localetype = LocaleType.ar;
    }
    else if(EasyLocalization.of(context).locale == Locale('en', 'US'))
    {
      localetype = LocaleType.en;
    }
    else if(EasyLocalization.of(context).locale == Locale('tr', 'TR'))
    {
      localetype = LocaleType.tr;
    }
    else
    {
      localetype = LocaleType.de;
    }
    return localetype;
  }


  Future<http.Response> postGroupRequest(String name) async {
    Group temp = new Group(name: name);
    temp.enddate = endcontroller.value.text;
    temp.startdate = startcontroller.value.text;
    temp.lifts = new List<Asansor>();
    for(Asansor asansor in asansors)
      {
        asansor.taken = true;
        temp.lifts.add(asansor);
      }
    temp.jsonlifts =  temp.lifts.map((lift) => lift.toJson()).toList();
    temp.done = false;

    Uri url = Uri.https(
        consts.domain, consts.domain_groups);

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.post(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
        {

          Fluttertoast.showToast(
              msg: "Group added".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );

        }
      else
        {
          Fluttertoast.showToast(
              msg: "Failed".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      Navigator.popAndPushNamed(context, "/");
    });
  }

}
