import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Group.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';

class GroupDetails extends StatefulWidget {
  Maintainance myMaintenance;

  GroupDetails({Key key, this.myMaintenance}) : super(key: key);

  @override
  _GroupDetailsState createState() => _GroupDetailsState();
}

class _GroupDetailsState extends State<GroupDetails> {
  var value = 0;
  String Country = " ";
  bool firsttime = true;
  bool secondtime = false;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  bool editable = false;
  TextEditingController groupName = TextEditingController();
  Constants consts = new Constants();

  List<Asansor> myLifts = [];
  List<Asansor> asansors=[];
  List<Asansor> objectAsansors = [];


  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<Asansor>();
  var items = List<Asansor>();
  Map<String, Object> rcvdData;
  Group temp = Group();

  TextEditingController startcontroller = TextEditingController();
  TextEditingController endcontroller = TextEditingController();

  LocaleType localetype;
  DateTime realdate1, realdate2;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if(firsttime)
    {
      rcvdData = ModalRoute.of(context).settings.arguments;
      if (rcvdData["group"] != null) temp = rcvdData["group"];
       getLifts();
       startcontroller.text = temp.startdate;
       endcontroller.text = temp.enddate;
      items.addAll(duplicateItems);
      firsttime = false;
    }

    String appTitle = 'ASANSÖRCÜ';
    return Scaffold(
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8155,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'groupDetails'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),

                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'groupName'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.05,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: groupName,
                          decoration: InputDecoration(
                            hintText: temp.name,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.description),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.75,
                                0),
                            child: Text(
                              'Start Date'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Row(

                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.date_range),
                                color: Colors.grey,
                                iconSize: 20,
                                onPressed: () {
                                  setState(() {
                                    DatePicker.showDatePicker(context,
                                        showTitleActions: true,
                                        minTime: DateTime(2000, 1, 1),
                                        maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                          setState(() {
                                            startcontroller = new TextEditingController(
                                                text: date.year.toString() +
                                                    "-" +
                                                    date.month.toString() +
                                                    "-" +
                                                    date.day.toString());
                                            realdate1 = date;
                                          });
                                          print('change $date');
                                        }, onConfirm: (date) {
                                          setState(() {
                                            startcontroller = new TextEditingController(
                                                text: date.year.toString() +
                                                    "-" +
                                                    date.month.toString() +
                                                    "-" +
                                                    date.day.toString());
                                            realdate1 = date;
                                          });
                                          print('confirm $date');
                                        },
                                        onCancel: () {},
                                        currentTime: DateTime.now(),
                                        locale: _changeLocal(context));
                                  });
                                },
                              ),
                              new Expanded(
                                child: TextField(
                                  controller: startcontroller,
                                  enabled: false,
                                  keyboardType: TextInputType.datetime,
                                  decoration: InputDecoration(

                                    hintText: "Pick Date".tr().toString(),
                                    hintStyle: TextStyle(color: Colors.grey),
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                    isDense: true,
                                  ),
                                  onTap: () {
                                    DatePicker.showDatePicker(context,
                                        showTitleActions: true,
                                        minTime: DateTime(2000, 1, 1),
                                        maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                          print('change $date');
                                        }, onConfirm: (date) {
                                          print('confirm $date');
                                        }, currentTime: DateTime.now(), locale: _changeLocal(context));
                                  },
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.0,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.75,
                                0),
                            child: Text(
                              'End Date'.tr().toString(),
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                          ),
                          Row(

                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.date_range),
                                color: Colors.grey,
                                iconSize: 20,
                                onPressed: () {
                                  setState(() {
                                    DatePicker.showDatePicker(context,
                                        showTitleActions: true,
                                        minTime: DateTime(2000, 1, 1),
                                        maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                          setState(() {
                                            endcontroller = new TextEditingController(
                                                text: date.year.toString() +
                                                    "-" +
                                                    date.month.toString() +
                                                    "-" +
                                                    date.day.toString());
                                            realdate2 = date;
                                          });
                                          print('change $date');
                                        }, onConfirm: (date) {
                                          setState(() {
                                            endcontroller = new TextEditingController(
                                                text: date.year.toString() +
                                                    "-" +
                                                    date.month.toString() +
                                                    "-" +
                                                    date.day.toString());
                                            realdate2 = date;
                                          });
                                          print('confirm $date');
                                        },
                                        onCancel: () {},
                                        currentTime: DateTime.now(),
                                        locale: _changeLocal(context));
                                  });
                                },
                              ),
                              new Expanded(
                                child: TextField(
                                  controller: endcontroller,
                                  enabled: false,
                                  keyboardType: TextInputType.datetime,
                                  decoration: InputDecoration(

                                    hintText: "Pick Date".tr().toString(),
                                    hintStyle: TextStyle(color: Colors.grey),
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                    isDense: true,
                                  ),
                                  onTap: () {
                                    DatePicker.showDatePicker(context,
                                        showTitleActions: true,
                                        minTime: DateTime(2000, 1, 1),
                                        maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                          print('change $date');
                                        }, onConfirm: (date) {
                                          print('confirm $date');
                                        }, currentTime: DateTime.now(), locale: _changeLocal(context));
                                  },
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height*0.611,
                        width: MediaQuery.of(context).size.width*1,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                onChanged: (value) {
                                  filterSearchResults(value);
                                },
                                controller: editingController,
                                decoration: InputDecoration(
                                    labelText: "search_elevator".tr().toString(),
                                    hintText: "search_elevator".tr().toString(),
                                    prefixIcon: Icon(Icons.search),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                              ),
                            ),
                            Expanded(
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: items.length,
                                itemBuilder: (context, index) {
                                  return _buildRow(items[index]);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),

                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {
            postGroupRequest(groupName.value.text);
          });
        },
        child: Icon(Icons.save),
      ),//drawer: MyDrawer(),
    );
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context, String op, Asansor temp) async {
    // start the SecondScreen and wait for it to finish with a result

    if (op == 'delete')
      Navigator.pushNamed(context, "/",
          arguments: {"asansor": temp, "ops": "delete"});
    else if (op == 'update') {
      Navigator.pushNamed(context, "/",
          arguments: {"asansor": temp, "ops": "update"});
    } else {
      //NAVIGATE TO MAINTENANCES
      Navigator.pushNamed(context, "/maintenOps",
          arguments: {"Mainten": temp.operations});
    }
    // after the SecondScreen result comes back update the Text widget with it
  }

  Future<Response> deleteRequest (Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+temp.id);

    await delete(url
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

  void filterSearchResults(String query) {
    List<Asansor> dummySearchList = List<Asansor>();
    dummySearchList.addAll(duplicateItems);
    if(query.isNotEmpty) {
      List<Asansor> dummyListData = List<Asansor>();
      dummySearchList.forEach((item) {
        if(item.kimlikNo.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  Future<void> getLifts() async {
    var client = new Client();
    try{
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);
      for (int i = 0; i < data.length; i++) {

        //List roles = jsonDecode(data[0]);

        Asansor temp = new Asansor();
        var codeUnits = data[i]["buildingname"].toString().codeUnits;
        temp.buildingname = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["buildingNo"].toString().codeUnits;
        temp.buildingNo = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["installerName"].toString().codeUnits;
        temp.installerName = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["installerPhone"].toString().codeUnits;
        temp.installerPhone = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["kimlikNo"].toString().codeUnits;
        temp.kimlikNo = Utf8Decoder().convert(codeUnits);
        temp.location = new Position(latitude: data[i]["latitude"] , longitude: data[i]["longitude"]);
        codeUnits = data[i]["phoneNo"].toString().codeUnits;
        temp.phoneNo = Utf8Decoder().convert(codeUnits);
        codeUnits = data[i]["status"].toString().codeUnits;
        temp.status = Utf8Decoder().convert(codeUnits);
        temp.id = data[i]["lift_id"].toString();

              temp.color = Colors.white;
              temp.icon = new Icon(Icons.add,
                size: 30.0,
                color: Colors.white,);

        myLifts.add(temp);
      }

      items.addAll(myLifts);

      duplicateItems.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }
  }

  LocaleType _changeLocal(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY'))
    {
      localetype = LocaleType.ar;
    }
    else if(EasyLocalization.of(context).locale == Locale('en', 'US'))
    {
      localetype = LocaleType.en;
    }
    else if(EasyLocalization.of(context).locale == Locale('tr', 'TR'))
    {
      localetype = LocaleType.tr;
    }
    else
    {
      localetype = LocaleType.de;
    }
    return localetype;
  }

  Widget _buildRow(Asansor temp) {
    return Row(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.7,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            shadowColor: Colors.orange[400],
            margin: const EdgeInsets.all(10),
            elevation: 15.0,
            color: temp.color,
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.01,
                          0,
                          MediaQuery.of(context).size.width * 0.02,
                          MediaQuery.of(context).size.height * 0.02),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'id_number'.tr().toString() + ' : ' + temp.kimlikNo.toString(),
                          style: TextStyle(),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.01,
                            0,
                            MediaQuery.of(context).size.width * 0.01,
                            MediaQuery.of(context).size.height * 0.02),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text(
                            'building_number'.tr().toString() +
                                ' : ' +
                                temp.buildingNo.toString(),
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        )),
                    Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.01,
                            0,
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text('constructor_name'.tr().toString() +
                              ' : ' +
                              temp.installerName.toString()),
                        )),
                  ],
                ),
              ],
            ),
          ),
        ),
        RawMaterialButton(
          onPressed: () {
            setState(() {
              if(temp.color == Colors.white)
              {
                asansors.add(temp);
                temp.color = Colors.green[300];
                temp.icon = new Icon(Icons.delete,
                  size: 30.0,
                  color: Colors.white,);
              }
              else
              {
                temp.color = Colors.white;
                asansors.remove(temp);
                temp.icon = new Icon(Icons.add,
                  size: 30.0,
                  color: Colors.white,);
              }
            });
          },
          elevation: 2.0,
          fillColor: Colors.amber[700],
          child: temp.icon,
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
      ],
    );
  }

  Future<Response> postRequest (String groupid, Asansor temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_groups+groupid+'/newlift');

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await post(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
    });
  }


  Future<Response> postGroupRequest(String name) async {
    Group temp = new Group(name: name);
    Uri url = Uri.https(
        consts.domain, consts.domain_groups);
    temp.startdate = startcontroller.value.text;
    temp.enddate = endcontroller.value.text;

    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        for(int i = 0; i < asansors.length;i++)
          postRequest(name, asansors[i]);
      }
    });
  }


}