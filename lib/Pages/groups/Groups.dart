import 'dart:convert';
import 'dart:core';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:asansorci/Models/Group.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';


class Groups extends StatefulWidget {
  @override
  _GroupsState createState() => _GroupsState();
}

class _GroupsState extends State<Groups> {
  List<Group> myGroups = [];
  var height=0.0, width = 0.0;

  Constants consts = new Constants();
  int value = 0;

  TextEditingController passwordController = new TextEditingController();

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  TextEditingController search = new TextEditingController();



  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
          myGroups.clear();
          getGroups();
      });
    _refreshController.loadComplete();
  }

  List<Group> temps = [];

  List<Group> existGroup(String temp, String cirtic) {
    List<Group> testo = new List<Group>();

      for (int i = 0; i < myGroups.length; i++) {
        if (myGroups[i].name.contains(temp)) {
          testo.add(myGroups[i]);
        }
      }
    return testo;
  }



  Future<void> getGroups() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_groups);
      http.Response response = await client.get(url);
      List data = jsonDecode(response.body);

      myGroups = data
          .map((data) => new Group().fromJson(data))
          .toList();

      temps.addAll(myGroups);
      setState(() {});
    } finally {
      client.close();
    }
  }


  Future<void> deleteGroups(Group temp) async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_groups+temp.id.toString());
      http.Response response = await client.delete(url);

      setState(() {});
    } finally {
      client.close();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getGroups();
    super.initState();
  }

  void _addItem() {
    Navigator.pushNamed(context, '/createGroup');
  }

  @override
  Widget build(BuildContext context) {

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: '',
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Groups'.tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 21.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: height * 0.062,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid),
                borderRadius:
                BorderRadius.all(Radius.circular(50)), // set border width
                // set rounded corner radius
              ),
              child: TextField(
                controller: search,
                  onChanged: (value) =>{
                    setState(() {
                      if(this.search.value.text == "")
                      {
                        myGroups = temps;
                      }
                      else
                        myGroups = existGroup(search.value.text, "");
                    })
                  },
                decoration: InputDecoration(
                  hintText: 'search_group'.tr().toString(),
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.02,
                  MediaQuery.of(context).size.width * 0.02,
                  0,
                  MediaQuery.of(context).size.width * 0.02),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                    child: Text(
                      'total'.tr().toString() +
                          ' : ' +
                          this.myGroups.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                  ? MediaQuery.of(context).size.height * 0.588
                  : MediaQuery.of(context).size.height * 0.6134,
              width: MediaQuery.of(context).size.width * 1,
              child:  Container(
                height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                    ? MediaQuery.of(context).size.height * 0.565
                    : MediaQuery.of(context).size.height * 0.585,
                width: MediaQuery.of(context).size.width * 0.95,
                child: Row(
                  children: [
                    Container(
                      child: Expanded(
                        child: ListView.builder(
                          itemCount: this.myGroups.length,
                          itemBuilder: (context, index) =>
                              this._buildNewRow(this.myGroups[index]),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ),
            copyRights(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {
            _addItem();
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }

  _buildNewRow(Group temp)
  {
    return GestureDetector(
      onHorizontalDragStart: (DragStartDetails details){
        setState(() {
          temp.color = Colors.redAccent;
        });
      },
      onHorizontalDragEnd: (DragEndDetails details){
        if (details.primaryVelocity > 0) {

          setState(() {
            _showDialog(temp);

          });
        }
        else
        {
          setState(() {
          });
        }
      },
      child: Card(
        child: Container(
          height: height*0.15,
          width: width,
          child: Row(
            children: [
              Container(
                width: width*0.4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: height*0.01, horizontal: width*0.05),
                      child: Text(
                          temp.name,
                        overflow: TextOverflow.fade,
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ),
                    Divider(height: 1, thickness: 1, indent: 15, endIndent: 15, color:Colors.black),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: height*0.01, horizontal: width*0.05),
                      child: Text(
                        temp.startdate,
                        style: TextStyle(
                          color: Colors.green,
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: height*0.001, horizontal: width*0.05),
                      child: Text(
                        temp.enddate,
                        style: TextStyle(
                          color: Colors.redAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              VerticalDivider(
                color: Colors.black54,
                thickness: 1,
                width: 1,
                indent: 10,
                endIndent: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: height*0.05, horizontal: width*0.03),
                    child: Text(
                      temp.doneLifts.toString()+"/"+temp.lifts.length.toString(),
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    ),
                  ),
                ],
              ),
              VerticalDivider(
                color: Colors.black54,
                thickness: 1,
                width: 1,
                indent: 10,
                endIndent: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    padding: EdgeInsets.symmetric(horizontal: width*0.05, vertical: height*0.005),
                    icon: Icon(
                      Icons.info,
                      color: Colors.blueGrey,
                      size: 35.0,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/groupDetails', arguments: {'group': temp});
                    },
                  ),
                  IconButton(
                    padding: EdgeInsets.symmetric(horizontal: width*0.05, vertical: height*0.005),
                    icon: Icon(
                      Icons.handyman,
                      color: Colors.blue,
                      size: 35.0,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/groupLiftMaintain', arguments: {'group': temp});
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        elevation: 8,
        shadowColor: Colors.orangeAccent,
        margin: EdgeInsets.symmetric(vertical: height*0.01, horizontal: width*0.1),
      ),
    );
  }

  // 
  double _getHeight()
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return height*.588;
    }

    return height*.60;
  }


  _showDialog(Group temp) async {
    await showDialog<String>(
      context: context,

      builder: (context) => AlertDialog(
      contentPadding: const EdgeInsets.all(16.0),
      //insetPadding: ,
      content: new Row(
        children: <Widget>[
          new Expanded(
            child: new TextField(
              autofocus: true,
              controller: passwordController,
              decoration: new InputDecoration(
                  labelText: 'Password', hintText: 'type your password'),
            ),
          )
        ],
      ),
      actions: <Widget>[
        new FlatButton(
            child:  Text('Cancel'.tr().toString()),
            onPressed: () {
              Navigator.pop(context);
            }),
        new FlatButton(
            child:  Text('Ok'),
            onPressed: () {
              if(passwordController.text == (new Constants()).password)
              {
                Fluttertoast.showToast(
                    msg: "Deleted".tr().toString(),
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
                deleteGroups(temp);
                myGroups.remove(temp);
                Navigator.pop(context);
              }
              else
                {
                  Fluttertoast.showToast(
                      msg: "Wrong Password".tr().toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                }
            })
      ],
      ),
    );
  }

  Future<http.Response> updateDoneRequest (Group temp) async {

    print(temp.id);

    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_groups + temp.id + "/done");
       await client.get(url);
      }
    finally{
      client.close();
    }
    }

  Future<http.Response> putRequest (Maintainance temp) async {

    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+temp.id);

    var body = temp.toJsonEdit();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.put(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        if(temp.liftType == "Well")
        {
          Maintainance m1 = new Maintainance();
          m1.liftNumber = temp.liftNumber;
          m1.description = '';
          m1.liftType = 'Engine';
          m1.dateTime = manipulateDate(temp.dateTime, 1);
          m1.done = '0';
          m1.employeeName = temp.employeeName;
          m1.location = temp.location;
          post3Maintenance(m1);
          Maintainance m2 = new Maintainance();
          m2.liftNumber = temp.liftNumber;
          m2.liftType = 'Cabine';
          m2.description = '';
          m2.dateTime = manipulateDate(temp.dateTime, 2);
          m2.done = '0';
          m2.employeeName = temp.employeeName;
          m2.location = temp.location;
          post3Maintenance(m2);
          Maintainance m3 = new Maintainance();
          m3.liftNumber = temp.liftNumber;
          m3.liftType = 'Well';
          m3.description = '';
          m3.dateTime = manipulateDate(temp.dateTime, 3);
          m3.done = '0';
          m3.employeeName = temp.employeeName;
          m3.location = temp.location;
          post3Maintenance(m3);
        }
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
    }).timeout(Duration(seconds: 5));
  }

  String manipulateDate(String date, int month)
  {
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(date);
    return manipulateDate1(tempDate, month);
  }

  String manipulateDate1(DateTime date, int month)
  {
    if(date.month+1 > 12)
    {
      if(date.month+month == 13)
        return (date.year+1).toString() +
            "-" +
            (1).toString() +
            "-" +
            date.day.toString();
      if(date.month+month == 14)
        return (date.year+1).toString() +
            "-" +
            (02).toString() +
            "-" +
            date.day.toString();
    }
    return (date.year).toString() +
        "-" +
        (date.month+month).toString() +
        "-" +
        date.day.toString();
  }

  Future<void> post3Maintenance(Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances);
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await http.post(url,
        headers: headers,
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else {
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      
    }).timeout(Duration(seconds: 5));

  }

}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}