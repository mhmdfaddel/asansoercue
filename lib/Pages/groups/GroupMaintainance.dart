import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Group.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';

class GroupMaintainance extends StatefulWidget {
  const GroupMaintainance({Key key}) : super(key: key);

  @override
  _GroupMaintainanceState createState() => _GroupMaintainanceState();
}

class _GroupMaintainanceState extends State<GroupMaintainance> {

  bool firsttime = true;
  bool secondtime = false;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  bool editable = false;
  TextEditingController groupName = TextEditingController();
  Constants consts = new Constants();
  Map<String, Object> rcvdData;
  Group temp = Group();
  double height=0, width = 0;
  List<Maintainance> myMaintenance = new List<Maintainance>();
  List<Maintainance> myTemps = new List<Maintainance>();

  @override
  Widget build(BuildContext context) {
    if(firsttime)
    {
      rcvdData = ModalRoute.of(context).settings.arguments;
      if (rcvdData["group"] != null) temp = rcvdData["group"];
      print(temp.lifts.length.toString());
      getMaintenance();
      firsttime = false;
    }

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: myAppBar(
        title: "UMUT ",
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.8155,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          itemCount: temp.lifts.length,
                          itemBuilder: (context, index) =>
                              this._buildRow(this.temp.lifts[index]),
                        ),
                        ),
                    ],
                  ),
                ),
                copyRights(),
              ],
            ),
          ),
        ],
      ),
    );

  }

  _buildRow(Asansor temp)
  {
    return Card(
        child: Row(
          children: [
          IconButton(
            padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
            icon: Icon(
              Icons.location_on,
              color: Colors.green,
              size: 30.0,
            ),
            onPressed: () {
              MapUtils.openMap(temp.location.latitude,temp.location.longitude);
              },
          ),
          Container(
              margin: EdgeInsets.symmetric(horizontal: width*0.1),
              width: width*0.1,
              child: Text(temp.buildingname)),
          VerticalDivider(
            color: Colors.black54,
            thickness: 1,
            width: 1,
            indent: 10,
            endIndent: 10,
          ),
            IconButton(
            padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
            icon: Icon(
              temp.maintanances[0].icon,
              color: temp.maintanances[0].color,
              size: 30.0,
            ),
              onPressed: () {
              setState(() {
                if( temp.maintanances[0].done == "0")
                 {
                   temp.maintanances[0].done = "1";
                   temp.maintanances[0].color = Colors.green;
                   temp.maintanances[0].location = temp.location;
                   post3Maintenance(temp.maintanances[0]);
                 }
              });
              },
            ),
            VerticalDivider(
              thickness: 5,
              width: 1,
              indent: 1,
              endIndent: 1,
            ),
            IconButton(
              padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
              icon: Icon(
                temp.maintanances[1].icon,
                color: temp.maintanances[1].color,
                size: 30.0,
              ),
              onPressed: () {
                setState(() {
                  if( temp.maintanances[1].done == "0")
                  {
                    temp.maintanances[1].done = "1";
                    temp.maintanances[1].color = Colors.green;
                    temp.maintanances[1].location = temp.location;
                    post3Maintenance(temp.maintanances[1]);
                  }
                });
              },
            ),
            VerticalDivider(
              color: Colors.black54,
              thickness: 1,
              width: 1,
              indent: 10,
              endIndent: 10,
            ),
            IconButton(
              padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
              icon: Icon(
                temp.maintanances[2].icon,
                color: temp.maintanances[2].color,
                size: 30.0,
              ),
              onPressed: () {
                setState(() {
                  if( temp.maintanances[2].done == "0")
                  {
                    temp.maintanances[2].done = "1";
                    temp.maintanances[2].color = Colors.green;
                    temp.maintanances[2].location = temp.location;
                    post3Maintenance(temp.maintanances[2]);
                  }
                });
              },
            ),
        ],
    ),
    elevation: 8,
    shadowColor: Colors.green,
    margin: EdgeInsets.all(20),
    shape:  OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: BorderSide(color: Colors.green, width: 1)
  ),
    );
  }

  Future<void> getMaintenance() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_maintainances);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);
      myMaintenance.addAll(data.map((maintenan) => new Maintainance().fromJson(maintenan)).toList());
      myTemps.addAll(myMaintenance);
      setState(() {});
    } finally {
      client.close();
    }
  }

  Future<void> post3Maintenance(Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+"update");
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else {
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }


}
