import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';

class MaintenanceDetails extends StatefulWidget {
  Maintainance myMaintenance;

  MaintenanceDetails({Key key, this.myMaintenance}) : super(key: key);

  @override
  _MaintenanceDetailsState createState() => _MaintenanceDetailsState();
}

class _MaintenanceDetailsState extends State<MaintenanceDetails> {
  var value = 0;
  String Country = " ";
  Constants consts = new Constants();

  Position _currentPosition;
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  String _dropDownValue;
  bool editable = false;
  bool isSwitched = false;

  TextEditingController liftController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController employeeNameController = new TextEditingController();


  @override
  void initState() {
    super.initState();
    //_getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Maintainance temp = Maintainance();
    Map<String, Object> rcvdData = ModalRoute.of(context).settings.arguments;

    if (rcvdData["maintenanceDetails"] != null) temp = rcvdData["maintenanceDetails"];

    print(temp.location);
    if(temp.done == '0') isSwitched = false; else isSwitched = true;

    print(temp.id);

    String appTitle = 'ASANSÖRCÜ';
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: true,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8156,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'MaintenanceInfo'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.7,
                            0),
                        child: Text(
                          'Lift ID'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: liftController,
                          enabled: false,
                          decoration: InputDecoration(
                            hintText: temp.liftNumber,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.credit_card),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.04,
                            MediaQuery.of(context).size.height * 0.0,
                            MediaQuery.of(context).size.width * 0.0,
                            0),
                        child: Text(
                          'Done'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding:EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.7,
                            MediaQuery.of(context).size.height * 0.0,
                            MediaQuery.of(context).size.width * 0.0,
                            0),
                        child: Switch(
                          value: isSwitched,

                          onChanged: (value){
                            setState(() {
                              if(value ==true)
                                {
                                  print(temp.location.longitude);
                                  temp.done = "1";
                                  post3Maintenance(temp);
                                  isSwitched=value;
                                }
                            });
                          },
                          activeTrackColor: Colors.lightGreenAccent,
                          activeColor: Colors.green,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.65,
                            0),
                        child: Text(
                          'constructor_name'.tr().toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: employeeNameController,
                          decoration: InputDecoration(
                            hintText: temp.employeeName.toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.contacts),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.68,
                            0),
                        child: Text(
                          'date_time'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: dateController,
                          decoration: InputDecoration(
                            enabled: false,
                            hintText: temp.dateTime,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.date_range_outlined),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'description'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: TextField(
                            controller: descriptionController,
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            decoration: InputDecoration(
                              enabled: true,
                              hintText: temp.description,
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.info),
                            ),
                          ),
                        ),
                      ),

                      Card(
                        shadowColor: Colors.black45,
                        margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        elevation: 5.0,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.amber,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'save'.tr().toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Icon(Icons.save),
                            ],
                          ),
                          onPressed: () {
                            Maintainance tempe = new Maintainance(
                              id: temp.id,
                                dateTime: temp.dateTime,
                                done: temp.done,
                                location: temp.location,
                                //typeo: 'Emergency',
                                liftNumber: temp.liftNumber
                            );
                            if(employeeNameController.text.isNotEmpty)
                              tempe.employeeName = employeeNameController.text;
                            else
                              tempe.employeeName = temp.employeeName;
                            if(descriptionController.text.isNotEmpty)
                              tempe.description = descriptionController.text;
                            else
                              tempe.description = temp.description;
                             putRequest(tempe);

                            // _awaitReturnValueFromSecondScreen(
                            //     context, 'update', tempe);
                          },
                        ),
                      ),
                      Card(
                        shadowColor: Colors.black45,
                        margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        elevation: 5.0,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.redAccent,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'delete'.tr().toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Icon(Icons.delete_outline),
                            ],
                          ),
                          onPressed: () {
                             _awaitReturnValueFromSecondScreen(
                                 context, 'delete', temp);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ), //drawer: MyDrawer(),
    );
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context, String op, Maintainance temp) async {
    // start the SecondScreen and wait for it to finish with a result

    if (op == 'delete') {
      deleteRequest(temp);
      Navigator.popAndPushNamed(context, "/");
    }
      else if (op == 'update') {
      Navigator.pushNamed(context, "/",
          arguments: {"asansor": temp, "ops": "update"});
    } else {
      //NAVIGATE TO MAINTENANCES
    }
    // after the SecondScreen result comes back update the Text widget with it
  }

  Future<Response> deleteRequest (Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+temp.id);

    await delete(url
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

  Future<Response> putRequest (Maintainance temp) async {

    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+temp.id);

    var body = temp.toJsonEdit();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
        {
          Navigator.pop(context);
          Fluttertoast.showToast(
              msg: "Maintenance Success".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      else
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
    });
  }



  Future<void> post3Maintenance(Maintainance temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+"update");
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else {
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  double _getHeight()
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*.8156;
    }
    return MediaQuery.of(context).size.height*.5873;
  }

}