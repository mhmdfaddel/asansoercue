import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;


class CreateMaintainance extends StatefulWidget {
  @override
  _CreateMaintainanceState createState() => _CreateMaintainanceState();
}

class _CreateMaintainanceState extends State<CreateMaintainance> {

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Constants consts = new Constants();
  Position _currentPosition;

  String _dropDownValue;

  TextEditingController dateController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController employeeNameController = TextEditingController();
  TextEditingController employeeNumberController = TextEditingController();

  int value = 0;
  String Country = " ";
  bool isSwitched = false;
  String _date;
  List<Asansor> myLifts = [];
  DateTime datetime;
  String lift_id = "Asansör";
  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    datetime = DateTime.now();
    getLifts();
     _date = datetime.year.toString()+'-'+datetime.month.toString()+'-'+datetime.day.toString();

  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      //_getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }


@override
  Widget build(BuildContext context) {
    final String appTitle = 'ASANSÖRCÜ';


    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8156,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'createMaintain'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'lifts'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),

                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: DropdownButton(
                          hint: Text(lift_id),
                          isExpanded: true,
                          items: myLifts.map((value) {
                            return DropdownMenuItem<String>(
                              value: value.buildingname,
                              child: Text(value.buildingname),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              lift_id = value;
                              print(value);
                            });
                          },
                        )
                      ),
                    ],
                  ),

                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'date_time'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: dateController,
                          enabled: false,
                          decoration: InputDecoration(
                            hintText: datetime.year.toString()+'-'+datetime.month.toString()+'-'+datetime.day.toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.date_range_outlined),
                          ),
                        ),
                      ),
                    ],
                  ),


                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.6,
                            0),
                        child: Text(
                          'constructor_name'.tr().toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: employeeNameController,
                          decoration: InputDecoration(
                            hintText: 'constructor_name'.tr().toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.contacts),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.04,
                            MediaQuery.of(context).size.height * 0.0,
                            MediaQuery.of(context).size.width * 0.0,
                            0),
                        child: Text(
                          'Done'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding:EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.7,
                            MediaQuery.of(context).size.height * 0.0,
                            MediaQuery.of(context).size.width * 0.0,
                            0),
                        child: Switch(
                          value: isSwitched,
                          onChanged: (value){
                            setState(() {
                              isSwitched=value;
                              print(isSwitched);
                            });
                          },
                          activeTrackColor: Colors.lightGreenAccent,
                          activeColor: Colors.green,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.75,
                            0),
                        child: Text(
                          'description'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: descriptionController,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          decoration: InputDecoration(
                            hintText: 'description'.tr().toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.description),
                          ),
                        ),
                      ),
                    ],
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                      _awaitReturnValueFromSecondScreen(context);
                      });
                    },
                    color: Color(0xffe46b10),
                    child: Icon(
                      Icons.save,
                      size: 24,
                    ),
                    padding: EdgeInsets.all(16),
                    shape: CircleBorder(),
                  )
                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ),
    );
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result

    Maintainance temp = new Maintainance(typeo: 'Emergency',liftNumber: _dropDownValue.toString(), employeeName: employeeNameController.value.text,dateTime: _date, description: descriptionController.value.text,done: (isSwitched == false)?'0':'1');
    temp.liftNumber = lift_id;
    temp.description.replaceAll('\n', '\\n');
    postRequest(temp);
    // after the SecondScreen result comes back update the Text widget with it
  }

  Future<http.Response> postRequest (Maintainance temp) async {

    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances);

    var body = temp.toJsonEdit();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await http.post(url,
        headers: headers,
        body: encodedbody
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
        {
          Navigator.popAndPushNamed(context, "/");
        }
    });
  }

  Future<void> getLifts() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);
      http.Response response = await client.get(url);

      List data = jsonDecode(response.body);
      myLifts = data.map((e) => new Asansor().fromJson(e)).toList();
      setState(() {});
    } finally {
      client.close();
    }

  }

}
