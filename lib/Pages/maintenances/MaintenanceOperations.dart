import 'dart:convert';
import 'dart:core';
import 'dart:ui';

import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/MyDrawer.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';


class MaintenanceOperations extends StatefulWidget {
  @override
  _MaintenanceOperationsState createState() => _MaintenanceOperationsState();
}

class _MaintenanceOperationsState extends State<MaintenanceOperations> {
  String _dropDownValue;

  String engineless, cabineless, wellless;

  int value = 0;
  bool normal = true;

  TextEditingController search = new TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isWellSwitched = false, isEngineSwitched = false, isCabineSwitched = false;
  List<Maintainance> cabinetemp =[], enginetemp=[], welltemp = [];

  Color tiles = Colors.grey;
  Color items = Colors.black;

  String desc, emplo,dateo,kimlik,id, lifttype, liftkind;

  Constants consts = new Constants();

  Future<void> getMaintenance() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_maintainances);
      http.Response response = await client.get(url);
      List data = jsonDecode(response.body);

      myMaintenance = new List<Maintainance>();

      myMaintenance = data.map((data) => new Maintainance().fromJson(data)).toList();
      for (Maintainance temp in myMaintenance) {
        if(temp.liftType == "Well")
          {
            well.add(temp);
          }
        else if(temp.liftType == "Cabine")
          {
            cabine.add(temp);
          }
        else if(temp.liftType == "Engine")
          engine.add(temp);
      }
      mytemps.addAll(myMaintenance);
      cabinetemp.addAll(cabine); welltemp.addAll(well); enginetemp.addAll(engine);
      setState(() {});
    } finally {
      client.close();
    }
  }

  List<Maintainance> myMaintenance = [];
  List<Maintainance> mytemps = [];

  List<Maintainance> engine = [];
  List<Maintainance> well = [];
  List<Maintainance> cabine = [];

  List<Maintainance> existMaintainance(String temp, String cirtic) {
    List<Maintainance> testo = new List<Maintainance>();

    if(cirtic == 'date_time'.tr().toString())
      for (int i = 0; i < myMaintenance.length; i++) {
        if (myMaintenance[i].dateTime.contains(temp)) {
          testo.add(myMaintenance[i]);
        }
      }
    else if(cirtic == 'constructor_name'.tr().toString()){
      for (int i = 0; i < myMaintenance.length; i++) {
        if (myMaintenance[i].employeeName.contains(temp)) {
          testo.add(myMaintenance[i]);
        }
      }
    }
    return testo;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMaintenance();
  }
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    setState(() {
      myMaintenance.clear();
      well.clear();
      engine.clear();
      cabine.clear();
      getMaintenance();
      cabineless = null;
      engineless = null;
      wellless = null;

    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        myMaintenance.clear();
        well.clear();
        engine.clear();
        cabine.clear();
        getMaintenance();
        cabineless = null;
        cabinetemp.clear();
        enginetemp.clear();
        welltemp.clear();
        engineless = null;
        wellless = null;
      });
    _refreshController.loadComplete();
  }

  void _addItem() {
    Navigator.pushNamed(context, '/createMainten');
  }

  @override
  Widget build(BuildContext context) {
    final String appTitle = 'ASANSÖRCÜ';

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: MediaQuery.of(context).size.height*.7,
              child: Center(child:body)
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'MaintenanceOps'.tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid),
                borderRadius:
                    BorderRadius.all(Radius.circular(50)), // set border width
                // set rounded corner radius
              ),
              child: TextField(
                controller: search,
                onChanged: (value) =>{
                  setState(() {
                    if(this.search.value.text == "")
                    {
                      myMaintenance = mytemps;
                    }
                    else if(_dropDownValue == 'date_time'.tr().toString())
                    {
                      myMaintenance = existMaintainance(search.value.text, 'date_time'.tr().toString());
                    }
                    else if(_dropDownValue == 'constructor_name'.tr().toString())
                    {
                      myMaintenance = existMaintainance(search.value.text, 'constructor_name'.tr().toString());
                    }
                  })
                },
                decoration: InputDecoration(
                  hintText: 'search_mainten'.tr().toString(),
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.height * 0.0,
                  MediaQuery.of(context).size.width * 0.0,
                  0,
                  MediaQuery.of(context).size.height * 0.0),
              child: Row(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.width * 0.4,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text('search_by'.tr().toString())
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: Colors.blue),
                              ),
                        isExpanded: true,
                        iconSize: 15.0,
                        style: TextStyle(color: Colors.blue),
                        items: [
                          'date_time'.tr().toString(),
                          'constructor_name'.tr().toString()
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val;
                            },
                          );
                        },
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * .02, 0, 0, 0),
                    child: Text(
                      'total'.tr().toString() +
                          ' : ' +
                          this.myMaintenance.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: IconButton(
                      color: items,
                        onPressed: (){
                          setState(() {
                            items = Colors.black;
                            tiles = Colors.grey;
                            normal = !normal;
                          });
                    }, icon: Icon(Icons.view_agenda)),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: IconButton(
                      color: tiles,
                        onPressed: (){
                          setState(() {
                            normal = !normal;
                            items = Colors.grey;
                            tiles = Colors.black;
                          });
                    }, icon: Icon(Icons.view_compact)),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Container(
                  height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                      ? MediaQuery.of(context).size.height * 0.5641
                      : MediaQuery.of(context).size.height * 0.5825,
                  width: MediaQuery.of(context).size.width * 1,
                  child: Row(
                    children: [
                      Container(
                        child: Expanded(
                          child: (normal)?ListView.builder(
                            itemCount: this.myMaintenance.length,
                            itemBuilder: (context, index) =>
                                this._buildRow(this.myMaintenance[index]),
                          ):
                            ListView.builder(
                                itemCount: 1,
                                itemBuilder: (context, index)=>
                                this._buildTiles(well, cabine, engine)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            copyRights(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {
            _addItem();
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }


  Widget _buildTiles(List<Maintainance> well, List<Maintainance> cabine, List<Maintainance> engine){
    var height = MediaQuery.of(context).size.height, width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        ExpansionTile(
          collapsedBackgroundColor: Colors.orange[300],
          backgroundColor: Colors.blueGrey,
          title: Row(
            children: [
              Text('Engine'.tr().toString(),
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.04,
                  width: MediaQuery.of(context).size.width * 0.5,
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                  child: DropdownButton(
                    hint: engineless == null
                        ? Text('Lift Type'.tr().toString())
                        : Text(
                      engineless,
                      style: TextStyle(color: Colors.blue),
                    ),
                    isExpanded: true,
                    iconSize: 15.0,
                    style: TextStyle(color: Colors.blue),
                    items: [
                      'all'.tr().toString(),
                      'Gear Full Engine'.tr().toString(),
                      'Gear Less Engine'.tr().toString()
                    ].map(
                          (val) {
                        return DropdownMenuItem<String>(
                          value: val,
                          child: Text(val),
                        );
                      },
                    ).toList(),
                    onChanged: (val) {
                      setState(
                            () {
                              engineless = val;
                              _doEngineCustomizing();
                        },
                      );
                    },
                  )),
            ],
          ),
          children: List.generate(engine.length, (index) => Column(
            children: [
              ListTile(
                title: Text(engine[index].liftNumber),

                onTap: (){
                  Navigator.pushNamed(context, "/maintenanceDetails",
                      arguments: {"maintenanceDetails": engine[index]});                    },
                leading: IconButton(
                  padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.green,
                    size: 30.0,
                  ),
                  onPressed: () {
                    MapUtils.openMap(engine[index].location.latitude,engine[index].location.longitude);
                  },
                ),
              )
            ],
          )),
        ),
        ExpansionTile(
          collapsedBackgroundColor: Colors.orange[300],
          backgroundColor: Colors.blueGrey,

          title: Row(
            children: [
              Text('Cabine'.tr().toString(),
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.04,
                  width: MediaQuery.of(context).size.width * 0.5,
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                  child: DropdownButton(
                    hint: cabineless == null
                        ? Text('Lift Type'.tr().toString())
                        : Text(
                      cabineless,
                      style: TextStyle(color: Colors.blue),
                    ),
                    isExpanded: true,
                    iconSize: 15.0,
                    style: TextStyle(color: Colors.blue),
                    items: [
                      'all'.tr().toString(),
                      'Gear Full Engine'.tr().toString(),
                      'Gear Less Engine'.tr().toString()
                    ].map(
                          (val) {
                        return DropdownMenuItem<String>(
                          value: val,
                          child: Text(val),
                        );
                      },
                    ).toList(),
                    onChanged: (val) {
                      setState(
                            () {
                              cabineless = val;
                              _doCabineCustomizing();
                        },
                      );
                    },
                  )),
            ],
          ),
          children: List.generate(cabine.length, (index) => Column(
            children: [
              ListTile(
                title: Text(cabine[index].liftNumber),
                onTap: (){
                  Navigator.pushNamed(context, "/maintenanceDetails",
                      arguments: {"maintenanceDetails": cabine[index]});
                  },
                leading: IconButton(
                  padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.green,
                    size: 30.0,
                  ),
                  onPressed: () {
                    //print(temp.lifts[index].location.longitude);
                    MapUtils.openMap(cabine[index].location.latitude, cabine[index].location.longitude);
                  },
                ),
              )
            ],
          )),
        ),
        ExpansionTile(
          collapsedBackgroundColor: Colors.orange[300],
          backgroundColor: Colors.blueGrey,

          title: Row(
            children: [
              Text('Well'.tr().toString(),
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.04,
                  width: MediaQuery.of(context).size.width * 0.5,
                  padding: EdgeInsets.fromLTRB(
                      MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                  child: DropdownButton(
                    hint: wellless == null
                        ? Text('Lift Type'.tr().toString())
                        : Text(
                      wellless,
                      style: TextStyle(color: Colors.blue),
                    ),
                    isExpanded: true,
                    iconSize: 15.0,
                    style: TextStyle(color: Colors.blue),
                    items: [
                      'all'.tr().toString(),
                      'Gear Full Engine'.tr().toString(),
                      'Gear Less Engine'.tr().toString()
                    ].map(
                          (val) {
                        return DropdownMenuItem<String>(
                          value: val,
                          child: Text(val),
                        );
                      },
                    ).toList(),
                    onChanged: (val) {
                      setState(
                            () {
                              wellless = val;
                              _doWellCustomizing();
                        },
                      );
                    },
                  )),
            ],
          ),
          children: List.generate(well.length, (index) => Column(
            children: [
              ListTile(
                title: Text(well[index].liftNumber),
                onTap: (){
                  Navigator.pushNamed(context, "/maintenanceDetails",
                      arguments: {"maintenanceDetails": well[index]});
                  },
                leading: IconButton(
                  padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.green,
                    size: 30.0,
                  ),
                  onPressed: () {
                    //print(temp.lifts[index].location.longitude);
                    MapUtils.openMap(well[index].location.latitude, well[index].location.longitude);
                  },
                ),
              )
            ],
          )),
        ),
      ],
    );

  }

  _buildRow(Maintainance temp) {
    return GestureDetector(
      onTap: (){
        print(temp.location);
        Navigator.pushNamed(context, "/maintenanceDetails",
            arguments: {"maintenanceDetails": temp});
      },
      onHorizontalDragStart: (DragStartDetails details){
        setState(() {
        });
      },
      onHorizontalDragEnd: (DragEndDetails details){
        if (details.primaryVelocity > 0) {

          setState(() {
            _showDialog(temp);

          });
        }
        else
        {
          setState(() {
          });
        }
      },

      child: Row(
        children: [
          Card(
            margin: const EdgeInsets.all(10),
            elevation: 15.0,
            color: temp.color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'Lift ID'.tr().toString() +
                              ' : ' +
                              temp.liftNumber.toString(),
                          style: TextStyle(),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          "constructor_name".tr().toString() +
                              ": " +
                              temp.employeeName.toString(),
                          style: TextStyle(),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(
                          5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                      child: GestureDetector(
                        onTap: () {},
                        child: Text(
                          'date_time'.tr().toString() +
                              ' : ' +
                              temp.dateTime.toString(),
                          style: TextStyle(),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }

  _doCabineCustomizing(){
    if(cabineless == 'all'.tr().toString())
      {
        cabine.clear();
        cabine.addAll(cabinetemp);
      }
    else if(cabineless == 'Gear Full Engine'.tr().toString())
      {
        cabine.clear();
        for(Maintainance m in cabinetemp)
          {
            if(m.lift_kind == 'Gear Full Engine'.tr().toString())
              {
                cabine.add(m);
              }
          }
        return cabine;
      }
    else
      {
        cabine.clear();
        for(Maintainance m in cabinetemp)
        {
          if(m.lift_kind == 'Gear Less Engine'.tr().toString())
          {
            cabine.add(m);
          }
        }
        return cabine;

      }
  }

  _doEngineCustomizing()
  {
    if(engineless == 'all'.tr().toString())
    {
      engine.clear();
      engine.addAll(enginetemp);
    }
    else if(engineless == 'Gear Full Engine'.tr().toString())
    {
      engine.clear();
      for(Maintainance m in enginetemp)
      {
        if(m.lift_kind == 'Gear Full Engine'.tr().toString())
        {
          engine.add(m);
        }
      }
      return engine;
    }
    else
    {
      engine.clear();
      for(Maintainance m in enginetemp)
      {
        if(m.lift_kind == 'Gear Less Engine'.tr().toString())
        {
          engine.add(m);
        }
      }
      return engine;
    }
  }

  _doWellCustomizing()
  {
    if(wellless == 'all'.tr().toString())
    {
      well.clear();
      well.addAll(welltemp);
    }
    else if(wellless == 'Gear Full Engine'.tr().toString())
    {
      print('helllo');
      well.clear();
      for(Maintainance m in welltemp)
      {
        print(m.lift_kind);
        if(m.lift_kind == 'Gear Full Engine'.tr().toString())
        {
          well.add(m);
        }
      }
      return well;
    }
    else
    {
      well.clear();
      for(Maintainance m in welltemp)
      {
        if(m.lift_kind == 'Gear Less Engine'.tr().toString())
        {
          print('helllo');
          well.add(m);
        }
      }
      return well;
    }
  }

  _showDialog(Maintainance temp) async {
    await showDialog<String>(
      context: context,
      builder: (context) => new _SystemPadding(child: new AlertDialog(
        contentPadding: const EdgeInsets.all(10.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                autofocus: true,
                controller: passwordController,
                decoration: new InputDecoration(

                    labelText: 'Password', hintText: 'type your password'),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child:  Text('Cancel'.tr().toString()),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child:  Text('Ok'),
              onPressed: () {
                if(passwordController.text == (new Constants()).password)
                {
                  deleteMaintenance(temp);
                  myMaintenance.remove(temp);
                  Navigator.pop(context);
                }
              })
        ],
      ),
      ),
    );
  }

  double _getHeight()
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*0.5688;
    }
    return MediaQuery.of(context).size.height*.5873;
  }

  Future<void> deleteMaintenance(Maintainance temp) async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_maintainances+temp.id.toString());
      http.Response response = await client.delete(url);
      List data = jsonDecode(response.body);

      if(response.statusCode == 200)
        {
          Fluttertoast.showToast(
              msg: "Deleted".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      else{
        Fluttertoast.showToast(
            msg: "Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      setState(() {});
    } finally {
      client.close();
    }
  }

}


class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
