import 'dart:convert';

import 'package:asansorci/Models/GridCard.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MyDrawer.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:translator/translator.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final String appTitle = 'ASANSÖRCÜ';

  List<GridCard> Gcards = [
    GridCard(
      funct: "lifts".tr().toString(),
      color: Colors.red[700],
      icon: Icons.sensor_door_sharp,
      toGo: "/lifts",
      cross: MainAxisAlignment.start
    ),
    GridCard(
      funct: "list_of_maintenance".tr().toString(),
      color: Colors.green[700],
      icon: Icons.handyman,
      toGo: "/maintenOps",
      cross: MainAxisAlignment.end,
    ),
    GridCard(
      funct: "InProgresslifts".tr().toString(),
      color: Colors.brown[700],
      icon: Icons.settings_rounded,
      toGo: "/inProgressLiftsView",
        cross: MainAxisAlignment.start
    ),
    GridCard(
      funct: "Tests Book".tr().toString(),
      color: Colors.pink[700],
      icon: Icons.restore,
      toGo: "/StandardView",
        cross: MainAxisAlignment.end
    ),
    GridCard(
      funct: "Employees".tr().toString(),
      color: Colors.deepOrange[700],
      icon: Icons.person,
      toGo: "/employees",
        cross: MainAxisAlignment.start
    ),
    GridCard(
      funct: "Maps".tr().toString(),
      color: Colors.blue[700],
      icon: Icons.location_on,
      toGo: "/maps",
        cross: MainAxisAlignment.end
    ),
    GridCard(
      funct: "Groups".tr().toString(),
      color: Colors.orange[700],
      icon: Icons.group_work_sharp,
      toGo: "/groups",
        cross: MainAxisAlignment.start
    ),
    GridCard(
      funct: "Hyper View".tr().toString(),
      color: Colors.grey[700],
      icon: Icons.all_inclusive,
      toGo: "/hypeView",
        cross: MainAxisAlignment.end
    ),
    GridCard(
        funct: "Contact Us & Report Bugs".tr().toString(),
        color: Colors.teal[700],
        icon: Icons.contact_support,
        toGo: "/contactus",
        cross: MainAxisAlignment.start
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMaintenance();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: myAppBar(
        title: appTitle,
        isHome: true,
        isDetails: false,
      ),
      drawer: MyDrawer(),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.8155,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          itemCount: Gcards.length,
                          itemBuilder: (context, index) {
                            return new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              ),
                              shadowColor: Colors.blue,
                              margin: EdgeInsets.all(4),
                              elevation: 10.0,
                              child: new InkResponse(
                                child: Gcards[index],
                                onTap: () {},
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                copyRights(),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed:() {
          setState(() {
            String msg = count.toString()+" "+"Maintenance Forgotten".tr().toString();
            showOverlayNotification((context) {
              return Card(
                margin: const EdgeInsets.symmetric(horizontal: 4),
                child: SafeArea(
                  child: ListTile(
                    leading: SizedBox.fromSize(
                        size: const Size(40, 40),
                        child: ClipOval(
                            child: Icon(
                              Icons.warning_amber,
                              color: Colors.red,
                            ))),
                    title: Text('Maintenance Notification'.tr().toString()),
                    subtitle: Text(msg),
                    trailing: IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          OverlaySupportEntry.of(context).dismiss();
                        }),
                  ),
                ),
              );
            }, duration: Duration(milliseconds: 4000));
          });
        },
        child: Icon(Icons.remove_red_eye, color: Colors.white,),
      ),
    );
  }
  int count = 0;
  Future<void> getMaintenance() async {
    var client = new Client();
    Constants consts = new Constants();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_maintainances);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);

      List<Maintainance>myMaintenance = new List<Maintainance>();

      myMaintenance = data.map((data) => new Maintainance().fromJson(data)).toList();
      for (Maintainance temp in myMaintenance)
        {
          if(temp.done == "0")
            count++;
        }
      setState(() {});
    } finally {
      client.close();
    }
  }
}
