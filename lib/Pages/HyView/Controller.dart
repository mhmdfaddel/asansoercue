import 'dart:convert';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Group.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class TabBarController extends StatefulWidget {
  const TabBarController({Key key}) : super(key: key);
  @override
  _TabBarControllerState createState() => _TabBarControllerState();
}

class _TabBarControllerState extends State<TabBarController>
    with SingleTickerProviderStateMixin {

  Constants consts= new Constants();
  var height=0.0, width = 0.0;
  String desc, emplo,dateo,kimlik,id;

  TabController _controller;
  int _selectedIndex = 0;
  final String appTitle = 'UMUT ASANSÖR';

  List<Maintainance> myMaintenance = [];
  List<Maintainance> mytemps = [];

  List<Group> myGroups = [];
  List<Group> tempGroup = [];

  List<Widget> list = [
    Tab(text: "list_of_maintenance".tr().toString(),icon: Icon(Icons.handyman)),
    Tab(text: "Groups".tr().toString(),icon: Icon(Icons.group_work)),
  ];

  String _dropDownValue;

  int value = 0;

  TextEditingController searchGroup = new TextEditingController();
  TextEditingController searchMaintenance = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();


  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        myGroups.clear();
        myMaintenance.clear();
        getGroups();
        getMaintenance();
      });
    _refreshController.loadComplete();
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
    });

    getGroups();
    getMaintenance();
  }


  List<Group> existGroup(String temp, String cirtic) {
    List<Group> testo = new List<Group>();

    for (int i = 0; i < myGroups.length; i++) {
      if (myGroups[i].name.contains(temp)) {
        testo.add(myGroups[i]);
      }
    }
    return testo;
  }

  @override
  Widget build(BuildContext context) {

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(
            appTitle,
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          centerTitle: true,
          backgroundColor: Color(0xFF0CA8F1),

          bottom: TabBar(
            onTap: (index) {
              // Should not used it as it only called when tab options are clicked,
              // not when user swapped
            },
            controller: _controller,
            tabs: list,
          ),
        ),
        body: SmartRefresher(

          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(
            builder: (BuildContext context,LoadStatus mode){
              Widget body ;
              if(mode==LoadStatus.idle){
                body =  Text("pull up load");
              }
              else if(mode==LoadStatus.loading){
                body =  CupertinoActivityIndicator();
              }
              else if(mode == LoadStatus.failed){
                body = Text("Load Failed!Click retry!");
              }
              else if(mode == LoadStatus.canLoading){
                body = Text("release to load more");
              }
              else{
                body = Text("No more Data");
              }
              return Container(
                height: 55.0,
                child: Center(child:body),
              );
            },
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,

          child: TabBarView(
            controller: _controller,
            children: [
              Center(
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.06,
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.grey, // set border color
                            width: 0.5,
                            style: BorderStyle.solid),
                        borderRadius:
                        BorderRadius.all(Radius.circular(50)), // set border width
                        // set rounded corner radius
                      ),
                      child: TextField(
                        controller: searchMaintenance,
                        onChanged: (value){
                          setState(() {
                            if(this.searchMaintenance.value.text == "")
                            {
                              myMaintenance = mytemps;
                            }
                            else if(_dropDownValue == 'date_time'.tr().toString())
                            {
                              myMaintenance = existMaintainance(searchMaintenance.value.text, 'date_time'.tr().toString());
                            }
                            else if(_dropDownValue == 'constructor_name'.tr().toString())
                            {
                              myMaintenance = existMaintainance(searchMaintenance.value.text, 'constructor_name'.tr().toString());
                            }
                          });
                        },
                        decoration: InputDecoration(
                          hintText: 'search_mainten'.tr().toString(),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          border: InputBorder.none,
                          icon: Icon(Icons.search),
                        ),
                      ),
                    ),
                    Container(
                      // height: MediaQuery.of(context).size.height*0.055,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.02,
                          MediaQuery.of(context).size.width * 0.02,
                          0,
                          MediaQuery.of(context).size.width * 0.02),
                      child: Row(
                        children: [
                          Container(
                              height: MediaQuery.of(context).size.height * 0.04,
                              width: MediaQuery.of(context).size.width * 0.47,
                              padding: EdgeInsets.fromLTRB(
                                  MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                              child: DropdownButton(
                                hint: _dropDownValue == null
                                    ? Text('search_by'.tr().toString())
                                    : Text(
                                  _dropDownValue,
                                  style: TextStyle(color: Colors.blue),
                                ),
                                isExpanded: true,
                                iconSize: 15.0,
                                style: TextStyle(color: Colors.blue),
                                items: [
                                  'date_time'.tr().toString(),
                                  'constructor_name'.tr().toString()
                                ].map(
                                      (val) {
                                    return DropdownMenuItem<String>(
                                      value: val,
                                      child: Text(val),
                                    );
                                  },
                                ).toList(),
                                onChanged: (val) {
                                  setState(
                                        () {
                                      _dropDownValue = val;
                                    },
                                  );
                                },
                              )),
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.2, 0, 0, 0),
                            child: Text(
                              'total'.tr().toString() +
                                  ' : ' +
                                  this.myMaintenance.length.toString(),
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                          ? MediaQuery.of(context).size.height * 0.5688
                          : MediaQuery.of(context).size.height * 0.5712,
                      width: MediaQuery.of(context).size.width * 1,
                      child: Column(
                        children: [
                          Container(
                            child: Expanded(
                              child: ListView.builder(
                                itemCount: this.myMaintenance.length,
                                itemBuilder: (context, index) =>
                                    this._buildMaintenanceRow(this.myMaintenance[index]),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    copyRights(),
                  ],
                  ),
              ),
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Groups'.tr().toString(),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 21.0,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: height * 0.062,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid),
                          borderRadius:
                          BorderRadius.all(Radius.circular(50)), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: searchGroup,
                          onChanged: (value) =>{
                            setState(() {
                              if(this.searchGroup.value.text == "")
                              {
                                myGroups = tempGroup;
                              }
                              else
                                myGroups = existGroup(searchGroup.value.text, "");
                            })
                          },
                          decoration: InputDecoration(
                            hintText: 'search_group'.tr().toString(),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.search),
                          ),
                        ),
                      ),
                      Container(

                        margin: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.02,
                            MediaQuery.of(context).size.width * 0.02,
                            0,
                            MediaQuery.of(context).size.width * 0.02),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(
                                  MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                              child: Text(
                                'total'.tr().toString() +
                                    ' : ' +
                                    this.myGroups.length.toString(),
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                              ? MediaQuery.of(context).size.height * 0.498
                              : MediaQuery.of(context).size.height * 0.515,
                          width: MediaQuery.of(context).size.width * 1,
                          child:  Row(
                            children: [
                              Container(
                                child: Expanded(
                                  child: ListView.builder(
                                    itemCount: this.myGroups.length,
                                    itemBuilder: (context, index) =>
                                        this._buildRow(this.myGroups[index]),
                                  ),
                                ),
                              ),
                            ],
                          )
                      ),
                      copyRights(),
                    ],
                  ),
                )
              ),
            ],
          ),
        ),
      );
  }

  Future<void> deleteGroups(Group temp) async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_groups+temp.id.toString());
      Response response = await client.delete(url);

      setState(() {});
    } finally {
      client.close();
    }
  }

  _showDialog(Group temp) async {
    await showDialog<String>(
      context: context,

      builder: (context) => AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        //insetPadding: ,
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                autofocus: true,
                controller: passwordController,
                decoration: new InputDecoration(
                    labelText: 'Password', hintText: 'type your password'),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child:  Text('Cancel'.tr().toString()),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child:  Text('Ok'),
              onPressed: () {
                if(passwordController.text == (new Constants()).password)
                {
                  Fluttertoast.showToast(
                      msg: "Deleted".tr().toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                  deleteGroups(temp);
                  myGroups.remove(temp);
                  Navigator.pop(context);
                }
                else
                {
                  Fluttertoast.showToast(
                      msg: "Wrong Password".tr().toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                }
              })
        ],
      ),
    );
  }

  _buildRow(Group temp) {
    return GestureDetector(
      onTap: (){
        setState(() {
        });
      },
      onHorizontalDragStart: (DragStartDetails details){
        setState(() {
        });
      },
      onHorizontalDragEnd: (DragEndDetails details){
        if (details.primaryVelocity > 0) {

          setState(() {
            _showDialog(temp);

          });
        }
        else
        {
          setState(() {
          });
        }
      },

      child: ExpansionTile(
        collapsedBackgroundColor: Color(0xFF0CA8F1),
        backgroundColor: Colors.blueGrey,
        title: Row(
          children: [
            Text(temp.name,
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: temp.color
              ),
            ),

            IconButton(
              padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
              icon: Icon(
                Icons.info,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/groupDetails', arguments: {'group': temp});
              },
            ),
            Text(temp.enddate,
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: temp.color,
              ),
            ),
          ],
        ),

        children: List.generate(temp.lifts.length, (index) => Column(
          children: [
            ExpansionTile(
              backgroundColor: Colors.amber,
              title: Row(
                children: [
                  Text(temp.lifts[index].buildingname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                ],
              ),

              leading: IconButton(
                padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
                icon: Icon(
                  Icons.location_on,
                  color: Colors.green,
                  size: 30.0,
                ),
                onPressed: () {
                  MapUtils.openMap(temp.lifts[index].location.latitude,temp.lifts[index].location.longitude);
                },
              ),
              children: List.generate(temp.lifts[index].maintanances.length,(index1) => Column(
                children: [
                  ListTile(
                    title: Row(
                      children: [
                        Text(temp.lifts[index].maintanances[index1].dateTime,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),),
                        IconButton(
                          padding: EdgeInsets.fromLTRB(width*.0, height*.0, width*.0, height*.0),
                          icon: Icon(
                            temp.lifts[index].maintanances[index1].icon,
                            color: (temp.lifts[index].maintanances[index1].done == "1")?Colors.white:Colors.red,
                            size: 30.0,
                          ),
                          onPressed: () {
                            setState(() {
                              temp.lifts[index].maintanances[index1].done = "1";
                              putRequest(temp.lifts[index].maintanances[index1]);
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              )),
            )
          ],
        )),
      ),
    );
  }
  static Future<void> navigateTo(String address) async {
    String query = Uri.encodeComponent(address);
    String googleUrl = "https://www.google.com/maps/search/?api=1&query=$query";
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    }
    else {
      throw 'Could not open the map.';
    }

  }
  Future<Response> putRequest (Maintainance temp) async {

    print(temp.id);

    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances+temp.id);

    var body = temp.toJsonEdit();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        if(temp.liftType == "Well")
        {
          Maintainance m1 = new Maintainance();
          m1.liftNumber = temp.liftNumber;
          m1.description = '';
          m1.liftType = 'Engine';
          m1.dateTime = manipulateDate(temp.dateTime, 1);
          m1.done = '0';
          m1.employeeName = temp.employeeName;
          m1.location = temp.location;
          Maintainance m2 = new Maintainance();
          m2.liftNumber = temp.liftNumber;
          m2.liftType = 'Cabine';
          m2.description = '';
          m2.dateTime = manipulateDate(temp.dateTime, 2);
          m2.done = '0';
          m2.employeeName = temp.employeeName;
          m2.location = temp.location;
          Maintainance m3 = new Maintainance();
          m3.liftNumber = temp.liftNumber;
          m3.liftType = 'Well';
          m3.description = '';
          m3.dateTime = manipulateDate(temp.dateTime, 3);
          m3.done = '0';
          m3.employeeName = temp.employeeName;
          m3.location = temp.location;
          List<Maintainance> posted = []; posted.add(m1); posted.add(m2); posted.add(m3);
          post3Maintenance(posted);
        }
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
    });
  }


  String manipulateDate(String date, int month)
  {
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(date);
    return tempDate.year.toString() +
        "-" +
        (tempDate.month+month).toString() +
        "-" +
        tempDate.day.toString();
  }

  Future<void> post3Maintenance(List<Maintainance> temp) async {
    Uri url = Uri.https(
        consts.domain, consts.domain_maintainances);
    var body = temp.map((e) => new Maintainance().toJsonEdit()).toList();
    var encodedbody = json.encode(body);
    Map<String, String> headers = {'Content-Type': 'application/json'};

    await post(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(
            msg: "Maintenance Success".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else {
        Fluttertoast.showToast(
            msg: "Maintenance Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  Future<void> getMaintenance() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_maintainances);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);

        myMaintenance.addAll(data.map((e) => new Maintainance().fromJson(e)));
      mytemps.addAll(myMaintenance);
      setState(() {});
    } finally {
      client.close();
    }  }


  List<Maintainance> existMaintainance(String temp, String cirtic) {
    List<Maintainance> testo = new List<Maintainance>();

    if(cirtic == 'date_time'.tr().toString())
      for (int i = 0; i < myMaintenance.length; i++) {
        if (myMaintenance[i].dateTime.contains(temp)) {
          testo.add(myMaintenance[i]);
        }
      }
    else if(cirtic == 'constructor_name'.tr().toString()){
      for (int i = 0; i < myMaintenance.length; i++) {
        if (myMaintenance[i].employeeName.contains(temp)) {
          testo.add(myMaintenance[i]);
        }
      }
    }
    return testo;
  }

   _buildMaintenanceRow(Maintainance temp) {
    return Row(
      children: [
        Card(
          margin: const EdgeInsets.all(10),
          elevation: 15.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                    child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        'Lift ID'.tr().toString() +
                            ' : ' +
                            temp.liftNumber.toString(),
                        style: TextStyle(),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                    child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        "constructor_name".tr().toString() +
                            ": " +
                            temp.employeeName.toString(),
                        style: TextStyle(),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(
                        5.0, 0, 0, MediaQuery.of(context).size.height * 0.01),
                    child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        'date_time'.tr().toString() +
                            ' : ' +
                            temp.dateTime.toString(),
                        style: TextStyle(),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        RawMaterialButton(
          onPressed: () {
            setState(() {
              Navigator.pushNamed(context, "/maintenanceDetails",
                  arguments: {"maintenanceDetails": temp});
            });
          },
          elevation: 2.0,
          fillColor: Colors.amber[700],
          child: Icon(
            Icons.build,
            size: 20.0,
          ),
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
      ],
    );
  }

  Future<void> getGroups() async {
    var client = new Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_groups);
      Response response = await client.get(url);
      List data = jsonDecode(response.body);

      myGroups.addAll(data.map((e) => new Group().fromJson(e)).toList());
      tempGroup.addAll(myGroups);
      setState(() {});
    } finally {
      client.close();
    }
  }
}

