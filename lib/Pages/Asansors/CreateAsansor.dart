import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/Models/Test_Book.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:asansorci/inProgress_short/InProgressLift.dart';
import 'package:asansorci/inProgress_short/Stage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


class CreateAsansor extends StatefulWidget {
  @override
  _CreateAsansorState createState() => _CreateAsansorState();
}

class _CreateAsansorState extends State<CreateAsansor> {
  Position _currentPosition;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  String _dropDownValue, _dropLiftType;
  int value = 0;
  Constants consts = new Constants();
  // ignore: non_constant_identifier_names
  String Country = " ";
  var height= 0.0, width = 0.0;
  List<Asansor> myLifts = [];
  DateTime realdate;
  TimeOfDay time;
  LocaleType localetype;

  TextEditingController _controller;
  TextEditingController kimlikController = TextEditingController();
  TextEditingController buildingNumberController = TextEditingController();
  TextEditingController buildingNameController = TextEditingController();
  TextEditingController telephoneController = TextEditingController();
  TextEditingController installerNameController = TextEditingController();
  TextEditingController installerNumberController = TextEditingController();
  TextEditingController buildingAptController = TextEditingController();

  bool showlifttype = false;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      Placemark place = p[0];
      setState(() {
        Country = place.country;
        // _currentAddress =
        //     "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  LocaleType _changeLocal(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY'))
      {
        localetype = LocaleType.ar;
      }
    else if(EasyLocalization.of(context).locale == Locale('en', 'US'))
      {
        localetype = LocaleType.en;
      }
    else if(EasyLocalization.of(context).locale == Locale('tr', 'TR'))
      {
        localetype = LocaleType.tr;
      }
    else
      {
        localetype = LocaleType.de;
      }
    return localetype;
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: '',
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: height * 0.8155,
            width: width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'createAsansor'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: kimlikController,
                      decoration: InputDecoration(
                        hintText: 'id_number'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.credit_card),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: buildingNumberController,
                      decoration: InputDecoration(
                        hintText: 'building_number'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.house_outlined),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: buildingNameController,
                      decoration: InputDecoration(
                        hintText: 'building_name'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.house),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: buildingAptController,
                      decoration: InputDecoration(
                        hintText: 'buildingapt'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.apartment),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: telephoneController,
                      decoration: InputDecoration(
                        hintText: 'liftTelephone'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.contact_phone),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: installerNameController,
                      decoration: InputDecoration(
                        hintText: 'constructor_name'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.contacts),
                      ),
                    ),
                  ),
                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      controller: installerNumberController,
                      decoration: InputDecoration(
                        hintText: 'constructor_number'.tr().toString(),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.phone_android_outlined),
                      ),
                    ),
                  ),

                  Container(
                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    child: DropdownButton(
                      hint: _dropDownValue == null
                          ? Text('status'.tr().toString())
                          : Text(
                        _dropDownValue,
                        style: TextStyle(color: Colors.blue),
                      ),
                      isExpanded: true,
                      iconSize: 15.0,
                      style: TextStyle(color: Colors.blue),
                      items: [
                        'working'.tr().toString(),
                        'in_progress'.tr().toString(),
                        'not_working'.tr().toString()
                      ].map(
                            (val) {
                          return DropdownMenuItem<String>(
                            value: val,
                            child: Text(val),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(() {
                            _dropDownValue = val;
                            if(_dropDownValue == "in_progress".tr().toString())
                              {
                                showlifttype = true;
                              }
                            else
                              showlifttype = false;
                          },
                        );
                      },
                    ),
                  ),
                  Container(

                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    child: DropdownButton(
                      hint: _dropLiftType == null
                          ? Text('Lift Type'.tr().toString())
                          : Text(
                        _dropLiftType,
                        style: TextStyle(color: Colors.blue),
                      ),
                      isExpanded: true,
                      iconSize: 15.0,
                      style: TextStyle(color: Colors.blue),
                      items: [
                        'Gear Full Engine'.tr().toString(),
                        'Gear Less Engine'.tr().toString(),
                      ].map(
                            (val) {
                          return DropdownMenuItem<String>(
                            value: val,
                            child: Text(val),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(() {
                          _dropLiftType = val;
                        },
                        );
                      },
                    ),
                  ),
                  Visibility(
                    visible: !showlifttype,
                    child: Row(

                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.date_range),
                          color: Colors.grey,
                          iconSize: 20,
                          onPressed: () {
                            setState(() {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2000, 1, 1),
                                  maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                    setState(() {
                                      _controller = new TextEditingController(
                                          text: date.year.toString() +
                                              "-" +
                                              date.month.toString() +
                                              "-" +
                                              date.day.toString());
                                      realdate = date;
                                    });
                                    print('change $date');
                                  }, onConfirm: (date) {
                                    setState(() {
                                      _controller = new TextEditingController(
                                          text: date.year.toString() +
                                              "-" +
                                              date.month.toString() +
                                              "-" +
                                              date.day.toString());
                                      realdate = date;
                                    });
                                    print('confirm $date');
                                  },
                                  onCancel: () {},
                                  currentTime: DateTime.now(),
                                  locale: _changeLocal(context));
                            });
                          },
                        ),
                        new Expanded(
                          child: TextField(
                            controller: _controller,
                            enabled: false,
                            keyboardType: TextInputType.datetime,
                            decoration: InputDecoration(

                              hintText: "Pick Date".tr().toString(),
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                              isDense: true,
                            ),
                            onTap: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2000, 1, 1),
                                  maxTime: DateTime(2050, 1, 1), onChanged: (date) {
                                    print('change $date');
                                  }, onConfirm: (date) {
                                    print('confirm $date');
                                  }, currentTime: DateTime.now(), locale: _changeLocal(context));
                            },
                            style: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: width*.99,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.amber,
                          ),
                          child: Row(
                            children: [
                              Text(
                                'location'.tr().toString() + ' ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Icon(Icons.my_location_sharp),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              _getCurrentLocation();
                            });
                          },
                        ),
                        Container(
                          height: height * 0.06,
                          width: width*0.5,
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey, // set border color
                                style: BorderStyle.none), // set border width
                            // set rounded corner radius
                          ),
                          child: TextField(
                            enabled: false,
                            decoration: InputDecoration(
                              hintText:
                              'your_location'.tr().toString() + ' '+this.Country,
                              hintStyle: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.location_on_outlined),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.amber,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'save'.tr().toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Icon(Icons.save),
                      ],
                    ),
                    onPressed: () {
                      Asansor temp = new Asansor(
                          installerPhone: installerNumberController.text,
                          installerName: installerNameController.text,
                          status: _dropDownValue,
                          buildingNo: buildingNumberController.text,
                          location: _currentPosition,
                          kimlikNo: kimlikController.text,
                          phoneNo: telephoneController.text,
                          buildingname: buildingNameController.text,
                          liftkind: _dropLiftType);
                    InProgressLift tempo = new InProgressLift(
                      comittedname: installerNameController.text,
                      comittedphone: installerNumberController.text,
                      buildinginfo: buildingAptController.text,
                      buildingName: buildingNameController.text,
                      buildingNumber: buildingNumberController.text,
                      lifttype: _dropLiftType,
                      location: _currentPosition,
                      fakekimlik: kimlikController.text,
                      currentStage: 0
                    );
                    for(int i =0 ; i< 12; i++)
                      {
                        tempo.stages[i] = new Stage(desc: "");
                      }
                      _awaitReturnValueFromSecondScreen(context, temp, tempo);
                    },
                  ),
                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ),
    );
  }

  String manipulateDate(DateTime date, int month)
  {
    if(date.month+1 > 12)
    {
      if(date.month+month == 13)
      return (date.year+1).toString() +
          "-" +
          (1).toString() +
          "-" +
          date.day.toString();
      if(date.month+month == 14)
        return (date.year+1).toString() +
            "-" +
            (02).toString() +
            "-" +
            date.day.toString();
    }
      return (date.year).toString() +
          "-" +
          (date.month+month).toString() +
          "-" +
          date.day.toString();
  }
  
  
  void _awaitReturnValueFromSecondScreen(
      BuildContext context, Asansor temp, InProgressLift tempo) async {
    postRequest(temp, tempo);

    Navigator.popAndPushNamed(context, "/");
  }

  Future<Response> postRequest (Asansor temp, InProgressLift tempo) async {
    if(_dropDownValue != "in_progress".tr().toString()) {
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);

      Maintainance m1 = new Maintainance();
      m1.liftNumber = temp.kimlikNo;
      m1.liftType = 'Engine';
      m1.dateTime = manipulateDate(realdate, 0);
      m1.done = '0';
      m1.employeeName = temp.installerName;
      m1.location = temp.location;
      m1.description = '';
      m1.lift_kind = _dropLiftType;
      Maintainance m2 = new Maintainance();
      m2.lift_kind = _dropLiftType;
      m2.liftNumber = temp.kimlikNo;
      m2.liftType = 'Cabine';
      m2.description = '';
      m2.dateTime = manipulateDate(realdate, 1);
      m2.done = '0';
      m2.employeeName = temp.installerName;
      m2.location = temp.location;
      Maintainance m3 = new Maintainance();
      m3.liftNumber = temp.kimlikNo;
      m3.liftType = 'Well';
      m3.lift_kind = _dropLiftType;
      m3.dateTime = manipulateDate(realdate, 2);
      m3.done = '0';
      m3.description = '';
      m3.employeeName = temp.installerName;
      m3.location = temp.location;
      temp.maintanances = new List<Maintainance>();
      temp.maintanances.add(m1);
      temp.maintanances.add(m2);
      temp.maintanances.add(m3);
      temp.jsonList = temp.maintanances.map((maintenanc) => maintenanc.toJson()).toList();

      var body = temp.toJson();
      var encodedbody = json.encode(body);
      Map<String, String> headers = {'Content-Type': 'application/json'};

      await post(url,
          headers: headers,
          body: encodedbody
      ).then((Response response) {
        print("Response status: ${response.statusCode}");
        print("Response body: ${response.contentLength}");
        print(response.headers);
        print(response.request);
        if(response.body == "Lift was added Successfully")
          {
            Fluttertoast.showToast(
                msg: "Success".tr().toString(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );

          }
        else
          {
            Fluttertoast.showToast(
                msg: "Failed".tr().toString(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );
          }
      });
    }
    else
      {
        Uri url = Uri.https(
            consts.domain, consts.domain_inProgress);
        tempo.lifttype = _dropLiftType;
        var body = tempo.toJson();
        var encodedbody = json.encode(body);
        Map<String, String> headers = {'Content-Type': 'application/json'};

        await post(url,
            headers: headers,
            body: encodedbody
        ).then((Response response) {
          print("Response status: ${response.statusCode}");
          print("Response body: ${response.contentLength}");
          print(response.headers);
          print(response.request);
          if(response.body == "Lift was added Successfully")
            {
              Fluttertoast.showToast(
                  msg: "Success".tr().toString(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
              Test_Book test_book = new Test_Book(fakekimlik: tempo.fakekimlik, lifttype: _dropLiftType, stageid: "0.0_0");
              postTest_book(test_book);
            }
        });
      }
  }

  Future<void> postTest_book(Test_Book temp) async{
    Uri url = Uri.https(
        consts.domain, consts.domain_test_book);
    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await post(url,
        headers: headers,
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
        {
          Fluttertoast.showToast(
              msg: "Success Test".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      else{
        Fluttertoast.showToast(
            msg: "Failed".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  double _getHeight(BuildContext context)
  {

    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*0.5688;
    }
    return MediaQuery.of(context).size.height*.5873;
  }

}