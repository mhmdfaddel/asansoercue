import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MapUtils.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:url_launcher/url_launcher.dart';

class AsansorDetails extends StatefulWidget {
  Asansor myAsansor;

  AsansorDetails({Key key, this.myAsansor}) : super(key: key);

  @override
  _AsansorDetailsState createState() => _AsansorDetailsState();
}

class _AsansorDetailsState extends State<AsansorDetails> {
  var value = 0;
  String Country = " ";
  Constants consts = new Constants();
  Position _currentPosition;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  String _dropDownValue;
  bool editable = false;

  TextEditingController kimlikController = TextEditingController();
  TextEditingController buildingNumberController = TextEditingController();
  TextEditingController buildingNameController = TextEditingController();
  TextEditingController telephoneController = TextEditingController();
  TextEditingController installerNameController = TextEditingController();
  TextEditingController installerNumberController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  _getAddressFromLatLng(Position position) async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          position.latitude, position.longitude);
      Placemark place = p[0];
      setState(() {
        Country = place.country;
        // _currentAddress =
        // "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> _launched;

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  bool first = true;
  String _dropLiftType='';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Map<String, Object> rcvdData = ModalRoute.of(context).settings.arguments;
    Asansor temp;
    if (rcvdData["asansorDetails"] != null) {
      temp = rcvdData["asansorDetails"];

    }
      String appTitle = 'ASANSÖRCÜ';


    if(first)
      {

        _dropDownValue = temp.status.tr().toString();
        _dropLiftType = temp.liftkind.tr().toString();
        first = false;
      }
    _getAddressFromLatLng(temp.location);


    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: true,
        isLang: false,
      ),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8155,
            width: MediaQuery.of(context).size.width * 1,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'asansor_info'.tr().toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.65,
                            0),
                        child: Text(
                          'id_number'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: kimlikController,
                          decoration: InputDecoration(
                            hintText: temp.kimlikNo,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.credit_card),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.65,
                            0),
                        child: Text(
                          'building_number'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: buildingNumberController,
                          decoration: InputDecoration(
                            hintText: temp.buildingNo,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.house_outlined),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.77,
                            0),
                        child: Text(
                          'building_name'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: buildingNameController,
                          decoration: InputDecoration(
                            hintText: temp.buildingname,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.house),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.4,
                            0),
                        child: Text(
                          'liftTelephone'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: GestureDetector(
                          onDoubleTap: (){
                            _launched = _makePhoneCall("tel:" + temp.phoneNo);
                          },
                          child: TextField(
                            controller: telephoneController,
                            decoration: InputDecoration(
                              hintText: temp.phoneNo,
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.contact_phone),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.65,
                            0),
                        child: Text(
                          'constructor_name'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: TextField(
                          controller: installerNameController,
                          decoration: InputDecoration(
                            hintText: temp.installerName,
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                            border: InputBorder.none,
                            icon: Icon(Icons.contacts),
                          ),
                        ),
                      ),
                    ],
                  ),

                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.0,
                            MediaQuery.of(context).size.height * 0.02,
                            MediaQuery.of(context).size.width * 0.35,
                            0),
                        child: Text(
                          'constructor_number'.tr().toString(),
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.06,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey, // set border color
                              width: 0.5,
                              style: BorderStyle.solid), // set border width
                          // set rounded corner radius
                        ),
                        child: GestureDetector(
                          onDoubleTap: (){
                            _launched = _makePhoneCall("tel:" + temp.installerPhone);
                          },
                          child: TextField(
                            controller: installerNumberController,
                            decoration: InputDecoration(
                              hintText: temp.installerPhone,
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              icon: Icon(Icons.phone_android_outlined),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(0.0, 0, 0, 0),
                    child: DropdownButton(
                      hint: _dropDownValue == null
                          ? Text('status')
                          : Text(
                              _dropDownValue.toString(),
                              style: TextStyle(color: Colors.blue),
                            ),
                      isExpanded: true,
                      iconSize: 15.0,
                      style: TextStyle(color: Colors.blue),
                      items: [
                        'working'.tr().toString(),
                        'not_working'.tr().toString()
                      ].map(
                        (val) {
                          return DropdownMenuItem<String>(
                            value: val,
                            child: Text(val),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(
                          () {
                            _dropDownValue = val;
                          },
                        );
                      },
                    ),
                  ),
                  Container(

                    height: height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    child: DropdownButton(
                      hint: _dropLiftType == null
                          ? Text('Lift Type'.tr().toString())
                          : Text(
                        _dropLiftType,
                        style: TextStyle(color: Colors.blue),
                      ),
                      isExpanded: true,
                      iconSize: 15.0,
                      style: TextStyle(color: Colors.blue),
                      items: [
                        'Gear Full Engine'.tr().toString(),
                        'Gear Less Engine'.tr().toString(),
                      ].map(
                            (val) {
                          return DropdownMenuItem<String>(
                            value: val,
                            child: Text(val),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(() {
                          _dropLiftType = val;
                        },
                        );
                      },
                    ),
                  ),
                  Card(
                    shadowColor: Colors.black45,
                    margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                    elevation: 5.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.amber,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'got_to_location'.tr().toString() + ' ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Icon(Icons.navigation),
                        ],
                      ),
                      onPressed: () {
                        setState(() {
                          MapUtils.openMap(temp.location.latitude,
                              temp.location.longitude);
                        });
                      },
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey, // set border color
                          width: 0.5,
                          style: BorderStyle.solid), // set border width
                      // set rounded corner radius
                    ),
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        hintText:
                            'your_location'.tr().toString() + this.Country,
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        border: InputBorder.none,
                        icon: Icon(Icons.location_on_outlined),
                      ),
                    ),
                  ),
                  Card(
                    shadowColor: Colors.black45,
                    margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                    elevation: 5.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.amber,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'save'.tr().toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Icon(Icons.save),
                        ],
                      ),
                      onPressed: () {
                        Asansor tempe = new Asansor(
                            id: temp.id,
                            kimlikNo: (kimlikController.text != "")? kimlikController.text: temp.kimlikNo,
                            location: temp.location,
                            installerName: (installerNameController.text != "")? installerNameController.text: temp.installerName,
                            phoneNo: (telephoneController.text != "")?telephoneController.text: temp.installerPhone,
                            buildingNo: (buildingNumberController.text != "")?buildingNumberController.text: temp.buildingNo,
                            buildingname: (buildingNameController.text != "")?buildingNameController.text: temp.buildingname,
                            status: _dropDownValue,
                            installerPhone: (installerNumberController.text != "")?installerNumberController.text: temp.installerPhone,
                          liftkind: _dropLiftType
                            );
                        putRequest(tempe);

                        _awaitReturnValueFromSecondScreen(
                            context, 'update', tempe);
                      },
                    ),
                  ),

                ],
              ),
            ),
          ),
          copyRights(),
        ],
      ), //drawer: MyDrawer(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed: () {
          setState(() {
            Navigator.pushNamed(context, '/createMainten');
          });
        },
        child: Icon(Icons.build),
      ),
    );
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context, String op, Asansor temp) async {
    // start the SecondScreen and wait for it to finish with a result

    //if (op == 'delete')
      //deleteRequest(temp);
    // Navigator.pushNamed(context, "/",
      //     arguments: {"asansor": temp, "ops": "delete"});
     if (op == 'update') {
       Navigator.popAndPushNamed(context, "/");

    } else {
      //NAVIGATE TO MAINTENANCES
      Navigator.pushNamed(context, "/maintenOps",
          arguments: {"Mainten": temp.operations});
    }
    // after the SecondScreen result comes back update the Text widget with it
  }

  Future<Response> putRequest (Asansor temp) async {
    print(temp.toJson());
    Uri url = Uri.https(
        consts.domain, consts.domain_lifts+temp.id);


    var body = temp.toJson();
    var encodedbody = json.encode(body);
    Map<String,String> headers = {'Content-Type':'application/json'};

    await put(url,
        headers: headers,
        //encoding: Encoding.getByName("application/json"),
        body: encodedbody
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

  Future<Response> deleteRequest (Asansor temp) async {
    print(temp.toJson());
    Uri url = Uri.https(
        consts.domain, consts.domain_lifts+temp.id);

    await delete(url
    ).then((Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });
  }

  double _getHeight(BuildContext context)
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return MediaQuery.of(context).size.height*.8155;
    }
    return MediaQuery.of(context).size.height*.81;
  }

}
