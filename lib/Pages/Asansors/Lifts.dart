import 'dart:convert';
import 'dart:ui';

import 'package:asansorci/Models/Asansor.dart';
import 'package:asansorci/Models/Maintainance.dart';
import 'package:asansorci/accessories/Constants.dart';
import 'package:asansorci/accessories/MyDrawer.dart';
import 'package:asansorci/accessories/copyRights.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';



class Lifts extends StatefulWidget {
  @override
  _LiftsState createState() => _LiftsState();
}

class _LiftsState extends State<Lifts> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  String _dropDownValue;
  double height;
  TextEditingController search = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  Constants consts = new Constants();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    if(mounted)
      setState(() {
        myLifts.clear();
        getLifts();
      });
    _refreshController.loadComplete();
  }


  List<Asansor> myLifts = [];
  List<Asansor> myTemps = [];

  Future<void> getLifts() async {
    var client = new http.Client();
    try {
      Uri url = Uri.https(
          consts.domain, consts.domain_lifts);
      http.Response response = await client.get(url);

      List data = jsonDecode(response.body);
      myLifts = data.map((e) => new Asansor().fromJson(e)).toList();
      myTemps.addAll(myLifts);
      setState(() {});
    } finally {
      client.close();
    }

  }


  @override
  void initState() {
    super.initState();
    getLifts();

  }

  List<Asansor> existAsansor(String temp, String cirtic) {
    List<Asansor> testo = new List<Asansor>();

    if(cirtic == 'id_number'.tr().toString())
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].kimlikNo.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    else if(cirtic == 'building_name'.tr().toString()){
      for (int i = 0; i < myLifts.length; i++) {
        if (myLifts[i].buildingname.contains(temp)) {
          testo.add(myLifts[i]);
        }
      }
    }
    return testo;
  }

  @override
  Widget build(BuildContext context) {
    final String appTitle = 'ASANSÖRCÜ';
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar(
        title: appTitle,
        isHome: false,
        isDetails: false,
        isLang: false,
      ),
      body: SmartRefresher(
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus mode){
            Widget body ;
            if(mode==LoadStatus.idle){
              body =  Text("pull up load");
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child:body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'lifts'.tr().toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 30.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.06,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.grey, // set border color
                    width: 0.5,
                    style: BorderStyle.solid),
                borderRadius:
                    BorderRadius.all(Radius.circular(50)), // set border width
                // set rounded corner radius
              ),
              child: TextField(
                controller: search,
                onChanged: (value) => {
                  setState(() {
                    if(this.search.value.text == "")
                      {
                        myLifts = myTemps;
                      }
                    else if(_dropDownValue == 'id_number'.tr().toString())
                      {
                        myLifts = existAsansor(search.value.text, 'id_number'.tr().toString());
                      }
                    else if(_dropDownValue == 'building_name'.tr().toString())
                      {
                        myLifts = existAsansor(search.value.text, 'building_name'.tr().toString());
                      }
                  })
                },
                decoration: InputDecoration(
                  hintText: 'search_elevator'.tr().toString(),
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                ),
              ),
            ),
            Container(
              // height: MediaQuery.of(context).size.height*0.055,
              // width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.02,
                  MediaQuery.of(context).size.width * 0.02,
                  0,
                  MediaQuery.of(context).size.width * 0.02),
              child: Row(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.width * 0.47,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text('search_by'.tr().toString())
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: Colors.blue),
                              ),
                        isExpanded: true,
                        iconSize: 15.0,
                        style: TextStyle(color: Colors.blue),
                        items: [
                          'id_number'.tr().toString(),
                          'building_name'.tr().toString(),
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val;
                            },
                          );
                        },
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.2, 0, 0, 0),
                    child: Text(
                      'total'.tr().toString() +
                          ' : ' +
                          this.myLifts.length.toString(),
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (EasyLocalization.of(context).locale == Locale('ar', 'SY'))
                  ? MediaQuery.of(context).size.height * 0.5687
                  : MediaQuery.of(context).size.height * 0.5873,
              width: MediaQuery.of(context).size.width * 0.95,
              child: Row(
                children: [
                  Container(
                    child: Expanded(
                      child: ListView.builder(
                        itemCount: this.myLifts.length,
                        itemBuilder: (context, index) =>
                            this._buildRow(this.myLifts[index]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            copyRights(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffe46b10),
        onPressed: () {
          setState(() {
            Navigator.popAndPushNamed(context, '/createLift');
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildRow(Asansor temp) {
    return Row(
      children: [
        GestureDetector(
          onTap: (){
            setState(() {
              Navigator.pushNamed(context, "/asansorDetails",
                  arguments: {"asansorDetails": temp});
            });
          },
          onHorizontalDragStart: (DragStartDetails details){
            setState(() {
              temp.color = Colors.redAccent;
            });
          },
          onHorizontalDragEnd: (DragEndDetails details){
            if (details.primaryVelocity > 0) {

              setState(() {
                _showDialog(temp);

              });
            }
            else
            {
              setState(() {
              });
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width * 0.7,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              shadowColor: Colors.orange[400],
              margin: const EdgeInsets.all(10),
              elevation: 15.0,
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.01,
                            0,
                            MediaQuery.of(context).size.width * 0.02,
                            MediaQuery.of(context).size.height * 0.02),
                        child: GestureDetector(
                          onTap: () {},
                          child: Text(
                            'id_number'.tr().toString() + ' : ' + temp.kimlikNo.toString(),
                            style: TextStyle(),
                          ),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.01,
                              0,
                              MediaQuery.of(context).size.width * 0.01,
                              MediaQuery.of(context).size.height * 0.02),
                          child: GestureDetector(
                            onTap: () {},
                            child: Text(
                              'building_name'.tr().toString() +
                                  ' : ' +
                                  temp.buildingname.toString(),
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width * 0.01,
                              0,
                              MediaQuery.of(context).size.width * 0.0,
                              MediaQuery.of(context).size.height * 0.02),
                          child: GestureDetector(
                            onTap: () {},
                            child: Text('constructor_name'.tr().toString() +
                                    ' : ' +
                                    temp.installerName.toString()),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
  Future<http.Response> deleteRequest (Asansor temp) async {
    print(temp.toJson());
    Uri url = Uri.https(
        consts.domain, consts.domain_lifts+temp.id);

    await http.delete(url
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);
      if(response.statusCode == 200)
      {
        Fluttertoast.showToast(
            msg: "Deleted".tr().toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => super.widget));
      }
      else
        {
          Fluttertoast.showToast(
              msg: "Failed".tr().toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
    });
  }
  double _getHeight()
  {
    if(EasyLocalization.of(context).locale == Locale('ar', 'SY')){
      return height*.5687;
    }
    return height*.5873;
  }

  _showDialog(Asansor temp) async {
    await showDialog<String>(
      context: context,
      builder: (context) =>  AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                autofocus: true,
                controller: passwordController,
                decoration: new InputDecoration(

                    labelText: 'Password', hintText: 'type your password'),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child:  Text('Cancel'.tr().toString()),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child:  Text('Ok'),
              onPressed: () {
                if(passwordController.text == (new Constants()).password)
                {
                  Fluttertoast.showToast(
                      msg: "Deleted".tr().toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                  myLifts.remove(temp);
                  deleteRequest(temp);
                  Navigator.pop(context);
                }
                else{
                  Fluttertoast.showToast(
                      msg: "Wrong Password".tr().toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                }
              })
        ],
      ),
    );
  }

}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
