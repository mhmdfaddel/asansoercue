import 'package:asansorci/Pages/HyView/extras/Expansione.dart';
import 'package:asansorci/Pages/contactus/Contact.dart';
import 'package:asansorci/accessories/myAppBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:open_mail_app/open_mail_app.dart';
import 'package:url_launcher/url_launcher.dart';


class ContactUs extends StatefulWidget {
  const ContactUs({Key key}) : super(key: key);

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final String appTitle = 'UMUT ASANSÖR';


  List<Contact> information = [];
  double height, width;
  
  
  @override
  void initState() {
    // TODO: implement initState
    information.add(new Contact(title: 'Developer Information:', subtitle: 'Hit me To know the Developer', desc: "My Name is Mohammad Fadel,\ni'm an IT Engineer,\nSpecialized in Systems and Networks,\nalongside with Developing Software Applications According to Famous and Creative Software Architectures ", gotoemail: 'mhmdfaddel@gmail.com', phonenumber: '+90 531 4977947'));
    information.add(new Contact(title: 'Owner Information', subtitle: 'Click Here To See', desc: 'The Owner is Khaled al-Masri', gotoemail: '', phonenumber: '+905369204146'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

     height = MediaQuery.of(context).size.height;
     width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: myAppBar(
        title: appTitle,
        isLang: false,
        isDetails: false,
        isHome: false,
      ),
      body: Column(
        children: [
          Container(
            color: HexColor("#3b4d61"),
            height: height*.4,
            width: width,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, height*.2, width*.2, 0),
                  child: Wrap(
                    children: [
                      Text('UMUT',style: TextStyle(
                        fontSize: 55,
                        color: Colors.white,
                        ),
                      ),
                      Text('ASANSÖR',style: TextStyle(
                        fontSize: 55,
                        color: Colors.white,
                      ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          Container(
            height: height*.49,
            child: SingleChildScrollView(
              child: Column(
                children: List.generate(information.length, (index) => _buildRow(information[index])),
              ),
            ),
          ),
        ],
      ),
    );
  }
  
  Widget _buildRow(Contact temp){
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0, height*.07, width*.3, 0),
          child: Wrap(
            direction: Axis.horizontal,
            children: [
              Text(temp.title.tr().toString(),style: TextStyle(
                fontSize: 25,
                color: HexColor("#3b4d61"),
              ),
              ),
            ],
          ),
        ),
        Container(
          width: width*.9,
          padding: EdgeInsets.fromLTRB(0, height*.01, width*.15, 0),
          child: ExpansionTile(
            title: Text(temp.subtitle.tr().toString(), style: TextStyle(
              color: Colors.white,
            ),
            ),
            backgroundColor: HexColor('3b4d61'),
            collapsedBackgroundColor: HexColor('d2601a'),
            children: [
              Wrap(
                  children:[
                    Padding(
                      padding: EdgeInsets.fromLTRB(width*.05, height*.01, width*.01, height*.02),
                      child: Text(temp.desc.tr().toString(), style: TextStyle(
                        color: Colors.white,
                        fontStyle: FontStyle.italic,
                      ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(width*.05, height*.01, width*.01, height*.02),
                      child: Text(temp.gotoemail, style: TextStyle(
                        color: Colors.white,
                        fontStyle: FontStyle.italic,
                      ),
                      ),
                    ),
              ]
              ),
            ],
          ),
        ),
        Padding(
          padding:  EdgeInsets.fromLTRB(width*.2,height*.03,width*.2,height*.03),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _makePhoneCall("tel:" + temp.phonenumber);
                  });
                },
                color: Colors.green[400],
                textColor: Colors.white,
                child: Icon(
                  Icons.call,
                  size: 24,
                ),
                padding: EdgeInsets.all(16),
                shape: CircleBorder(),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _openmail();
                  });
                },
                color: Colors.redAccent[400],
                textColor: Colors.white,
                child: Icon(
                  Icons.mail,
                  size: 24,
                ),
                padding: EdgeInsets.all(16),
                shape: CircleBorder(),
              ),
            ],
          ),
        ),
        Divider(height: 8,indent: 2, endIndent: 8,),

      ],
    );
  }
  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _openmail()async {
    // Android: Will open mail app or show native picker.
    // iOS: Will open mail app if single mail app found.
    var result = await OpenMailApp.openMailApp();

    // If no mail apps found, show error
    if (!result.didOpen && !result.canOpen) {
      showNoMailAppsDialog(context);

      // iOS: if multiple mail apps found, show dialog to select.
      // There is no native intent/default app system in iOS so
      // you have to do it yourself.
    } else if (!result.didOpen && result.canOpen) {
      showDialog(
        context: context,
        builder: (_) {
          return MailAppPickerDialog(
            mailApps: result.options,
          );
        },
      );
    }
  }
  void showNoMailAppsDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Open Mail App"),
          content: Text("No mail apps installed"),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }
}



class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
